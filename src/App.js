import React,{Component} from 'react';
import {View} from 'react-native';
import Router,{InBetween} from './Router';

class App extends Component{
  render(){
    return(
      <View style={{flex:1}}>
        <Router/>
      </View>
    )
  }
}

export default App;

// import { createStackNavigator, createAppContainer } from 'react-navigation';
// import NavigationService from './NavigationService';
// import Splash from './screens/Splash'
// const TopLevelNavigator = createStackNavigator({ 
//   home:Splash
//  })

// const AppContainer = createAppContainer(TopLevelNavigator);

// export default class App extends React.Component {
//   // ...

//   render() {
//     return (
//       <AppContainer
//         ref={navigatorRef => {
//           NavigationService.setTopLevelNavigator(navigatorRef);
//         }}
//       />
//     );
//   }
// }