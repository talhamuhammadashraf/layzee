import {combineReducers} from 'redux'
import {Auth} from './auth'
import {Profile} from './profile'
import { MainContainer,InBetween } from '../../Router'
import { NavigationActions } from 'react-navigation'
import {createNavigationReducer} from 'react-navigation-redux-helpers'
// const initialAction = { type: NavigationActions.Init }
// const initialState = MainContainer.router.getStateForAction(MainContainer.router.getActionForPathAndParams('Profile'))

// const navReducer = (state = initialState, action) => {
//   return { ...state }
// }
// const navReducer = createNavigationReducer(InBetween)
const rootReducer = combineReducers({
    auth:Auth,
    profile:Profile
    // nav : navReducer
})
export default rootReducer

