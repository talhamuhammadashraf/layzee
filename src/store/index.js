import {createStore,applyMiddleware} from 'redux';
import thunk from 'redux-thunk'
import logger from 'redux-logger';
import rootReducer from './reducers'
import Auth from './actions/auth'

const store = createStore(rootReducer,applyMiddleware(thunk,logger))
store.getState((state)=>console.log(state,'this is what '))
export default store

// store.dispatch(Auth.register())