import React,{Component} from 'react';
import {View,Image,Keyboard,Text,Dimensions,ScrollView} from 'react-native';
import Button from '../components/Buttons'
import Input from '../components/Input'
import Header from '../components/Header'
import CodeInput from 'react-native-confirmation-code-input';
const { height, width } = Dimensions.get("window");

class Policies extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Instructions and Policies" goBack={() => navigation.navigate('About')} />
        };    

    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this._keyboardDidShow());
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',() => this._keyboardDidHide());
      }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
    _keyboardDidShow () {
      }
    
      _keyboardDidHide () {
      }

    _verifyCode(){
    }

    _onChangeCode(){

    }

   
    render(){
        return(
                <ScrollView
                contentContainerStyle={{backgroundColor:'#F8F8F8'}}
                >
                <View
            style={{height:height-(height*0.08)}}                
                >
                <View
                style={{
                    width:width*0.8,
                    marginRight:"auto",
                    marginLeft:"auto",
                    borderBottomWidth:1,
                    borderBottomColor:"#d6dbdb"
                }}
                >
                <Text style={{
                    color:"#AE1371",
                    fontSize:17,
                    marginVertical:15
                }}>
                    Who May Use the Services 
                </Text>
                {/* <Text
                style={{
                    color:"black",
                    fontSize:14,
                    // marginVertical:15
                }}
                >
                    Who May Use the Services 
                </Text> */}
                <Text style={{marginVertical:5,fontSize:12,marginTop:10}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
                minim veniam, quis nostrud  aliquip ex ea commodo consequat.
                </Text>
                <Text style={{marginVertical:5,fontSize:12}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
                minim veniam, quis nostrud  aliquip ex ea commodo consequat.
                </Text>
                <Text style={{marginVertical:5,fontSize:12,marginBottom:20}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
                minim veniam, quis nostrud  aliquip ex ea commodo consequat.
                </Text>
                </View>


                <View
                style={{
                    width:width*0.8,
                    marginRight:"auto",
                    marginLeft:"auto",
                }}
                >
                <Text style={{
                    color:"#AE1371",
                    fontSize:17,
                    marginVertical:15
                }}>
                Privacy
                </Text>
                <Text style={{marginVertical:5,fontSize:12,marginTop:10}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
                minim veniam, quis nostrud  aliquip ex ea commodo consequat.
                </Text>
                <Text style={{marginVertical:5,fontSize:12}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
                minim veniam, quis nostrud  aliquip ex ea commodo consequat.
                </Text>
                <Text style={{marginVertical:5,fontSize:12,marginBottom:20}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
                minim veniam, quis nostrud  aliquip ex ea commodo consequat.
                </Text>
                </View>
                </View>
                </ScrollView>
        )
    }

}

const styles = {
    midHeading:{
        alignSelf:'flex-start'
    }
}

export default Policies;