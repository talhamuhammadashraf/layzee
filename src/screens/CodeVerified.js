import React,{Component} from 'react';
import {View,Image,Keyboard,Text,Dimensions,TouchableOpacity,Animated,Platform} from 'react-native';
import colors from '../../assets/colors';
import Input from './../components/Input';
import Buttons from './../components/Buttons';
import { Actions } from 'react-native-router-flux';
const { height, width } = Dimensions.get("window");

class CodeVerified extends Component{
    constructor(props){
        super(props);
        this.state = {
            height:height,
            width:width
        }
    }

    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this._keyboardDidShow());
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',() => this._keyboardDidHide());
      }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
    componentWillMount(){
        Animated.spring();
    }

    _keyboardDidShow () {
      }
    
      _keyboardDidHide () {
      }

    _navigateBack(){
        Actions.pop();
    }

    _verifyCode(){
        Actions.main({type:'reset'});
    }

   
    render(){
        return(
                <View style={{flex:1,alignItems:'center'}}>
                    <TouchableOpacity style={{position:'absolute',top:Platform.OS=='ios'?height*0.05:height*0.03,left:15}} onPress={()=>this._navigateBack()}>
                        <Image
                        source={require('../../images/backBlack.png')}
                        style={{height:20,width:20}}
                        />
                    </TouchableOpacity>
                    
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={styles.midHeading}>Your email has been successfully verified</Text>
                        <Buttons filled={true} heading="GET STARTED" onPress={this._verifyCode}/>
                    </View>
                </View>
        )
    }

}

const styles = {
    midHeading:{
        color:colors.black,
        alignSelf:'center'
    }
}

export default CodeVerified;