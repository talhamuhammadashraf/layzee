import React ,{Component} from  'react';
import {View,Text,Dimensions,TouchableOpacity,ScrollView,Image,Keyboard,Linking,DatePickerAndroid} from 'react-native';
import {bindActionCreators} from 'redux'
import  {connect} from 'react-redux';
import store from '../store'
import {REGISTER} from '../store/Middleware/auth'
const {width,height} = Dimensions.get('window')
import Select from '../../images/select.png'
import Unselect from '../../images/unselect.png'
import Input from '../components/Input'
import Button from '../components/Buttons'
import Header from '../components/Header'
import Logo from '../../images/logo.png'
import Menu from '../../images/menu.png'
import Back from '../../images/backward.png'
import Cross from '../../images/close.png'

const Title = (props) =>{
    (<View style={{width:width,height:height*0.08,flexDirection:"row",justifyContent:"space-evenly"}}>
        <Image source={props.menu ? Menu : Back}/>
        <Image source={Logo} resizeMode="contain"/>
    </View>)
}
const mapStateToProps = (state) =>({
state:state
})
const mapDispatchToProps = (dispatch) => ({
    REGISTER:(abcd)=>dispatch(REGISTER(abcd))
})
    
class SignUp extends Component{
    constructor(){
        super();
        this.state={
            checked:false,
            passwordMatch : true,
            errorEmail : false,
            errorPass : false,
            gender : 0
        }
    }
    static navigationOptions = {
        header:({navigation}) => <Header   goBack={()=>navigation.navigate('SignIn')}/>
        };
    componentDidMount () {
        // this.props.REGISTER("kdhksdjh",this.props.navigation)
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this._keyboardDidShow());
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',() => this._keyboardDidHide());
      }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
    _keyboardDidShow () {
        this.setState({ keyboardOpened: true })
      }
    
      _keyboardDidHide () {
        this.setState({ keyboardOpened: false })
      }
    async openCalandar(){
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
              // Use `new Date()` for current date.
              // May 25 2020. Month 0 is January.
              date: new Date(2020, 4, 25)
            });
            if (action !== DatePickerAndroid.dismissedAction) {
              // Selected year, month (0-11), day
              this.setState({dob:`${day}/${month}/${year}`})
              console.log(year,month,day,"this is the date")
            }
          } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
          }
    }
    render(){console.log('sign up state',this.state)
        // console.log(this.props.state,this.props.REGISTER("kdhksdjh"),"ye daikho")
        return(
            <ScrollView>
            <View
            style={{height:height,backgroundColor:"#F8F8F8"}}
            >
                <Text style={{
                    color:"#AE1371",
                    fontSize:30,
                    fontWeight:'400',
                    marginVertical:10,
                    textAlign:"center"
                    }}>
                Sign up
                </Text>
                {
                this.state.errorEmail ||  
                this.state.errorPass || 
                this.state.errorName || 
                !this.state.passwordMatch ? 
                <Text style={{
                    color: "red",
                    fontSize: 12,
                    width: width * 0.9,
                    marginRight: "auto",
                    marginLeft: "auto"
                }}>
                {this.state.errorMsg}
                </Text> : null}
                <Input
                    placeholder="Name"
                    onChangeText={(text)=>{
                        var validate = new RegExp("(?=.{3,})");
                        var bool = validate.test(text)
                        this.setState({name:text,errorName : bool ? false : true ,errorMsg:'must be atleast 3 characters'})}}
                />
                {/* <Input
                    placeholder="Phone Number"
                /> */}
                <Input
                onChangePhoneNumber={(phone)=>{this.setState({phone:phone})}}
                phone
                isValidNumber={(bool)=>console.log(bool,"this will show the phone ")}
                // isValidNumber={(bool)=>console.log(bool,"ye lou")}
                />
                <Input
                    placeholder="Email"
                    error={this.state.errorEmail}
                    onChangeText={(text)=>{
                        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        var bool = re.test(text)
                        this.setState({email:text,errorEmail:bool ? false : true,errorMsg:'invalid email'})}
                    }

                />
                <Input
                    placeholder="Password"
                    error={this.state.errorPass}
                    secureTextEntry={true}
                    onChangeText={(text)=>{
                        var validate = new RegExp("(?=.{6,})");
                        var bool = validate.test(text)
                        this.setState({password:text,errorPass:bool ? false : true ,errorMsg:'password must be atleast 6 characters'})}}

                />
                <Input
                    placeholder="Confirm Password"
                    secureTextEntry={true}
                    onChangeText={(text)=>this.setState({confirm:text,passwordMatch:this.state.password === text ? true : false,errorMsg:'password did not match'})}
                    error={!this.state.passwordMatch}
                    // onFocus={()=>{
                    //     if(this.state.password !== null){
                    //     this.state.confirm === this.state.password ? 
                    //     this.setState({passwordMatch:true}) :
                    //     this.setState({passwordMatch:false})} 
                    // }}
                />
                <TouchableOpacity style={{
                    marginTop:15,
                backgroundColor:"white",
                borderRadius:10,
                width:width*0.9,
                marginRight:"auto",
                marginLeft:"auto",
                height:height*0.08,
                // marginTop:5,
                flexDirection:'row',
                alignItems:'center',
                }} 
                onPress={this.openCalandar}
                >
                    <Text 
                            style={{
                                color:"#F8F8F8",
                                width:"85%",
                                marginRight:"auto",
                                marginLeft:"auto",
                                // fontSize:18
                    
                            }}
                    >Date Of Birth</Text>
                </TouchableOpacity>

                        <View
                        style={
                            {flexDirection: "row",
                            width: "75%",
                            marginRight: "auto",
                            marginLeft: "auto",
                            justifyContent: "space-between",
                            marginTop: 20,
                            marginBottom:15}
                        }
                        >
                        {['Male', 'Female', "Other"].map((data, index) => (
                            <TouchableOpacity onPress={() => { this.setState({ gender: index }) }} key={index}
                                style={{ backgroundColor: this.state.gender === index ? "#AE1371" : "white",
                                width: "27.5%",
                                height: 30,
                                borderRadius: 12.5,
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: 'center' 
                                }}>
                                <View>
                                    <Text style={{ fontSize: 13, color: this.state.gender === index ? "white" : "black" }}>{data}</Text>
                                </View>
                            </TouchableOpacity>
                        ))}</View>

                        <View style={{
                            flexDirection:"row",
                            width:width*0.9,
                            marginRight:"auto",
                            marginLeft:"auto",
                            marginVertical:10}}>
                            {
                                /* <CheckBox checked={this.state.checked}
                            color="#AE1371" style={{marginRight:10}}
                            onPress={()=>this.setState((prevState)=>({checked:!prevState.checked}))}
                            /> */
                            }

                    <TouchableOpacity onPress={()=>this.setState((prevState)=>({checked:!prevState.checked}))}>
                    <Image source={this.state.checked ? Select : Unselect} style={{height:20,width:20,marginRight:10}}/>
                    </TouchableOpacity>
                    <Text style={{color:"#808080"}}>
                        I agree to <Text
                        onPress={()=>
                            // Linking.canOpenURL().then(supported => {
                            //     if (!supported) {
                            //       console.log('Can\'t handle url: ');
                            //     } else {
                            //       return 
                                  Linking.openURL('https://layzee.com')
                            //     }
                            //   }).catch(err => alert('An error occurred', err))
                        }
                        style={{color:"blue",fontSize:14}}>terms and conditions</Text> of LAYZEE
                    </Text>
                </View>
                    <Button
                    title="Sign Up"
                    colored
                    width={width*0.9}
                    style={{
                        bottom:10,
                        position:'absolute',
                        left:width*0.05,
                        right:width*0.05,

                    }}
                    onPress={()=>this.props.REGISTER({
                    user:{name:this.state.name,
                    phone:this.state.phone,
                    email:this.state.email,
                    password:this.state.password,
                    user_role_id:4,
                    gender:this.state.gender,
                    dob:this.state.dob,
                    CityId:"1",},
                    navigation:this.props.navigation
        })}
                    // onPress={()=>this.props.navigation.navigate('verify')}
                    />
            </View>
            </ScrollView>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(SignUp)