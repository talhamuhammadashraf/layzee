import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body, Right } from "native-base";
import Header from '../components/Header'
import Favorite from '../../images/favoritebutton.png'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class FavServices extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Favorite Services" goBack={()=>navigation.navigate('Starter')} />
    };
    render() {
        return(
            <View>
                {[
                    {
                        titleRight:'Car Wash',
                        titleLeft:'100AED',
                        vendor:'xyzzy',
                        location:'street123,xyz,area,state',
                        date:'23rd,oct,2018'
                    },                    {
                        titleRight:'Plumbing',
                        titleLeft:'100AED',
                        vendor:'xyzzy',
                        location:'street123,xyz,area,state',
                        date:'23rd,oct,2018'
                    },                    {
                        titleRight:'Home Cleaning',
                        titleLeft:'100AED',
                        vendor:'xyzzy',
                        location:'street123,xyz,area,state',
                        date:'23rd,oct,2018'
                    },                    {
                        titleRight:'Laundry',
                        titleLeft:'100AED',
                        vendor:'xyzzy',
                        location:'street123,xyz,area,state',
                        date:'23rd,oct,2018'
                    },
                ]
                    .map((data,index)=>(<View key={index}
                style={styles.container}
                >
                <View style={styles.left} >
                <Text style={styles.titleLeft} >
                    {data.titleRight}
                </Text>
                <Text style={styles.textLeft} >
                    Vendor:{data.vendor}
                </Text>
                <Text style={styles.textLeft} >
                    Location:{data.location}
                </Text>
                </View>
                <View style={styles.right} >
                <Text style={styles.titleRight} >
                    {data.titleLeft}
                </Text>
                <Text style={styles.textRight} >
                    {data.date}
                </Text>
                <Image source={Favorite} style={{width:22,height:20,alignSelf:"center",margin:10}}/>
                </View>

                </View>))}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:"white",
        borderRadius:15,
        marginRight:'auto',
        marginLeft:'auto',
        width:"90%",
        height:height*0.135,
        marginTop:20,
        flexDirection:'row',
        alignItems:'center'

    },
    titleLeft:{
        color: 'black',
        fontSize:16,
        marginLeft:17,
        // fontWeight:'450'
    },
    titleRight:{
        color: 'black',
        fontSize: 16,
        textAlign:"center",
        // fontWeight:'450'

    },
    textRight:{
        fontSize: 10,
        textAlign:'center',
        marginTop:3
    },
    textLeft:{
        fontSize: 12,
        marginLeft: 17,
        marginTop:5
    },
    right:{
        width:'32%',
        height:'80%',
    },
    left:{
        width:'68%',
        height:'80%'
    }
});

export default FavServices;