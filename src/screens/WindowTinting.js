import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView,TimePickerAndroid } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body } from "native-base";
import Modal from 'react-native-modal'
import ModalDropdown from 'react-native-modal-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Down from '../../images/downarrow.png'
import Header from '../components/Header'
import Cross from '../../images/close.png'
import Tick from '../../images/tick.png'
import Button from '../components/Buttons'
import Unselect from '../../images/unselect.png'
import Select from '../../images/select.png'
import Pen from '../../images/pen.png'
import DatePicker from 'react-native-datepicker';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator,
  } from 'react-native-indicators';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class WindowTinting extends Component {

    constructor(props) {
        super(props);
        this.state = {
            indicatorVisible:false,
            modalVisible : false,
            datetime: '2016-05-05 20:00',
            tintPercent: "",
            isDateTimePickerVisible: false,
            dropDownCarSelect: false,
            carSelectHeader: {
                title: 'Select Car',
                carLogo: null,
                modal1Visible: false,
                modal2Visible: false,
                'Wind Sheild': false,
                'Front Windows': false,
                'Back Windows': false
            },

        }
    }

    _showTimePicker = async() =>{
        try {
            const { action, hour, minute } = await TimePickerAndroid.open({
            //   hour: this.props.date.getHours(),
            //   minute: this.props.date.getMinutes(),
            //   is24Hour: this.props.is24Hour,
            //   mode: this.props.datePickerModeAndroid
            });
            // if (action !== TimePickerAndroid.dismissedAction) {
            //   let date;
            //   if (this.props.date) {
            //     // This prevents losing the Date elements, see issue #71
            //     const year = this.props.date.getFullYear();
            //     const month = this.props.date.getMonth();
            //     const day = this.props.date.getDate();
            //     date = new Date(year, month, day, hour, minute);
            //   } else {
            //     date = new Date().setHours(hour).setMinutes(minute);
            //   }
            //   this.props.onConfirm(date);
            //   this.props.onHideAfterConfirm(date);
            // } else {
            //   this.props.onCancel();
            // }
          } 
          catch ({ code, message }) {
            console.warn("Cannot open time picker", message);
          }
      
    } 

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
    };
    static navigationOptions = {
        header: ({ navigation }) => <Header RightIcon={Cross} title="Window Tinting" onRightPress={() => navigation.navigate('Starter')} goBack={()=>navigation.navigate('Starter')} />
    };


    renderDropdownCarSelect = () => {
        let { title, carLogo } = this.state.carSelectHeader;
        let { dropDownCarSelect } = this.state;
        let imageUp = require('../../images/up.png');
        let imageDown = require('../../images/Down.png');
        let { dropDownOptionImage, downImage } = styles;
        return (
            <TouchableOpacity style={styles.dropDownContainer} onPress={() => this.setState(prevState => ({
                dropDownCarSelect: !prevState.dropDownCarSelect,
            }))}>
                <Image resizeMode='contain' source={carLogo && Tick} style={carLogo === null ? null : dropDownOptionImage} />
                <Text style={[styles.headerText, { color: this.state.carSelectHeader.carLogo ? '#AE1371' : 'black' }]}>{title}</Text>
                <Image resizeMode='contain' source={dropDownCarSelect === true ? imageUp : imageDown} style={downImage} />
            </TouchableOpacity>
        )
    }

    valueSelected(dataArr) {
        this.setState({
            carSelectHeader: dataArr,
            dropDownCarSelect: false
        });
    }

    renderDropdownOptionsCarSelect = () => {
        let { dropDownOptionImage } = styles;
        if (this.state.dropDownCarSelect === true) {
            return (
                <View style={styles.dropDownOptionContainer}>
                    {/* <Text style={styles.optionsGuide}>Select Car</Text> */}
                    <TouchableOpacity style={styles.row} onPress={() => this.valueSelected({ title: 'Mercedes', carLogo: require('../../images/mercedes.jpg') })}>
                        <Image resizeMode='contain' source={require('../../images/mercedes.jpg')} style={dropDownOptionImage} />
                        <Text style={styles.headerText}>Mercedes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.row} onPress={() => this.valueSelected({ title: 'BMW', carLogo: require('../../images/bmw.png') })}>
                        <Image resizeMode='contain' source={require('../../images/bmw.png')} style={dropDownOptionImage} />
                        <Text style={styles.headerText}>BMW</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.row} onPress={() => this.valueSelected({ title: 'Audi', carLogo: require('../../images/audi.jpg') })}>
                        <Image resizeMode='contain' source={require('../../images/audi.jpg')} style={dropDownOptionImage} />
                        <Text style={styles.headerText}>Audi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.row} onPress={() => this.setState({ modal1Visible: true })}>
                        <Image resizeMode='contain' source={require('../../images/plus.png')} style={dropDownOptionImage} />
                        <Text style={styles.headerText}>Add Car</Text>
                    </TouchableOpacity>
                </View>

            )
        } else {
            return <View />
        }

    }


    componentWillMount() {
        this.setState({ modal1Visible: false })
    }
componentWillUnmount(){
    this.setState({modalVisible:false,indicatorVisible:false})
}
    render() {
        console.log(this.state, "this is the state in window")
        return (
            <ScrollView contentContainerStyle={styles.container}>
                {/* <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                /> */}
                <Modal isVisible={this.state.indicatorVisible}>
                    <View style={{width:width*0.3,height:width*0.3,backgroundColor:'white',alignSelf:'center',borderRadius:14}}>
                        <BallIndicator size={60} color="#AE1371"/>
                    </View>
                </Modal>
                <Modal isVisible={this.state.modalVisible}>
                    <View style={styles.modalvendorContainer}>
                        <View style={styles.modalvendorTopContainer}>
                            <Text style={{ color: '#AE1371', fontSize: 18, marginLeft: 15 }}>Sorry</Text>
                            <TouchableOpacity onPress={() => this.setState({ modalVisible: false })}>
                                <Image source={Cross} style={{ width: 20, height: 20, marginRight: 15 }} />
                            </TouchableOpacity>
                        </View>
                        <Text style={{ color: 'grey', width: '90%', alignSelf: 'center' }}>
                            Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                    </Text>
                        <View style={{ paddingVertical: 15, justifyContent: "space-between" }}>
                            <Button width={width * 0.8} title="Manually Select a Vendor" colored style={{ marginBottom: 7 }} onPress={()=>{this.setState({modalVisible:false});this.props.navigation.navigate('VendorMap')}}/>
                            <Button width={width * 0.8} title="Automatically Select a Vendor" colored style={{ marginBottom: 7 }}  onPress={()=>{this.setState({modalVisible:false});this.setState({indicatorVisible:true})}}/>
                        </View>
                    </View>
                </Modal>

                <Modal isVisible={this.state.modal1Visible}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalTop}>
                            <Text style={styles.modalTitle}>Car Details</Text>
                            <TouchableOpacity onPress={() => this.setState({ modal1Visible: false, dropDownCarSelect: false })}>
                                <Image source={Cross} style={{ height: 20, width: 20, marginRight: 10, marginVertical: 15 }} />
                            </TouchableOpacity>
                        </View>
                        {["Car Model", "Car Type", "Car Make"].map((data, index) => (
                            <View key={index} style={{ margin: 8 }}>
                                <Text style={styles.itemTitle}>{data}</Text>
                                <View style={styles.innerContainer}>
                                    <View style={{ width: "80%" }}>
                                        <TextInput underlineColorAndroid="transparent" placeholder="Type here" style={{ paddingLeft: 20 }} />
                                    </View>
                                    <ModalDropdown options={['option 1', 'option 2']} style={styles.rightContainer}
                                        dropdownStyle={{
                                            backgroundColor: "green",
                                            width: "80%",
                                            marginRight: "auto",
                                            marginLeft: "auto"
                                        }}
                                    >
                                        <View style={{ width: "100%" }}>
                                            <Image source={Down} style={styles.downArrow} />
                                        </View>
                                    </ModalDropdown>
                                </View>
                            </View>
                        ))
                        }
                        <View style={{ margin: 8 }}>
                            <Text style={styles.itemTitle}>Plate Number</Text>
                            <View style={styles.innerContainer}>
                                <View style={{ width: "80%" }}>
                                    <TextInput underlineColorAndroid="transparent" placeholder="Type here" style={{ paddingLeft: 20 }} />
                                </View>
                            </View>
                        </View>

                        <View style={styles.modalButton}>
                            <Text style={styles.modalButtonText}>Save</Text>
                        </View>

                    </View>
                </Modal>
                <Modal isVisible={this.state.modal2Visible}>
                    <View style={[styles.modalContainer, { height: height * 0.6 }]}>
                        <View style={styles.modalTop}>
                            <Text style={styles.modalTitle}>Tinting Options</Text>
                            <TouchableOpacity onPress={() => this.setState({ modal2Visible: false, 'Wind Sheild': false, 'Front Windows': false, 'Back Windows': false, tintPercent: "" })}>
                                <Image source={Cross} style={{ height: 20, width: 20, marginRight: 10, marginVertical: 15 }} />
                            </TouchableOpacity>
                        </View>
                        <View>
                            {['Wind Sheild', 'Front Windows', 'Back Windows']
                                .map((data, index) => (<View style={styles.modal2Row} key={index}>
                                    <Text>{data}</Text>
                                    <TouchableOpacity onPress={() => this.setState((prevState) => ({ [data]: !prevState[data] }))}>
                                        <Image source={this.state[data] ? Select : Unselect} style={{ height: 20, width: 20 }} />
                                    </TouchableOpacity>
                                </View>))}
                        </View>
                        <View style={{
                            width: '80%',
                            marginRight: 'auto',
                            marginLeft: "auto",
                            marginTop: 20
                        }} >
                            <Text>Tint Percentage</Text>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                marginTop: 15
                            }}>
                                {
                                    ['40%', '55%', "75%"].map((data, index) => (
                                        <TouchableOpacity onPress={() => { this.setState({ tintPercent: index }) }} key={index}
                                            style={[styles.selectBox, { backgroundColor: this.state.tintPercent === index ? "#AE1371" : "white" }]}>
                                            <View>
                                                <Text style={{ fontSize: 13, color: this.state.tintPercent === index ? "white" : "black" }}>{data}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => this.setState({ modal2Visible: false })} style={styles.modalButton}>
                            {/* <View style={styles.modalButton}> */}
                            <Text style={styles.modalButtonText}>Confirm</Text>
                            {/* </View> */}
                        </TouchableOpacity>

                    </View>
                </Modal>
                <Modal isVisible={this.state.modal3Visible}>
                    <View style={[styles.modalContainer, { height: height * 0.77 }]}>
                        <View style={styles.modalTop}>
                            <Text style={styles.modalTitle}>Add ons</Text>
                            <TouchableOpacity onPress={() => this.setState({ modal3Visible: false, "Add on 1": false, "Add on 2": false, "Add on 3": false, "Add on 4": false })}>
                                <Image source={Cross} style={{ height: 20, width: 20, marginRight: 10, marginVertical: 15 }} />
                            </TouchableOpacity>
                        </View>
                        <View>
                            {[
                                { title: "Add on 1", price: '15.00', details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad" },
                                { title: "Add on 2", price: '15.00', details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad" },
                                { title: "Add on 3", price: '15.00', details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad" },
                                { title: "Add on 4", price: '15.00', details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad" },
                            ]
                                .map((data, index) => (<View style={styles.modal2Row} key={index}>
                                    <View style={{
                                        width: '90%',
                                        // backgroundColor:'green',
                                        // flexDirection:'row'
                                    }}>
                                        <Text style={{ color: '#AE1371', marginBottom: 0 }}>{data.title}</Text>
                                        <Text style={{ color: '#AE1371', fontSize: 10, color: 'grey', margin: 0 }}>AED {data.price}</Text>
                                        <Text style={{ color: '#AE1371', fontSize: 10, color: 'grey', marginVertical: 10 }}>{data.details}</Text>

                                    </View>
                                    <TouchableOpacity style={{ width: '10%', height: '100%', justifyContent: 'flex-start', alignItems: 'center' }} onPress={() => this.setState((prevState) => ({ [data.title]: !prevState[data.title] }))}>
                                        <Image source={this.state[data.title] ? Select : Unselect} style={{ height: 20, width: 20 }} />
                                    </TouchableOpacity>
                                </View>))}
                        </View>
                        <TouchableOpacity style={styles.modalButton} onPress={() => this.setState({ modal3Visible: false })}>
                            <Text style={styles.modalButtonText}>Confirm</Text>
                        </TouchableOpacity>

                    </View>
                </Modal>

                <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={styles.circleWhite} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                </View>
                <View style={{ height: "auto", bottom: 0 }}>
                    {this.renderDropdownCarSelect()}
                    {this.renderDropdownOptionsCarSelect()}
                </View>
                <TouchableOpacity style={[styles.dropDownContainer, { marginTop: 10 }]} onPress={() => this.setState({ modal2Visible: true })} >
                    <Image resizeMode='contain' source={Tick} style={[styles.dropDownOptionImage,
                    { display: this.state['Wind Sheild'] || this.state['Front Windows'] || this.state['Back Windows'] ? 'flex' : 'none' }
                    ]} />
                    <Text style={[styles.headerText, {
                        color: this.state['Wind Sheild'] || this.state['Front Windows'] || this.state['Back Windows'] ? "#AE1371" : 'black'
                    }]}>Job Details</Text>
                    <Image resizeMode='contain' source={Pen} style={styles.downImage} />
                </TouchableOpacity>
                <TouchableOpacity style={[styles.dropDownContainer, { marginTop: 10 }]} onPress={() => this.setState({ modal3Visible: true })}>
                    <Image resizeMode='contain' source={Tick} style={[styles.dropDownOptionImage,
                    { display : this.state['Add on 1'] || this.state['Add on 2'] || this.state['Add on 3'] || this.state['Add on 4'] ? 'flex' :'none'}
                    ]}/>
                    <Text style={[styles.headerText, { 
                        color: this.state['Add on 1'] || this.state['Add on 2'] || this.state['Add on 3'] || this.state['Add on 4'] ? "#AE1371" : 'black' 
                        }]}>Add on</Text>
                    <Image resizeMode='contain' source={Pen} style={styles.downImage} />
                </TouchableOpacity>
                <Text style={{ color: 'black', width: '85%', marginRight: 'auto', marginLeft: 'auto', marginTop: 10, fontSize: 15 }}>Additional Information</Text>
                <View style={{ width: width * 0.9, padding: 10, borderRadius: 15, marginTop: 10, height: height * 0.2, backgroundColor: 'white' }}>
                    <TextInput
                        style={{ width: "100%" }}
                        placeholder="Type here"
                        placeholderTextColor='grey'
                        multiline
                    />
                </View>
                <View style={styles.bottomPart}>
                    <View style={styles.pricePart}>
                        <Text style={styles.mainPrice}>
                            Price: 0.00 AED
                    </Text>
                        <Text style={styles.priceDesc}>
                            (Including VAT - 10 AED)
                    </Text>
                    </View>
                    <View style={styles.bottomDiv}>
                        <TouchableOpacity style={styles.bottomSheetButton} onPress={()=>this.setState({modalVisible:true})}>
                            <Text style={{ color: 'white', fontSize: 18 }}>Book Right Now</Text>
                            {/* <Image source={require('../../images/arrow.png')} style={{height:15,width:23,marginLeft:25}}/> */}
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={styles.bottomSheetButtonMinor} onPress={this._showTimePicker}>
                            <Image resizeMode='contain' source={require('../../images/waiting.png')} style={styles.waitingImage} />
                        </TouchableOpacity> */}
                        <DatePicker
                            style={{ width: 200 }}
                            date={this.state.datetime}
                            mode="datetime"
                            format="YYYY-MM-DD HH:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            androidMode='spinner'
                            showIcon={false}
                            ref={(picker) => { this.datePicker = picker; }}
                            TouchableComponent={() => <TouchableOpacity style={styles.bottomSheetButtonMinor} onPress={() => this.datePicker.onPressDate()}>
                                <Image resizeMode='contain' source={require('../../images/red-card.png')} style={styles.waitingImage} />
                            </TouchableOpacity>}
                            onDateChange={(datetime) => { this.setState({ datetime: datetime }); }}
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    modalvendorContainer:{
        width:width*0.9,
        height:height*0.4,
        backgroundColor:'white',
        borderRadius:15
        },
        modalvendorTopContainer:{
            width:'100%',
            borderTopLeftRadius:15,
            borderTopRightRadius:15,
            height:50,
            flexDirection:'row',
            justifyContent:'space-between',
            alignItems:'center',
            borderBottomWidth:1,
            borderColor:'grey',
            marginBottom:10
    },

    selectBox: {
        width: "27.5%",
        height: 30,
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
        borderRadius: 12.5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'center'
    },

    modal2Row: {
        width: '80%',
        minHeight: 40,
        marginTop: 10,
        marginRight: 'auto',
        marginLeft: 'auto',
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: 'row',
        borderBottomColor: '#DCDCDC',
        borderBottomWidth: 1

    },
    modalButton: {
        backgroundColor: '#AE1371',
        width: '100%',
        height: 50,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        position: 'absolute',
        bottom: 0

    },
    modalTitle: {
        color: 'black',
        fontSize: 18,
        width: '80%',
        marginRight: "auto",
        marginLeft: "auto",
        height: 50,
        fontWeight: "bold",
        textAlignVertical: "center"

    },
    modalContainer: {
        width: width * 0.9,
        height: height * 0.7,
        borderRadius: 15,
        backgroundColor: '#F8F8F8',

    },
    modalTop: {
        backgroundColor: "white",
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        width: '100%',
        height: 50,
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    modalButtonText: {
        color: "white",
        textAlign: 'center',
        height: 50,
        textAlignVertical: 'center',
        fontSize: 16

    },
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    bottomSheetButton: {
        flexDirection: 'row',
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 7,
        height: 50,
        width: width * 0.6,
        borderRadius: 15,
        backgroundColor: '#AE1371'
    },
    bottomSheetButtonMinor: {
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        width: width * 0.17,
        borderRadius: 15,
        backgroundColor: 'white'
    },
    optionsGuide: {
        fontSize: 16,
        padding: 10,
        color: 'grey',
        marginLeft: 10
    },
    checkboxContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    dropDownContainer: {
        // position:"absolute",
        flexDirection: 'row',
        width: width * 0.9,
        height: height * 0.06,
        backgroundColor: 'white',
        borderRadius: 15,
        padding: 7,
        alignItems: 'center',
        marginTop: 30,
        zIndex: 0
    },
    checkbox: {
        width: 20,
        height: 20,
    },
    addsOnText: {
        fontSize: 16,
        marginLeft: 20,
        color: 'grey'
    },
    dropDownOptionContainer: {
        position: "absolute",
        zIndex: 1,
        width: width * 0.9,
        height: "auto",//change
        backgroundColor: 'white',
        borderRadius: 15,
        padding: 12,
        alignItems: 'center',
        marginTop: 20
    },
    dropDownOptionImage: {
        height: 12,
        width: 16,
        marginLeft: 10
    },
    downImage: {
        height: 15,
        width: 20,
        position: 'absolute',
        right: 20
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginTop: 10,
        paddingBottom: 5,
        alignSelf: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#E3E0DD'
    },
    headerText: {
        fontSize: 16,
        color: 'grey',
        marginLeft: 10
    },
    pricePart: {
        alignItems: 'flex-end',
        marginRight: 15,
        marginTop: 15
    },
    bottomPart: {
        height: 150,
        width: width,
        bottom: 0,
        marginTop: 20,
        borderTopWidth: 1,
        borderTopColor: 'grey',
    },
    mainPrice: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#AE1371'
    },
    priceDesc: {
        fontSize: 12,
        color: 'grey'
    },
    buttonBookLater: {
        width: width * 0.15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#C5C8CC'
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleWhite: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    Arrow: {
        height: 15,
        width: 15
    },
    waitingImage: {
        height: 25,
        width: 25
    },
    bottomDiv: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: width * 0.9,
        alignSelf: 'center',
    },
    itemTitle: {
        fontSize: 12,
        color: "black",
        width: "80%",
        marginLeft: "auto",
        marginRight: "auto",
        marginBottom: 10,
        fontWeight: "bold"
    },
    innerContainer: {
        width: "84%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,

    },
    rightContainer: {
        width: "20%",
        backgroundColor: "#AE1371",
        borderTopRightRadius: 14,
        borderBottomRightRadius: 14,
        height: "100%",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"

    },
    downArrow: {
        height: 15,
        width: 15,
        marginRight: "auto",
        marginLeft: "auto"
    },
});

export default WindowTinting;