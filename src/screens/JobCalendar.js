import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import menu from '../../images/list.png'
import Header from '../components/Header';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
//#6AC083 green , orrange #FE959F ,blue #7FB8F9
export default class JobCalendar extends Component{
    constructor(){
        super();
        this.state={
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="2018" RightIcon={menu}  goBack={navigation.goBack} />
    };
    render(){
    return(
        <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.topContainer}>
        <View style={styles.jobNames}>
            {[
                {title:'Past Jobs',color:"#6AC083"},
                {title:'Ongoing Jobs',color:"#7FB8F9"},
                {title:'Upcoming Jobs',color:"#FE959F"},
            ].map((data,index)=>(<View style={{width:width*0.33,flexDirection:'row',alignItems:"center",padding:15}} key={index}>
                <View style={{height:10,width:10,borderRadius:5,backgroundColor:data.color}}></View>
                <Text style={{marginLeft:5}}>{data.title}</Text>
            </View>))}
        </View>
        <View style={styles.weekContainer}>
        {['S','M','T','W','T','F','S'].map(
            (data,index)=><Text style={{fontSize:12,margin:width*0.03}} key={index}>{data}</Text>
        )}
        </View>
        </View>
        <CalendarList
        onDayPress={(day) => this.props.navigation.navigate('Agenda')}
        markingType={'custom'}
        markedDates={{
            '2018-12-05':{
                customStyles:{
                    container:{
                        backgroundColor:"#6AC083"
                    },
                    text:{
                        color:"white"
                    }
                }
            },
            '2018-12-06':{
                customStyles:{
                    container:{
                        backgroundColor:"#FE959F"
                    },
                    text:{
                        color:"white"
                    }
                }
            },
            '2018-12-07':{
                customStyles:{
                    container:{
                        backgroundColor:"#7FB8F9"
                    },
                    text:{
                        color:"white"
                    }
                }
            }


        }}
        scrollEnabled={true}
        />






        </ScrollView>
)}}

const styles = StyleSheet.create({
    weekContainer:{
        height:height*0.03,
        width:width,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:15
        },
    jobNames:{
        height:height*0.12,
        width:width,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    container:{
        backgroundColor: '#F8F8F8',
        flex:1
    },
    topContainer:{
        width:width,
        height:height*0.15,

    }
});