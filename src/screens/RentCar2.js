import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body } from "native-base";
import Header from '../components/Header'
import Button from '../components/Buttons'
import ModalDropdown from 'react-native-modal-dropdown';
import Down from '../../images/downarrow.png'
// import HomeWhite  from '../../images/homewhite.png'
// import Home  from '../../images/home.png'
import Standard from '../../images/standard.png'
import StandardSelected from '../../images/standardselected.png'
import Budget from '../../images/budget.png'
import BudgetSelected from '../../images/budgetselected.png'
import Luxury from '../../images/luxury.png'
import LuxurySelected from '../../images/luxuryselected.png'
import Cross from '../../images/close.png'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class RentCar2 extends Component {

    constructor(props) {
        super(props);
        this.state={
            type:"",
            icon:[Luxury,Standard,Budget],
            selected:[LuxurySelected,StandardSelected,BudgetSelected]
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Rent a Car" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
            <View style={{height:height*1.175}}>
                <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={styles.circleColor} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                </View>

                {["Select Car Type", "Select Age", "Select Color", "Car Model"].map((data, index) => (
                    <View key={index} style={{ margin: 8 }}>
                        <Text style={styles.itemTitle}>{data}</Text>
                        <View style={styles.innerContainer}>
                            <View style={{ width: "80%" }}>
                                <TextInput underlineColorAndroid="transparent" placeholder="Type here" style={{ paddingLeft: 20 }} />
                            </View>
                            <ModalDropdown options={['option 1', 'option 2']} style={styles.rightContainer}
                                dropdownStyle={{
                                    backgroundColor: "green",
                                    width: "80%",
                                    marginRight: "auto",
                                    marginLeft: "auto"
                                }}
                            >
                                <View style={{ width: "100%" }}>
                                    <Image source={Down} style={styles.downArrow} />
                                </View>
                            </ModalDropdown>
                        </View>
                    </View>
                ))
                }
                <Text style={{width:"72%",marginLeft:"auto",marginRight:'auto'}}>Price Category</Text>
                <View style={styles.selectContainer}>
                    {
                        ['Luxury', 'Standard', "Budget"].map((data, index) => (
                            <TouchableOpacity onPress={() => { this.setState({ type: index }) }} key={index}
                                style={[styles.selectBox, { backgroundColor: this.state.type === index ? "#AE1371" : "white" }]}>
                                <View>{console.log(this.state.type === "" ? true : false)}
                                    <Image source={this.state.type !== "" && this.state.type === index ? this.state.selected[index] : this.state.icon[index]}
                                        style={styles.icon} />
                                    <Text style={{ fontSize: 13, color: this.state.type === index ? "white" : "black" }}>{data}</Text>
                                </View>
                            </TouchableOpacity>
                        ))
                    }
                </View>

                    <View style={styles.priceContanier}>
                        <Text style={styles.text1}>Price:  0.00 AED</Text>
                        <Text style={styles.text2}>including VAT - 10 AED</Text>
                    </View>

                    <View style={styles.bottom}>
                        <Button title="Next" colored width={"90%"} onPress={()=>this.props.navigation.navigate('RentCar3')}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    container: {
        // justifyContent:'center',
        // alignItems:'center',
        // flex: 1
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleColor: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    itemTitle: {
        fontSize: 12,
        color: "black",
        width: "80%",
        marginLeft: "auto",
        marginRight: "auto",
        marginBottom: 10,
        fontWeight: "bold"
    },
    innerContainer: {
        width: "84%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,

    },
    rightContainer: {
        width: "20%",
        backgroundColor: "#AE1371",
        borderTopRightRadius: 14,
        borderBottomRightRadius: 14,
        height: "100%",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"

    },
    downArrow: {
        height: 15,
        width: 15,
        marginRight: "auto",
        marginLeft: "auto"
    },
    selectContainer:{
        flexDirection:"row",
        width:"75%",
        marginRight:"auto",
        marginLeft:"auto",
        justifyContent:"space-between",
        marginTop:20
    },
    selectBox:{
        width: "27.5%", 
        height: "auto", 
        padding:12,
        borderRadius:12.5,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:'center'
    },
    icon:{
        height: 22, 
        width: 55, 
        marginRight: "auto", 
        marginLeft: "auto",
        marginVertical:9
    },
    bottom:{
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },
    priceContanier:{
        borderTopWidth:1,
        marginTop:15,
        borderColor:"#DCDCDC",
        padding:20,
    },
    text1:{
        textAlign:"right",
        fontSize:18,
        fontWeight:"bold",
        color:"#AE1371"

    },
    text2:{
        justifyContent:"flex-end",
        textAlign:"right",
        fontSize:10

    }


});

export default RentCar2;