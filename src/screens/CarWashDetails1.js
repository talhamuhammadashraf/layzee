import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body } from "native-base";
import Header from '../components/Header'
import MapView, { } from 'react-native-maps';
import Location from '../../images/Path.png'
import StarRating from 'react-native-star-rating';
import Cross from '../../images/menu2.png'
import Modal from 'react-native-modal'


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

let _this = null;

class CarWashDetails1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sortVisible: false,
            modalVisible: false,
            region: {
                latitude: 24.9418733,
                longitude: 67.1121096,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            starCount: 0,
            markers: [
                {
                    coordinate: {
                        latitude: 24.9418733,
                        longitude: 67.1121096
                    },
                    title: "Best Place",
                    description: "Description1",
                    id: 1
                },
                // {
                //     coordinate: {
                //         latitude: 24.9438733,
                //         longitude: 67.1131096
                //     },
                //     title: "Best Place2",
                //     description: "Description 2",
                //     id: 2
                // }
            ],
        }
    }
    static navigationOptions = {
        header: ({ navigation, screenProps }) => <Header title="Car Wash" RightIcon={Cross} onRightPress={() => _this.setState({ sortVisible: true })} goBack={() => navigation.navigate('Starter')} />
    };
    componentDidMount() {
        _this = this;
    }
    async componentWillMount() {
        await navigator.geolocation.getCurrentPosition(
            (position) => {
                startPoint = {
                    longitude: position.coords.longitude,
                    latitude: position.coords.latitude
                }
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
        // this.scroller.scrollTo({x:380,y:0})
    }


    render() {
        console.log('this is state', this.state)
        return (
            <ScrollView contentContainerStyle={styles.container}>


                {this.state.sortVisible && <View style={{
                    position: 'absolute',
                    top: 0,//height * 0.08,
                    right: 1,
                    zIndex: 2,
                    backgroundColor: 'white',
                    borderRadius: 14,
                    height: 80,
                    width: 150,
                    alignItems: 'center',
                    borderColor: "#e5e3e3",
                    borderWidth: 0.40
                }}>
                    <View style={{ width: '100%', height: "33.33%" }}><Text style={{ width: '80%', marginLeft: 15 }}>Message Vendor</Text></View>
                    <View style={{ width: '100%', height: "33.33%", borderColor: "#DCDCDC", borderTopWidth: 1 }}><Text style={{ width: '80%', marginLeft: 15 }}>Call Vendor</Text></View>
                    <TouchableOpacity onPress={() => _this.setState({ modalVisible: true,sortVisible:false })}
                        style={{ width: '100%', borderColor: "#DCDCDC", height: "33.33%", borderTopWidth: 1 }}><Text style={{ width: '80%', marginLeft: 15 }}>Cancel Vendor</Text></TouchableOpacity>
                </View>}

                <MapView
                    ref={map => this.map = map}
                    showsMyLocationButton={true}
                    showsCompass={true}
                    showsUserLocation={true}
                    loadingEnabled={true}
                    initialRegion={this.state.region}
                    style={styles.containerMap}
                >
                    {this.state.markers.map((marker) =>
                        (
                            <MapView.Marker
                                key={marker.id}
                                coordinate={marker.coordinate}
                                image={Location}
                                draggable
                                title={marker.title}
                                description={marker.description}
                            />))
                    }
                </MapView>
                <View style={{
                    width: width * 0.7,
                    minHeight: 35,
                    borderRadius: 12,
                    padding: 13,
                    alignSelf: 'center',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'white',
                    marginTop: -20
                }}>
                    <Text style={{ color: 'black', fontSize: 16 }}>Booking ID # 54651</Text>
                </View>
                <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
                    <View style={[styles.circleColor, { backgroundColor: "#AE1371" }]} />
                    <View style={[styles.bar, { backgroundColor: "#AE1371" }]} />
                    <View style={styles.circleColor} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                </View>
                <Text style={styles.container1Title}>Job Details</Text>
                <View style={[styles.container1Outer, { borderTopWidth: 0, borderBottomWidth: 1 }]}>
                    <View style={[styles.container1Inner, { padding: 3 }]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Job Status</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>started</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer, { borderTopWidth: 0, borderBottomWidth: 1 }]}>
                    <View style={[styles.container1Inner, { padding: 3 }]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Booking Date</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>23rd Oct,2018</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer, { borderTopWidth: 0, borderBottomWidth: 1 }]}>
                    <View style={[styles.container1Inner, { padding: 3 }]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Booking Time</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>10:45 AM</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer, { borderTopWidth: 0, borderBottomWidth: 1 }]}>
                    <View style={[styles.container1Inner, { padding: 3 }]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Location</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>Street xyz,State</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer, { borderTopWidth: 0, borderBottomWidth: 1 }]}>
                    <View style={[styles.container1Inner, { padding: 3 }]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Vendor</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>John Doe</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer, { borderTopWidth: 0, borderBottomWidth: 1 }]}>
                    <View style={[styles.container1Inner, { padding: 3 }]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Feedback Given</Text>
                        <View style={{ width: '17.5%', flexDirection: 'row' }}>
                            <View style={{}}>
                                <StarRating
                                    disabled={false}
                                    starSize={15}
                                    starStyle={{}}
                                    disabled={true}
                                    // style={{flexDirection:'row',justifyContent:'flex-start'}}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={5}
                                    selectedStar={(rating) => this.setState({
                                        starCount: rating
                                    })}
                                    fullStarColor={'#e8c814'}
                                />
                            </View>
                        </View>
                    </View>
                </View>


                <Text style={styles.container1Title}>Job Details</Text>
                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>No. of Cleaners</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>2 x 20AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>No. of Hours</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>3 X 50AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Materials</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>10AED</Text>
                    </View>
                </View>

                <Text style={styles.container1Title}>Add ons</Text>
                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 1</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>20AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 2</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>30AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 3</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>40AED</Text>
                    </View>
                </View>


                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Wait time</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>5AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Service Time</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>435.00AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Discount</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>-100AED</Text>
                    </View>
                </View>


                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Final Service Cost</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>355.00 AED</Text>
                    </View>
                </View>

                <View style={styles.container1Outer}>
                    <View style={{ width: '90%', marginRight: 'auto', marginLeft: 'auto' }}>
                        <Text style={{ color: 'black', fontSize: 17, height: 40, textAlignVertical: "center", }}>Additional Info</Text>
                        <View style={{ backgroundColor: 'white', borderRadius: 15, minHeight: 80, padding: 10 }}>
                            <TextInput multiline={true}
                                defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
                            />
                        </View>
                    </View>
                </View>

                <Modal isVisible={this.state.modalVisible}>
                    <View style={styles.modalContainer}>
                        <Text style={{
                            color: 'black',
                            marginTop: 16,
                            marginBottom:8,
                            textAlign: "center",
                            fontSize: 20,
                        }}>Cancel Booking</Text>
                        <Text style={{
                            color: 'grey',
                            textAlign: "center",
                            width:'80%',
                            alignSelf:'center',
                            fontSize: 12,
                        }}>Are you sure you want to cancel the booking of window tinting service</Text>
                        <Text style={{
                            color: '#AE1371',
                            marginTop: 6,
                            textAlign: "center",
                            fontSize: 17,
                        }}>You will be charged X AED</Text>

                        <View style={styles.alertButton}>
                            <TouchableOpacity style={styles.no} onPress={()=>this.setState({modalVisible:false})}><Text style={{ color: 'white' }}>No</Text></TouchableOpacity>
                            <TouchableOpacity style={styles.yes}onPress={()=>this.setState({modalVisible:false})}><Text style={{ color: 'grey' }}>Yes</Text></TouchableOpacity>
                        </View>
                    </View>
                </Modal>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    modalContainer: {
        width: '80%',
        height: height * 0.24,
        borderRadius: 15,
        backgroundColor: "white",
        alignSelf: 'center',

    },
    alertButton: {
        bottom: 0,
        width: '100%',
        height: 40,
        position: 'absolute',
        flexDirection: 'row'

    },
    no: {
        width: '50%',
        borderBottomLeftRadius: 15,
        height: 40,
        backgroundColor: '#AE1371',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center"

    },
    yes: {
        width: '50%',
        borderBottomRightRadius: 15,
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        borderTopWidth: 1,
        borderColor: "#DCDCDC"

    },
    containerMap: {
        // position:'absolute',
        top: 0,
        height: height * 0.35,
        width: width,
        right: 0,
        left: 0
    },
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleColor: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    bottomSheetButton: {
        flexDirection: 'row',
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        width: width * 0.6,
        borderRadius: 15,
        backgroundColor: '#AE1371'
    },
    bottomSheetButtonMinor: {
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        width: width * 0.17,
        borderRadius: 15,
        backgroundColor: 'white'
    },

    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F8F8F8'
    },
    waitingImage: {
        height: 25,
        width: 25
    },
    bottomDiv: {
        marginVertical: 12,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: width * 0.9,
        alignSelf: 'center',
    },
    container1Title: {
        color: 'black',
        fontSize: 17,
        width: '90%',
        marginRight: 'auto',
        marginLeft: 'auto',
        height: 40,
        textAlignVertical: "center",

    },
    container1Outer: {
        borderTopWidth: 1,
        borderColor: '#DCDCDC',
        width: width,
        paddingVertical: 7

    },
    container1Inner: {
        width: width * 0.9,
        marginLeft: 'auto',
        marginRight: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5,

    }
});

export default CarWashDetails1;