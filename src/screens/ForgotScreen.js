import React,{Component} from 'react';
import {View,Image,Keyboard,Text,Dimensions,TouchableOpacity,Animated,Platform} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors';
import Input from './../components/Input';
import Buttons from './../components/Buttons';
import { Actions } from 'react-native-router-flux';
const { height, width } = Dimensions.get("window");

class ForgotScreen extends Component{
    constructor(props){
        super(props);
        this.state = {
            height:height,
            width:width,
            logoHeight:height*0.12
        }
    }

    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this._keyboardDidShow());
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',() => this._keyboardDidHide());
      }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
    componentWillMount(){
        Animated.spring();
    }

    _keyboardDidShow () {
      }
    
      _keyboardDidHide () {
      }

    _navigateBack(){
        Actions.pop();
    }

    _onChangeEmail(){

    }

    _resendPassword(){
        Actions.code();
    }

    _Guest(){

    }

   
    render(){
        return(
            <LinearGradient 
                colors={['#848AD8', '#950F56', '#BD4686']} 
                style={styles.linearGradient}
                start={{x: 1, y: 0}} 
                end={{x: 0.5, y: 1.0}}
                >
                <View style={{flex:1,alignItems:'center'}}>
                    <TouchableOpacity style={{position:'absolute',top:Platform.OS=='ios'?height*0.05:height*0.03,left:15}} onPress={()=>this._navigateBack()}>
                        <Image
                        source={require('../../images/back.png')}
                        style={{height:20,width:20}}
                        />
                    </TouchableOpacity>
                    <View style={{flex:0.5,justifyContent:'flex-end'}}>
                        <Image
                        source={require('../../images/logo.png')}
                        style={{height:50,width:width*0.3,alignSelf:'flex-end'}}
                        resizeMode='contain'
                        />
                    </View>
                    <View style={{flex:2,alignItems:'center',justifyContent:'center'}}>
                        <Text style={styles.midHeading}>FORGOT YOUR PASSWORD</Text>
                        <Input type="email-address" hint="Enter your Email" changeText={(text)=> this._onChangeEmail(text)} />
                        <Buttons color={true} heading="RESET PASSWORD" onPress={this._resendPassword}/>
                    </View>
                    <View style={{justifyContent:'center',alignItems:'center',flex:0.5}}>
                        <View style={styles.horizon}/>
                        <TouchableOpacity style={styles.guest} onPress={()=>this._Guest()}>
                            <Text style={styles.guestText}>CONTINUE AS GUEST</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </LinearGradient>
        )
    }

}

const styles = {
    linearGradient: {
        flex: 1,
      },
      horizon:{
        height:1,
        width:width*0.7,
        backgroundColor:'#FFFFFF'
    },
    guest:{
        marginTop:10
    },
    guestText:{
      color:'#FFFFFF',
    },
    midHeading:{
        fontWeight:'bold',
        color:colors.white
    }
}

export default ForgotScreen;