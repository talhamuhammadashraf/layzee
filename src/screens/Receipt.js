import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body } from "native-base";
import Header from '../components/Header'
import Thumbs from '../../images/jobdone.png'
import Fav from '../../images/favouriteoutline.png'
import FavFilled from '../../images/favoritebutton.png'
import StarRating from 'react-native-star-rating';
import DatePicker from 'react-native-datepicker';
import Modal from 'react-native-modal'
import Cross from '../../images/close.png'
import Button from '../components/Buttons'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Receipt extends Component {

    constructor(props) {
        super(props);
        this.state = {
            starCount: 0,
            fav:false,
            datetime: '2016-05-05 20:00',
            modalVisible:false
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Receipt" goBack={()=>navigation.navigate('Starter')} />
    };
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={{
                    backgroundColor: '#AE1371',
                    height: height * 0.35,
                    width: "100%",

                }}>
                <Modal isVisible={this.state.modalVisible}>
                <View style={styles.modalContainer}>
                    <View style={styles.modalTopContainer}>
                    <Text style={{color:'#AE1371',fontSize:18,marginLeft:15}}>Sorry</Text>
                    <TouchableOpacity onPress={()=>this.setState({modalVisible:false})}>
                    <Image source={Cross} style={{width:20,height:20,marginRight:15}}/>
                    </TouchableOpacity>
                    </View>
                    <Text style={{color:'grey',width:'90%',alignSelf:'center'}}>
                    Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                    </Text>
                    <View style={{paddingVertical:15,justifyContent:"space-between"}}>
                    <Button width={width*0.8} title="Book the vendor Later" colored  style={{marginBottom:7}}/>
                    <Button width={width*0.8} title="Select Another Vendor" colored   style={{marginBottom:7}}/>
                    <Button width={width*0.8} title="Assign vendor automatically" colored />
                    </View>
                </View>
                </Modal>
                    <Image source={Thumbs} style={{ width: 30, height: 30, alignSelf: "center", margin: 12 }} />
                    <Text style={{
                        color: 'white',
                        fontSize: 17,
                        fontWeight: '300',
                        width: "90%",
                        alignSelf: 'center'
                    }}>Home Cleaning Completed Successfully</Text>
                    <View style={{
                        width: '90%',
                        alignSelf: 'center',
                        // flexDirection:'row',

                    }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 8 }}>
                            <Text style={{ color: 'white', fontSize: 12 }}>Booking ID</Text>
                            <Text style={{ color: 'white', fontSize: 12 }}>45651</Text>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 8 }}>
                            <Text style={{ color: 'white', fontSize: 12 }}>Booking Date</Text>
                            <Text style={{ color: 'white', fontSize: 12 }}>23rd Ocrt, 2018</Text>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 8 }}>
                            <Text style={{ color: 'white', fontSize: 12 }}>Booking Time</Text>
                            <Text style={{ color: 'white', fontSize: 12 }}>10:45 AM</Text>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 8 }}>
                            <Text style={{ color: 'white', fontSize: 12 }}>Location</Text>
                            <Text style={{ color: 'white', fontSize: 12 }}>House B55,Area no23</Text>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 8 }}>
                            <Text style={{ color: 'white', fontSize: 12 }}>Vendor Name</Text>
                            <Text style={{ color: 'white', fontSize: 12 }}>John Doe</Text>
                        </View>

                    </View>
                </View>


                <Text style={styles.container1Title}>Job Details</Text>
                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>No. of Cleaners</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>2 x 20AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>No. of Hours</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>3 X 50AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Materials</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>10AED</Text>
                    </View>
                </View>

                <Text style={styles.container1Title}>Add ons</Text>
                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 1</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>20AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 2</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>30AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 3</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>40AED</Text>
                    </View>
                </View>


                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Wait time</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>5AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Service Time</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>435.00AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Discount</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>-100AED</Text>
                    </View>
                </View>


                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Final Service Cost</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>355.00 AED</Text>
                    </View>
                </View>

                <View style={styles.container1Outer}>
                    <View style={{
                        width: '90%',
                        marginRight: 'auto',
                        marginLeft: 'auto'
                    }}>
                        <Text style={{
                            color: 'black',
                            fontSize: 17,
                            height: 40,
                            textAlignVertical: "center",

                        }}>Rate Vendor</Text>
                        <View style={{ width: '25%' }}>
                            <StarRating
                                disabled={false}
                                starSize={20}
                                starStyle={{}}
                                // style={{flexDirection:'row',justifyContent:'flex-start'}}
                                emptyStar={'ios-star-outline'}
                                fullStar={'ios-star'}
                                halfStar={'ios-star-half'}
                                iconSet={'Ionicons'}
                                maxStars={5}
                                rating={this.state.starCount}
                                selectedStar={(rating) => this.setState({
                                    starCount: rating
                                })}
                                fullStarColor={'#e8c814'}
                            /></View>
                        <Text style={{
                            color: 'black',
                            fontSize: 17,
                            height: 40,
                            textAlignVertical: "center",

                        }}>Review</Text>
                        <View style={{
                            backgroundColor:'white',
                            borderRadius:15,
                            minHeight:80
                            }}>
                            <TextInput multiline={true}/>
                        </View>
                    </View>
                </View>
                <View style={{ borderTopWidth: 1, borderColor: '#DCDCDC', flexDirection: 'row', width: '100%' }}>
                    <View style={{ width: '90%', marginRight: 'auto', marginLeft: "auto", flexDirection: 'row', height: 50, alignItems: "center" }}>
                        <TouchableOpacity onPress={()=>this.setState((prevState)=>({fav:!prevState.fav}))}>
                        <Image source={this.state.fav ? FavFilled : Fav} style={{ width: 24, height: 22 }} />
                        </TouchableOpacity>
                        <Text style={{ color: "black", margin: 5 }}>Mark this as favourite</Text>
                    </View></View>
                <View style={styles.bottomDiv}>
                    <TouchableOpacity style={styles.bottomSheetButton} onPress={()=>this.setState({modalVisible:true})}>
                        <Text style={{ color: 'white', fontSize: 18 }}>Accept</Text>
                    </TouchableOpacity>
                    <DatePicker
                        style={{ width: 200 }}
                        date={this.state.datetime}
                        mode="datetime"
                        format="YYYY-MM-DD HH:mm"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        androidMode ='spinner'
                        showIcon={false}
                        ref={(picker) => { this.datePicker = picker; }}
                        TouchableComponent={()=><TouchableOpacity style={styles.bottomSheetButtonMinor} onPress={()=>this.datePicker.onPressDate()}>
                        <Image resizeMode='contain' source={require('../../images/red-card.png')} style={styles.waitingImage} />
                    </TouchableOpacity>}
                        onDateChange={(datetime) => { this.setState({ datetime: datetime }); }}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    modalContainer:{
        width:width*0.9,
        height:height*0.55,
        backgroundColor:'white',
        borderRadius:15
        },
        modalTopContainer:{
            width:'100%',
            borderTopLeftRadius:15,
            borderTopRightRadius:15,
            height:50,
            flexDirection:'row',
            justifyContent:'space-between',
            alignItems:'center',
            borderBottomWidth:1,
            borderColor:'grey',
            marginBottom:10
    },
    bottomSheetButton: {
        flexDirection: 'row',
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        width: width * 0.6,
        borderRadius: 15,
        backgroundColor: '#AE1371'
    },
    bottomSheetButtonMinor: {
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        width: width * 0.17,
        borderRadius: 15,
        backgroundColor: 'white'
    },

    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F8F8F8'
    },
    waitingImage: {
        height: 25,
        width: 25
    },
    bottomDiv: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: width * 0.9,
        alignSelf: 'center',
    },
    container1Title: {
        color: 'black',
        fontSize: 17,
        width: '90%',
        marginRight: 'auto',
        marginLeft: 'auto',
        height: 40,
        textAlignVertical: "center",

    },
    container1Outer: {
        borderTopWidth: 1,
        borderTopColor: '#DCDCDC',
        width: width,
        paddingVertical: 7

    },
    container1Inner: {
        width: width * 0.9,
        marginLeft: 'auto',
        marginRight: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5,

    }
});

export default Receipt;