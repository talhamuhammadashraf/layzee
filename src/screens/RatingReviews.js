import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon } from "native-base";
import StarRating from 'react-native-star-rating';
import Header from '../components/Header'
import Button from '../components/Buttons'
import Avatar from '../../images/avatar.jpg'
import Round from '../../images/about.png'
import VendorCard from '../components/VendorCard'
import Cross from '../../images/close.png'
import Group from '../../images/Group2.png'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class RatingReviews extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sortVisible: false,
            vendor:null
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Rating and Review" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>


                <ScrollView contentContainerStyle={{
                    width: width * 0.9,
                    alignSelf: 'center'
                }}>
                    {[1,2,3,4,5,6,7]
                        .map((data,index)=>(
                        <VendorCard key={index} onPress={()=>this.setState({vendor:index})}/>
                    //     <View style={styles.cardContainer} key={index}>
                    //     <View style={styles.cardContainerRight}>
                    //         <View style={styles.upperRow}>
                    //             <Text style={{ fontSize: 16, color: this.state.vendor === index ? "white" : 'black' }}>Shehryar Mirza</Text>
                    //             <View style={{ width: '22.5%', flexDirection: 'row' }}>
                    //                 <View style={{}}>
                    //                     <StarRating
                    //                         disabled={false}
                    //                         starSize={15}
                    //                         starStyle={{}}
                    //                         disabled={true}
                    //                         // style={{flexDirection:'row',justifyContent:'flex-start'}}
                    //                         emptyStar={'ios-star-outline'}
                    //                         fullStar={'ios-star'}
                    //                         halfStar={'ios-star-half'}
                    //                         iconSet={'Ionicons'}
                    //                         maxStars={5}
                    //                         rating={5}
                    //                         selectedStar={(rating) => this.setState({
                    //                             starCount: rating
                    //                         })}
                    //                         fullStarColor={'#e8c814'}
                    //                     />
                    //                 </View>
                    //             </View>
                    //         </View>

                    //         <View style={{ width: '90%', flexDirection: 'row', justifyContent: 'flex-start', alignSelf: "center", alignItems: 'center' ,marginTop:10}}>
                    //             <Text style={{width: '100%',fontSize: 12,color: this.state.vendor === index ? 'white' : 'grey'}}>
                    //             Lorem ipsum dolor sit amet, consectetur adipiscing consectetur adipiscing elit</Text>
                    //         </View>

                    //     </View>
                    // </View>
                    ))}
                </ScrollView>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    upperRow:{
        width: '90%', 
        flexDirection: 'row', 
        marginTop: 12, 
        justifyContent: 'space-between', 
        alignSelf: "center" 
    },
    cardContainerRight: {
        width: '100%',
        // backgroundColor:"red",
        height: '100%'

    },

    cardContainerLeft: {
        width: '22%',
        // backgroundColor:"green",
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'

    },
    cardContainer: {
        backgroundColor: 'white',//"#AE1371",,
        marginTop:12,
        width: '100%',
        height: 100,
        borderRadius: 15,
        flexDirection: 'row',
        // justifyContent: 'space-between'
    },
    container: {
        // justifyContent:'center',
        // alignItems:'center',
        backgroundColor: "#F8F8F8",
        flex: 1
    },
});

export default RatingReviews;