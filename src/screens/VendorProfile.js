import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon } from 'native-base'
import Avatar from '../../images/avatar.jpg'
import Star from '../../images/star.png'
import Header from '../components/Header'
import Button from '../components/Buttons'
// import Avatar from '../../images/avatar.jpg'

import Cross from '../../images/close.png'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class VendorProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sortVisible: false,
            vendor: null
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="John Doe" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };
    render() {
        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{height:'auto'}}>
                <View style={{
                    // backgroundColor: "#AE1371",
                    borderBottomColor: '#DCDCDC',
                    borderBottomWidth: 1,
                    minHeight: height * 0.3,
                }}>
                    <Image source={Avatar} style={styles.avatar} />
                    <Text style={styles.userName}>John Doe</Text>
                    <View style={styles.ratingContainer}>
                        {[1, 2, 3, 4, 5].map((value, index) => (<Image key={index} source={Star} style={styles.ratingStar} />))}
                    </View>
                </View>
                <ScrollView contentContainerStyle={{minHeight:height*0.7}}>
                <View style={{ width: '90%', alignSelf: 'center', marginVertical: 12 }}>
                    <Text style={{ color: 'black', fontSize: 15, fontWeight: 'bold', marginVertical: 5 }}>About</Text>
                    <Text style={{ color: 'grey', fontSize: 12, marginVertical: 7 }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                    minim veniam, quis nostrud  aliquip ex ea commodo consequat.
                  
                </Text>
                    <Text style={{ color: 'grey', fontSize: 12, marginVertical: 7 }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                        minim veniam, quis nostrud  aliquip 
                    </Text>
                    <Text style={{ color: 'grey', fontSize: 12, marginVertical: 7 }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                        minim veniam, quis nostrud  aliquip ex ea commodo consequat.
                    </Text>
                </View>

                <TouchableOpacity onPress={() => this.props.navigation.navigate("ServicesProvided")}>
                    <View style={styles.itemContainer} >
                        <Text style={styles.itemText} >Services Provided</Text>
                        <Icon name='ios-arrow-forward' style={{ fontSize: 18, margin: 10 }} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("RatingReviews")}>
                <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>Rating and  Reviews</Text>
                    <Icon name='ios-arrow-forward' style={{ fontSize: 18, margin: 10 }} />
                </View>
                </TouchableOpacity>
                </ScrollView>

                </ScrollView>

                <View style={styles.bottom}>
                    <Button 
                        // style={{
                        //     backgroundColor: this.state.vendor === null ? '#EFEFEF' : '#AE1371'
                        // }}
                        // textStyle={{
                        //     color: this.state.vendor === null ? '#888888' : 'white'
                        // }}
                        onPress={() => this.props.navigation.navigate('Portfolio')}
                        title="Confirm Vendor" colored width={"90%"}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: width * 0.9,
        marginRight: "auto",
        marginLeft: "auto",
        borderRadius: 10,
        marginTop: 10,
        backgroundColor: "white"
    },
    itemText: {
        height: "auto",
        fontSize: 16,
        fontWeight: "400",
        margin: 10,
        color: "black"
    },
    ratingStar: {
        height: 10,
        width: 10
    },
    avatar: {
        height: 120,
        width: 120,
        borderRadius: 60,
        borderWidth: 2,
        borderColor: "white",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: height * 0.025
    },
    ratingContainer: {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        flexDirection: "row",
        justifyContent: "center",
        marginBottom : 10
    },
    userName: {
        color: "black",
        textAlign: "center",
        fontSize: 18,
        fontWeight: "bold",
        marginTop: 7.5
    },

    container: {
        // justifyContent:'center',
        // alignItems:'center',
        backgroundColor: "#F8F8F8",
        flex: 1,
        minHeight: height*0.88
    },
    bottom: {
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },

});

export default VendorProfile;