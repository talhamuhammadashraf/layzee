import React ,{Component} from 'react';
import {View,Text,TouchableOpacity,Dimensions} from 'react-native';
import {Agenda,CalendarList} from 'react-native-calendars'
import Header from '../components/Header'
const {width,height} = Dimensions.get('window')
export default class AgendaComponent extends Component{
    static navigationOptions = {
        header: ({ navigation }) => 
        <Header title="2018" goBack={() => navigation.navigate('Starter')} />
    };
    render(){
        return(
            <View style={{height:height}}>
                {/* <CalendarList/> */}
                <Agenda
                    // the list of items that have to be displayed in agenda. If you want to render item as empty date
                    // the value of date key kas to be an empty array []. If there exists no value for date key it is
                    // considered that the date in question is not yet loaded
                    items={
                        {
                            '2018-12-22': [{ text: 'item 1 - any js object' }],
                            '2018-12-23': [{ text: 'item 2 - any js object' }],
                            '2018-12-24': [],
                            '2018-12-25': [{ text: 'item 3 - any js object' }, { text: 'any js object' }],
                        }}
                    // callback that gets called when items for a certain month should be loaded (month became visible)
                    loadItemsForMonth={(day) => 
                        <View><Text>{day}</Text></View>
                        }
                    // callback that fires when the calendar is opened or closed
                    onCalendarToggled={(calendarOpened) => { console.log(calendarOpened) }}
                    // callback that gets called on day press
                    onDayPress={(day) => { console.log('day pressed') }}
                    // callback that gets called when day changes while scrolling agenda list
                    onDayChange={(day) => { console.log('day changed') }}
                    // initially selected day
                    // selected={'2012-05-16'}
                    // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                    // minDate={'2012-05-10'}
                    // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                    // maxDate={'2012-05-30'}
                    // Max amount of months allowed to scroll to the past. Default = 50
                    pastScrollRange={50}
                    // Max amount of months allowed to scroll to the future. Default = 50
                    futureScrollRange={50}
                    // specify how each item should be rendered in agenda
                    renderItem={(item, firstItemInDay) => { return (<View><Text>{item.text}</Text></View>); }}
                    // specify how each date should be rendered. day can be undefined if the item is not first in that day.
                    renderDay={(day, item) => { return (<View style={{backgroundColor:'green',height:height,width:width}}></View>); }}
                    // specify how empty date content with no items should be rendered
                    renderEmptyDate={() => { return (<View style={{height:height,width:width,backgroundColor:'yellow'}}>

                    </View>); }}
                    // specify how agenda knob should look like
                    renderKnob={() => { return (<View style={{width:width,minHeight:height*0.8,backgroundColor:'blue'}}
                    ></View>); }}
                    // specify what should be rendered instead of ActivityIndicator
                    renderEmptyData={() => { return (<View />); }}
                    // specify your item comparison function for increased performance
                    rowHasChanged={(r1, r2) => { return r1.text !== r2.text }}
                    // Hide knob button. Default = false
                    hideKnob={true}
                    // By default, agenda dates are marked if they have at least one item, but you can override this if needed
                    markedDates={{
                        '2012-05-16': { selected: true, marked: true },
                        '2012-05-17': { marked: true },
                        '2012-05-18': { disabled: true }
                    }}
                    // If provided, a standard RefreshControl will be added for "Pull to Refresh" functionality. Make sure to also set the refreshing prop correctly.
                    onRefresh={() => console.log('refreshing...')}
                    // Set this true while waiting for new data from a refresh
                    refreshing={false}
                    // Add a custom RefreshControl component, used to provide pull-to-refresh functionality for the ScrollView.
                    refreshControl={null}
                    // agenda theme
                      theme={{
                    //     ...calendarTheme,
                    //     agendaDayTextColor: 'yellow',
                        agendaDayNumColor: 'green',
                    //     agendaTodayColor: 'red',
                        agendaKnobColor: 'black'
                      }}
                    // agenda container style
                    style={{

                    }}
                />

            </View>
        )
    }
}