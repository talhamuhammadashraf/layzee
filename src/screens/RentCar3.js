import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Header from '../components/Header'
import Button from '../components/Buttons'
import Pen from '../../images/pen.png'
import Cross from '../../images/close.png'
import TextArea from '../components/TextArea'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class RentCar3 extends Component {

    constructor(props) {
        super(props);
        this.state={
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Rent a Car" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={{ height: height * 0.88 }}>
                    <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={styles.circleColor} />
                        <View style={styles.bar} />
                        <View style={styles.circle} />
                        <View style={styles.bar} />
                        <View style={styles.circle} />
                    </View>

                <TouchableOpacity>
                    <View style={{ margin: 12 }} >
                        <View style={styles.innerContainer}>
                            <Text style={styles.itemText}>Add on</Text>
                            <Image source={Pen} style={styles.icon}/>
                        </View>
                    </View>
                </TouchableOpacity>

                    {/* <View style={{ margin: 12 }} >
                    <Text style={styles.itemTitle}>Additional Information</Text>
                        <View style={[styles.innerContainer, { minHeight: 120,height:'auto' ,alignItems:'flex-start'}]}>
                        <TextInput
                        underlineColorAndroid="transparent"
                        multiline={true}
                        placeholder="Type here"
                        style={{width:"90%",marginRight:"auto",marginLeft:"auto"}}
                        />
                        </View>
                    </View> */}

                    <TextArea title="Additional Information" onChangeText={(text)=>console.log(text)}/>

                    <View style={styles.priceContanier}>
                        <Text style={styles.text1}>Price:  300.00 AED</Text>
                        <Text style={styles.text2}>including VAT - 10 AED</Text>
                    </View>

                    <View style={styles.bottom}>
                        <Button title="Next" colored width={"90%"} onPress={()=>this.props.navigation.navigate('RentCar4')}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    container: {
        // justifyContent:'center',
        // alignItems:'center',
        // flex: 1
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleColor: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    innerContainer: {
        width: "90%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,
        justifyContent:"space-between",
        alignItems:"center"

    },
    itemTitle:{
        fontSize:14,
        color:"black",
        width:"80%",
        marginLeft:"auto",
        marginRight:"auto",
        marginBottom:10,
    },
    itemText:{
        color:"black",
        marginLeft:15,
        fontSize:16
    },
    icon:{
        width:20,
        height:20,
        marginRight:15
    },
    bottom:{
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },
    priceContanier:{
        borderTopWidth:1,
        width:"100%",
        // marginTop:15,
        position:"absolute",
        bottom:60,
        borderColor:"#DCDCDC",
        padding:20,
    },
    text1:{
        textAlign:"right",
        fontSize:18,
        fontWeight:"bold",
        color:"#AE1371"

    },
    text2:{
        justifyContent:"flex-end",
        textAlign:"right",
        fontSize:10

    }


});

export default RentCar3;