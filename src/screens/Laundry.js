import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView, TimePickerAndroid } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body } from "native-base";
import Modal from 'react-native-modal'
import ModalDropdown from 'react-native-modal-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Header from '../components/Header'
import Cross from '../../images/close.png'
import Tick from '../../images/tick.png'
import Button from '../components/Buttons'
import Unselect from '../../images/unselect.png'
import Select from '../../images/select.png'
import Pen from '../../images/pen.png'
import DatePicker from 'react-native-datepicker';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Laundry extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal3Visible: false,
            datetime: '2016-05-05 20:00',
            isDateTimePickerVisible: false,


        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header RightIcon={Cross} onRightPress={() => navigation.navigate('Starter')} title="Laundry" goBack={() => navigation.navigate('Starter')} />
    };
    componentWillUnmount() {
        this.setState({ modalVisible: false })
    }
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>

                <Modal isVisible={this.state.modal3Visible}>
                    <View style={[styles.modalContainer, { height: height * 0.77 }]}>
                        <View style={styles.modalTop}>
                            <Text style={styles.modalTitle}>Add ons</Text>
                            <TouchableOpacity onPress={() => this.setState({ modal3Visible: false, "Add on 1": false, "Add on 2": false, "Add on 3": false, "Add on 4": false })}>
                                <Image source={Cross} style={{ height: 20, width: 20, marginRight: 10, marginVertical: 15 }} />
                            </TouchableOpacity>
                        </View>
                        <View>
                            {[
                                { title: "Top", price: '15.00', details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad" },
                                { title: "Dress", price: '15.00', details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad" },
                                { title: "Towel", price: '15.00', details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad" },
                                { title: "Pillow", price: '15.00', details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad" },
                            ]
                                .map((data, index) => (<View style={styles.modal2Row} key={index}>
                                    <TouchableOpacity style={{ width: '10%', height: '100%', justifyContent: 'flex-start', alignItems: 'center', marginRight: 3 }} onPress={() => this.setState((prevState) => ({ [data.title]: !prevState[data.title] }))}>
                                        <Image source={this.state[data.title] ? Select : Unselect} style={{ height: 20, width: 20 }} />
                                    </TouchableOpacity>

                                    <View style={{
                                        width: '90%',
                                        // backgroundColor:'green',
                                        // flexDirection:'row'
                                    }}>
                                        <Text style={{ color: '#AE1371', marginBottom: 0 }}>{data.title}</Text>
                                        <Text style={{ color: '#AE1371', fontSize: 10, color: 'grey', margin: 0 }}>AED {data.price}</Text>
                                        <Text style={{ color: '#AE1371', fontSize: 10, color: 'grey', marginVertical: 10 }}>{data.details}</Text>

                                    </View>
                                </View>))}
                        </View>
                        <TouchableOpacity style={styles.modalButton} onPress={() => this.setState({ modal3Visible: false })}>
                            <Text style={styles.modalButtonText}>Confirm</Text>
                        </TouchableOpacity>

                    </View>
                </Modal>

                <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={styles.circleWhite} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                </View>
                <TouchableOpacity style={[styles.dropDownContainer, { marginTop: 10 }]} onPress={() => this.setState({ modal2Visible: true })} >
                    <Image resizeMode='contain' source={Tick} style={[styles.dropDownOptionImage,
                    { display: this.state['Wind Sheild'] || this.state['Front Windows'] || this.state['Back Windows'] ? 'flex' : 'none' }
                    ]} />
                    <Text style={[styles.headerText, {
                        color: this.state['Wind Sheild'] || this.state['Front Windows'] || this.state['Back Windows'] ? "#AE1371" : 'black'
                    }]}>Bag</Text>
                    <Image resizeMode='contain' source={Pen} style={styles.downImage} />
                </TouchableOpacity>
                <TouchableOpacity style={[styles.dropDownContainer, { marginTop: 10 }]} >
                    <Image resizeMode='contain' source={Tick} style={[styles.dropDownOptionImage,
                    { display: this.state['Add on 1'] || this.state['Add on 2'] || this.state['Add on 3'] || this.state['Add on 4'] ? 'flex' : 'none' }
                    ]} />
                    <Text style={[styles.headerText, {
                        color: this.state['Add on 1'] || this.state['Add on 2'] || this.state['Add on 3'] || this.state['Add on 4'] ? "#AE1371" : 'black'
                    }]}>Press Only</Text>
                    <Image resizeMode='contain' source={Pen} style={styles.downImage} />
                </TouchableOpacity>

                <TouchableOpacity style={[styles.dropDownContainer, { marginTop: 10 }]} >
                    <Image resizeMode='contain' source={Tick} style={[styles.dropDownOptionImage,
                    { display: this.state['Add on 1'] || this.state['Add on 2'] || this.state['Add on 3'] || this.state['Add on 4'] ? 'flex' : 'none' }
                    ]} />
                    <Text style={[styles.headerText, {
                        color: this.state['Add on 1'] || this.state['Add on 2'] || this.state['Add on 3'] || this.state['Add on 4'] ? "#AE1371" : 'black'
                    }]}>Clean and  Press</Text>
                    <Image resizeMode='contain' source={Pen} style={styles.downImage} />
                </TouchableOpacity>

                <TouchableOpacity style={[styles.dropDownContainer, { marginTop: 10 }]} onPress={() => this.setState({ modal3Visible: true })}>
                    <Image resizeMode='contain' source={Tick} style={[styles.dropDownOptionImage,
                    { display: this.state['Add on 1'] || this.state['Add on 2'] || this.state['Add on 3'] || this.state['Add on 4'] ? 'flex' : 'none' }
                    ]} />
                    <Text style={[styles.headerText, {
                        color: this.state['Add on 1'] || this.state['Add on 2'] || this.state['Add on 3'] || this.state['Add on 4'] ? "#AE1371" : 'black'
                    }]}>Add on</Text>
                    <Image resizeMode='contain' source={Pen} style={styles.downImage} />
                </TouchableOpacity>

                <Text style={{ color: 'black', width: '85%', marginRight: 'auto', marginLeft: 'auto', marginTop: 10, fontSize: 15 }}>Additional Information</Text>
                <View style={{ width: width * 0.9, padding: 10, borderRadius: 15, marginTop: 10, height: height * 0.2, backgroundColor: 'white' }}>
                    <TextInput
                        style={{ width: "100%" }}
                        placeholder="Type here"
                        placeholderTextColor='grey'
                    />
                </View>
                <View style={styles.bottomPart}>
                    <View style={styles.pricePart}>
                        <Text style={styles.mainPrice}>
                            Price: 330.00 AED
                    </Text>
                        <Text style={styles.priceDesc}>
                            (Including VAT - 10 AED)
                    </Text>
                    </View>
                    <View style={styles.bottomDiv}>
                        <TouchableOpacity style={styles.bottomSheetButton} onPress={() => this.setState({ modalVisible: true })}>
                            <Text style={{ color: 'white', fontSize: 18 }}>Book Right Now</Text>
                            {/* <Image source={require('../../images/arrow.png')} style={{height:15,width:23,marginLeft:25}}/> */}
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={styles.bottomSheetButtonMinor} onPress={this._showTimePicker}>
                            <Image resizeMode='contain' source={require('../../images/waiting.png')} style={styles.waitingImage} />
                        </TouchableOpacity> */}
                        <DatePicker
                            style={{ width: 200 }}
                            date={this.state.datetime}
                            mode="datetime"
                            format="YYYY-MM-DD HH:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            androidMode='spinner'
                            showIcon={false}
                            ref={(picker) => { this.datePicker = picker; }}
                            TouchableComponent={() => <TouchableOpacity style={styles.bottomSheetButtonMinor} onPress={() => this.datePicker.onPressDate()}>
                                <Image resizeMode='contain' source={require('../../images/red-card.png')} style={styles.waitingImage} />
                            </TouchableOpacity>}
                            onDateChange={(datetime) => { this.setState({ datetime: datetime }); }}
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    selectBox: {
        width: "27.5%",
        height: 30,
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
        borderRadius: 12.5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'center'
    },

    modal2Row: {
        width: '80%',
        minHeight: 40,
        marginTop: 10,
        marginRight: 'auto',
        marginLeft: 'auto',
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: 'row',
        borderBottomColor: '#DCDCDC',
        borderBottomWidth: 1

    },
    modalButton: {
        backgroundColor: '#AE1371',
        width: '100%',
        height: 50,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        position: 'absolute',
        bottom: 0

    },
    modalTitle: {
        color: 'black',
        fontSize: 18,
        width: '80%',
        marginRight: "auto",
        marginLeft: "auto",
        height: 50,
        fontWeight: "bold",
        textAlignVertical: "center"

    },
    modalContainer: {
        width: width * 0.9,
        height: height * 0.7,
        borderRadius: 15,
        backgroundColor: '#F8F8F8',

    },
    modalTop: {
        backgroundColor: "white",
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        width: '100%',
        height: 50,
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    modalButtonText: {
        color: "white",
        textAlign: 'center',
        height: 50,
        textAlignVertical: 'center',
        fontSize: 16

    },
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    bottomSheetButton: {
        flexDirection: 'row',
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 7,
        height: 50,
        width: width * 0.6,
        borderRadius: 15,
        backgroundColor: '#AE1371'
    },
    bottomSheetButtonMinor: {
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        width: width * 0.17,
        borderRadius: 15,
        backgroundColor: 'white'
    },
    dropDownContainer: {
        // position:"absolute",
        flexDirection: 'row',
        width: width * 0.9,
        height: height * 0.07,
        backgroundColor: 'white',
        borderRadius: 13,
        padding: 7,
        alignItems: 'center',
        marginTop: 30,
        zIndex: 0
    },
    checkbox: {
        width: 20,
        height: 20,
    },

    dropDownOptionImage: {
        height: 12,
        width: 16,
        marginLeft: 10
    },
    downImage: {
        height: 15,
        width: 20,
        position: 'absolute',
        right: 20
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginTop: 10,
        paddingBottom: 5,
        alignSelf: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#E3E0DD'
    },
    headerText: {
        fontSize: 16,
        color: 'grey',
        marginLeft: 10
    },
    pricePart: {
        alignItems: 'flex-end',
        marginRight: 15,
        marginTop: 15
    },
    bottomPart: {
        height: 150,
        width: width,
        bottom: 0,
        marginTop: 20,
        borderTopWidth: 1,
        borderTopColor: 'grey',
    },
    mainPrice: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#AE1371'
    },
    priceDesc: {
        fontSize: 12,
        color: 'grey'
    },
    buttonBookLater: {
        width: width * 0.15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#C5C8CC'
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleWhite: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    Arrow: {
        height: 15,
        width: 15
    },
    waitingImage: {
        height: 25,
        width: 25
    },
    bottomDiv: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: width * 0.9,
        alignSelf: 'center',
    },
    itemTitle: {
        fontSize: 12,
        color: "black",
        width: "80%",
        marginLeft: "auto",
        marginRight: "auto",
        marginBottom: 10,
        fontWeight: "bold"
    },
    innerContainer: {
        width: "84%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,

    },
    rightContainer: {
        width: "20%",
        backgroundColor: "#AE1371",
        borderTopRightRadius: 14,
        borderBottomRightRadius: 14,
        height: "100%",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"

    },

});

export default Laundry;