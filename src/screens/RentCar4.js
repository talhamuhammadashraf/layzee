import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import {Icon} from 'native-base'
import Header from '../components/Header'
import Button from '../components/Buttons'
import Cross from '../../images/close.png'
import Plus from '../../images/plus.png'
import Edit from '../../images/edit3.png'
import IDCard from '../../images/DavidCameron.jpg'
import ImagePicker from 'react-native-image-picker';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class RentCar4 extends Component {

    constructor(props) {
        super(props);
        this.state={
            View1Expanded:false,
            View2Expanded:false,
            View3Expanded:false,
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Rent a Car" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };
    selectPhotoTapped(name) {console.log("clicked")
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      takePhotoButtonTitle:"Take Photo",
      chooseFromLibraryButtonTitle:"Choose Existing",
      permissionDenied:{
          title:"Layzee Would Like to Access The Camera",
          text:"Layzee camera use",
          okTitle:"OK"
        },
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        console.log(source,",############")
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          [name]: source
        });
      }
    });
    // console.log(this.state,"this is the state")
  }

    render() {console.log(this.state,"this is  the state")
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={{ minHeight: height * 0.88,height:'auto' }}>
                    <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={styles.circleColor} />
                        <View style={styles.bar} />
                        <View style={styles.circle} />
                        <View style={styles.bar} />
                        <View style={styles.circle} />
                    </View>

                    <View>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Passport /emirates ID</Text>
                                <TouchableOpacity onPress={()=>this.setState((prevState)=>({View1Expanded:!prevState.View1Expanded}))}>
                                <Icon name={this.state.View1Expanded ? 'ios-arrow-up' : 'ios-arrow-down'} 
                                style={{ fontSize: 18, marginRight: 15 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{display:this.state.View1Expanded ? 'flex' : 'none',paddingBottom:20}}>
                            {!this.state.passportFront ? 
                            <TouchableOpacity style={styles.attach} onPress={()=>this.selectPhotoTapped('passportFront')}>
                                <Image source={Plus} style={styles.attachIcon}/>
                                <Text style={{color:"#9F9F9F"}}>Attach frontside</Text>
                            </TouchableOpacity> : 
                                <View style={{borderBottomWidth:0.5,borderColor:'#DCDCDC',marginBottom:5}}>
                                    <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:"center",marginHorizontal:10}}>
                                        <Text style={{color:"#9F9F9F"}}>Emirates ID Card - Front Side</Text>
                                        <Image source={Edit} style={{height:17,width:17}}/>
                                    </View>
                                    <View>
                                        <Image source={this.state.passportFront} style={{width:width*0.7,height:width*0.4,margin:8,borderRadius:10}}/>
                                    </View>
                                </View>
                            }
                            {!this.state.passportBack ? 
                            <TouchableOpacity style={styles.attach} onPress={()=>this.selectPhotoTapped('passportBack')}>
                                <Image source={Plus} style={styles.attachIcon}/>
                                <Text style={{color:"#9F9F9F"}}>Attach backside</Text>
                            </TouchableOpacity> : 
                                <View style={{borderBottomWidth:0.5,borderColor:'#DCDCDC',marginBottom:5}}>
                                    <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:"center",marginHorizontal:10}}>
                                        <Text style={{color:"#9F9F9F"}}>Emirates ID Card - Front Side</Text>
                                        <Image source={Edit} style={{height:17,width:17}}/>
                                    </View>
                                    <View>
                                        <Image source={this.state.passportBack} style={{width:width*0.7,height:width*0.4,margin:8,borderRadius:10}}/>
                                    </View>
                                </View>
                            }
                            </View>
                        </View>
                    </View>
                    <View>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Driving License</Text>
                                <TouchableOpacity onPress={()=>this.setState((prevState)=>({View3Expanded:!prevState.View3Expanded}))}>
                                <Icon name={this.state.View3Expanded ? 'ios-arrow-up' : 'ios-arrow-down'} 
                                style={{ fontSize: 18, marginRight: 15 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{display:this.state.View3Expanded ? 'flex' : 'none',paddingBottom:20}}>
                            {/* <View style={styles.attach}>
                                <Image source={Plus} style={styles.attachIcon}/>
                                <Text style={{color:"#9F9F9F"}}>Attach frontside</Text>
                            </View>
                            <View style={styles.attach}>
                                <Image source={Plus} style={styles.attachIcon}/>
                                <Text style={{color:"#9F9F9F"}}>
                                    Attach backside
                                </Text>
                            </View> */}
                                {!this.state.dlFront ?
                                    <TouchableOpacity style={styles.attach} onPress={() => this.selectPhotoTapped('dlFront')}>
                                        <Image source={Plus} style={styles.attachIcon} />
                                        <Text style={{ color: "#9F9F9F" }}>Attach frontside</Text>
                                    </TouchableOpacity> :
                                    <View style={{ borderBottomWidth: 0.5, borderColor: '#DCDCDC', marginBottom: 5 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: "center", marginHorizontal: 10 }}>
                                            <Text style={{ color: "#9F9F9F" }}>Emirates ID Card - Front Side</Text>
                                            <Image source={Edit} style={{ height: 17, width: 17 }} />
                                        </View>
                                        <View>
                                            <Image source={this.state.dlFront} style={{ width: width * 0.7, height: width * 0.4, margin: 8, borderRadius: 10 }} />
                                        </View>
                                    </View>
                                }
                                {!this.state.dlBack ?
                                    <TouchableOpacity style={styles.attach} onPress={() => this.selectPhotoTapped('dlBack')}>
                                        <Image source={Plus} style={styles.attachIcon} />
                                        <Text style={{ color: "#9F9F9F" }}>Attach backside</Text>
                                    </TouchableOpacity> :
                                    <View style={{ borderBottomWidth: 0.5, borderColor: '#DCDCDC', marginBottom: 5 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: "center", marginHorizontal: 10 }}>
                                            <Text style={{ color: "#9F9F9F" }}>Emirates ID Card - Front Side</Text>
                                            <Image source={Edit} style={{ height: 17, width: 17 }} />
                                        </View>
                                        <View>
                                            <Image source={this.state.dlBack} style={{ width: width * 0.7, height: width * 0.4, margin: 8, borderRadius: 10 }} />
                                        </View>
                                    </View>
                            }
                            </View>
                        </View>
                    </View>
                    <View>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Visa Copy</Text>
                                <TouchableOpacity onPress={()=>this.setState((prevState)=>({View2Expanded:!prevState.View2Expanded}))}>
                                <Icon name={this.state.View2Expanded ? 'ios-arrow-up' : 'ios-arrow-down'} 
                                style={{ fontSize: 18, marginRight: 15 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{display:this.state.View2Expanded ? 'flex' : 'none',paddingBottom:20}}>
                            {/* <View style={styles.attach}>
                                <Image source={Plus} style={styles.attachIcon}/>
                                <Text style={{color:"#9F9F9F"}}>Attach frontside</Text>
                            </View>
                            <View style={styles.attach}>
                                <Image source={Plus} style={styles.attachIcon}/>
                                <Text style={{color:"#9F9F9F"}}>
                                    Attach backside
                                </Text>
                            </View> */}
                                {!this.state.visaFront ?
                                    <TouchableOpacity style={styles.attach} onPress={() => this.selectPhotoTapped('visaFront')}>
                                        <Image source={Plus} style={styles.attachIcon} />
                                        <Text style={{ color: "#9F9F9F" }}>Attach frontside</Text>
                                    </TouchableOpacity> :
                                    <View style={{ borderBottomWidth: 0.5, borderColor: '#DCDCDC', marginBottom: 5 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: "center", marginHorizontal: 10 }}>
                                            <Text style={{ color: "#9F9F9F" }}>Emirates ID Card - Front Side</Text>
                                            <Image source={Edit} style={{ height: 17, width: 17 }} />
                                        </View>
                                        <View>
                                            <Image source={this.state.visaFront} style={{ width: width * 0.7, height: width * 0.4, margin: 8, borderRadius: 10 }} />
                                        </View>
                                    </View>
                                }
                                {!this.state.visaBack ?
                                    <TouchableOpacity style={styles.attach} onPress={() => this.selectPhotoTapped('visaBack')}>
                                        <Image source={Plus} style={styles.attachIcon} />
                                        <Text style={{ color: "#9F9F9F" }}>Attach backside</Text>
                                    </TouchableOpacity> :
                                    <View style={{ borderBottomWidth: 0.5, borderColor: '#DCDCDC', marginBottom: 5 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: "center", marginHorizontal: 10 }}>
                                            <Text style={{ color: "#9F9F9F" }}>Emirates ID Card - Front Side</Text>
                                            <Image source={Edit} style={{ height: 17, width: 17 }} />
                                        </View>
                                        <View>
                                            <Image source={this.state.visaBack} style={{ width: width * 0.7, height: width * 0.4, margin: 8, borderRadius: 10 }} />
                                        </View>
                                    </View>
                                }

                            </View>
                        </View>
                    </View>






                    <View style={styles.bottom}>
                        <Button title="Next" colored width={"90%"}  onPress={()=>this.props.navigation.navigate('ConfirmBooking')}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    container: {
        backgroundColor:'#F8F8F8'
        // justifyContent:'center',
        // alignItems:'center',
        // flex: 1
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleColor: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    innerContainer: {
        width: "100%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,
        justifyContent:"space-between",
        alignItems:"center"

    },
    outerContainer: {
        width: "84%",
        margin:10,
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        // flexDirection: "row",
        height: 'auto',
        borderRadius: 14,
        // justifyContent:"space-between",
        // alignItems:"center"

    },
    itemTitle:{
        fontSize:14,
        color:"black",
        width:"80%",
        marginLeft:"auto",
        marginRight:"auto",
        marginBottom:10,
    },
    itemText:{
        color:"black",
        marginLeft:15,
        fontSize:16
    },
    icon:{
        width:20,
        height:20,
        marginRight:15
    },
    bottom:{
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },
    attach:{
        width:"100%",flexDirection:'row',alignItems:"center",marginTop:10
    },
    attachIcon:{
        height:12,width:12,marginHorizontal:10
    },

    priceContanier:{
        borderTopWidth:1,
        width:"100%",
        // marginTop:15,
        position:"absolute",
        bottom:60,
        borderColor:"#DCDCDC",
        padding:20,
    },
    text1:{
        textAlign:"right",
        fontSize:18,
        fontWeight:"bold",
        color:"#AE1371"

    },
    text2:{
        justifyContent:"flex-end",
        textAlign:"right",
        fontSize:10

    }


});

export default RentCar4;