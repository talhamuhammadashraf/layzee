import React ,{Component} from  'react';
import {View,Text,Dimensions,AsyncStorage,Keyboard,ScrollView,TouchableOpacity} from 'react-native';
const {width,height} = Dimensions.get('window');
import {connect} from 'react-redux'
import PhoneInput from 'react-native-phone-input'
import {CheckBox} from 'native-base'
import Input from '../components/Input'
import Button from '../components/Buttons'
import Header from '../components/Header'
import {LOGIN} from '../store/Middleware/auth'
import Logo from '../../images/logo.png'
import Menu from '../../images/menu.png'
import Back from '../../images/backward.png'
import Cross from '../../images/close.png'

const mapStateToProps = (state) =>({
    state:state
    })
    const mapDispatchToProps = (dispatch) => ({
        LOGIN:(abcd)=>dispatch(LOGIN(abcd))
    })
class SignIn extends Component{
    constructor(){
        super();
        this.state={
        }
    }
    static navigationOptions = {
        header:({navigation}) => <Header  onRightPress={() => navigation.navigate('Starter')} goBack={()=>alert('please authenticate first')}/>
        };    

    componentDidMount () {
        // AsyncStorage.setItem('id','value')
        AsyncStorage.getItem('user',(err,result)=>console.log(err,'bc',result))

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this._keyboardDidShow());
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',() => this._keyboardDidHide());
      }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
    _keyboardDidShow () {
        this.setState({ keyboardOpened: true })
      }
    
      _keyboardDidHide () {
        this.setState({ keyboardOpened: false })
      }

    render(){console.log(this.props.navigation,"state in sign in")
        return(
            <ScrollView>
            <View
            style={{height:height*0.88,backgroundColor:"#F8F8F8"}}
            >
                <Text style={{
                    color:"#AE1371",
                    fontSize:30,
                    fontWeight:'400',
                    marginVertical:10,
                    textAlign:"center"
                    }}>
                Sign in
                </Text>
                <Input
                onChangePhoneNumber={(phone)=>{this.setState({phone:phone})}}
                phone
                isValidNumber={(bool)=>console.log(bool,"ye lou")}
                />
                <Input
                    placeholder="Password"
                    secureTextEntry={true}
                    onChangeText = {(Password)=>this.setState({password:Password})}
                />
                <View style={{
                    flexDirection:"row",
                    width:width*0.9,
                    justifyContent:"space-between",
                    marginRight:"auto",
                    marginLeft:"auto",
                    marginVertical:10}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('ForgetPassword')}>
                    <Text style={{color:"#808080"}}>Forgot Password?</Text></TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('SignUp')}>
                    <Text style={{color:"blue"}}>
                    Click here to register
                    </Text>
                    </TouchableOpacity>
                </View>
                    <Button
                    onPress={() => this.props.LOGIN({
                        user:{phone: this.state.phone,
                        password: this.state.password},
                        navigation:this.props.navigation
                    })
                    }
                    // disabled={true}
                    title="Sign in"
                    colored
                    width={width*0.9}
                    style={{
                        bottom:30,
                        position:'absolute',
                        left:width*0.05,
                        right:width*0.05
                    }}
                    />
            </View>
            </ScrollView>
        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(SignIn)