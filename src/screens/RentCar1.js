import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body } from "native-base";
import Header from '../components/Header'
import Button from '../components/Buttons'
import Modal from 'react-native-modal'
import CalendarPicker from 'react-native-calendar-picker';
import Cross from '../../images/close.png'
import Pen from '../../images/pen.png'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class RentCar1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            selectedStartDate: null,
            selectedEndDate: null,
            type: "From"
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Rent A Car" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={()=>navigation.navigate('Starter')} />
    };
    onDateChange(date, type) {
        if (type === 'END_DATE') {
            this.setState({
                selectedEndDate: date,
            });
        } else {
            this.setState({
                selectedStartDate: date,
                selectedEndDate: null,
            });
        }
    }
    render() {
        const { selectedStartDate, selectedEndDate } = this.state;
        const disabled = selectedStartDate && selectedEndDate ? false : true
        const Days = (Date.parse(selectedEndDate) - Date.parse(selectedStartDate)) / 86400000 + 1
        const minDate = new Date();
        const StringStartDate = this.state.selectedStartDate && new Date(Date.parse(this.state.selectedStartDate)).getDate();
        const StringStartMonth = this.state.selectedStartDate && new Date(Date.parse(this.state.selectedStartDate)).getMonth() + 1;
        const StringStartYear = this.state.selectedStartDate && new Date(Date.parse(this.state.selectedStartDate)).getFullYear()
        const StringEndDate = this.state.selectedEndDate && new Date(Date.parse(this.state.selectedEndDate)).getDate();
        const StringEndMonth = this.state.selectedEndDate && new Date(Date.parse(this.state.selectedEndDate)).getMonth() + 1;
        const StringEndYear = this.state.selectedEndDate && new Date(Date.parse(this.state.selectedEndDate)).getFullYear()
        console.log(StringStartDate && StringStartDate, "str");
        console.log("mcmcm")
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <Modal isVisible={this.state.modalVisible}>
                    <View style={styles.modalContainer}>
                        <View style={{ flexDirection: "row" }}>
                            <View style={[{ backgroundColor: !selectedStartDate ? "#AE1371" : "white", borderTopLeftRadius: 16 }, styles.modalTab]}>
                                <Text
                                    style={[{ color: !selectedStartDate ? "white" : "black", }, styles.modalTabText]}
                                >From</Text>
                            </View>
                            <View style={[{ backgroundColor: selectedStartDate ? "#AE1371" : "white", borderTopRightRadius: 16 }, styles.modalTab]}>
                                <Text
                                    style={[{ color: selectedStartDate ? "white" : "black" }, styles.modalTabText]}
                                >To</Text>
                            </View>
                        </View>
                        <CalendarPicker
                            startFromMonday={false}
                            allowRangeSelection={true}
                            weekdays={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
                            months={['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']}
                            scaleFactor={470}
                            selectedRangeStyle={{ backgroundColor: "#d3a9cc", height: "100%" }}
                            selectedRangeStartStyle={{ backgroundColor: "#AE1371", height: "100%", width: "100%", borderTopLeftRadius: 20, borderBottomLeftRadius: 20 }}
                            selectedRangeEndStyle={{ backgroundColor: "#AE1371", height: "100%", width: "100%", borderTopRightRadius: 20, borderBottomRightRadius: 20 }}
                            todayBackgroundColor="#AE1371"
                            selectedDayColor="#7300e6"
                            selectedDayTextColor="black"
                            todayTextStyle={{ color: "white" }}
                            minDate={minDate}
                            onDateChange={this.onDateChange.bind(this)}
                        />

                        <View style={styles.modalBottomContainer}>
                            <Text
                                style={styles.days}
                            >{`${Days ? Days : 0} Days`}</Text>
                            <View style={styles.bottomContainer}>


                                <TouchableOpacity
                                    style={{ width: '42.5%', marginLeft: 8, marginBottom: 8, borderRadius: 10, height: 40, backgroundColor: "white" }}
                                    onPress={() => this.setState({ modalVisible: false, selectedStartDate: null, selectedEndDate: null })}
                                >
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            alignItems: "center",
                                            height: "100%",
                                            justifyContent: "center"
                                        }}
                                    >
                                        <Text style={{ color: "#9F9F9F", fontSize: 14 }}>Cancel</Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{
                                        width: '42.5%',
                                        marginRight: 8,
                                        marginBottom: 8,
                                        borderRadius: 10,
                                        height: 40,
                                        backgroundColor: disabled ? "#E8E8E8" : '#AE1371',

                                    }}
                                    disabled={disabled}
                                    onPress={() => this.setState({ modalVisible: false })}
                                >
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            alignItems: "center",
                                            height: "100%",
                                            justifyContent: "center"
                                        }}
                                    >
                                        <Text style={{
                                            color: disabled ? "#9F9F9F" : "white",
                                            fontSize: 14,
                                        }}>Done</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={styles.circleColor} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                </View>

                <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                    <View style={{ margin: 12 }} >
                        <View style={styles.innerContainer}>
                            <View style={styles.leftContainer}>
                                <Text style={styles.leftText}>From:</Text>
                                <Text>{StringStartDate ? `${StringStartDate}/${StringStartMonth}/${StringStartYear}` : ' -/-/-'}</Text>
                                <Text>{'    -  '}</Text>
                                <Text style={styles.leftText}>To:</Text>
                                <Text>{StringStartDate ? `${StringEndDate}/${StringEndMonth}/${StringEndYear}` : ' -/-/-'}</Text>
                            </View>
                            <View style={{ flexDirection:'row',alignItems:"center",justifyContent:"center" }}>
                            <Image source={Pen} style={{width:20,height:20,margin:10}}/>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>

                <View style={{ margin: 12 }} >
                    <View style={styles.innerContainer}>
                        <View style={styles.leftContainer}>
                            <Text style={styles.leftText}>Time:</Text>
                            <Text>{'  -  '}</Text>
                        </View>
                        <Image source={Pen} style={{width:20,height:20,margin:10}}/>
                        <View style={{flexDirection:'row',alignItems:"center",justifyContent:'center'}}>
                        </View>
                    </View>
                </View>

                <View style={styles.bottom}>
                    <Button
                        onPress={() => this.props.navigation.navigate('RentCar2')}
                        title="Next" colored width={"90%"}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    container: {
        // justifyContent:'center',
        // alignItems:'center',
        flex: 1
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleColor: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    innerContainer: {
        width: "94%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,

    },
    leftContainer: {
        width: "80%",
        backgroundColor: "white",
        margin: 10,
        flexDirection: "row",
        alignItems: "center"
    },
    leftText: {
        fontWeight: "bold",
        marginLeft: 10
    },
    bottom: {
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },
    modalContainer: {
        width: width * 0.9,
        marginRight: "auto",
        marginLeft: "auto",
        height: "70%",
        backgroundColor: "white",
        borderRadius: 16
    },
    modalTab: {
        width: "50%",
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 12
    },
    modalTabText: {
        fontSize: 16,
        fontWeight: "bold"
    },
    modalBottomContainer: {
        backgroundColor: "#F9F9F9",
        height: "27%",
        bottom: 0,
        borderBottomRightRadius: 16,
        borderBottomLeftRadius: 16,
        position: "absolute",
        width: "100%"

    },
    days: {
        color: "#AE1371",
        textAlign: "center",
        fontSize: 18,
        margin: 15

    },
    bottomContainer: {
        width: "100%",
        flexDirection: 'row',
        height: "60%",
        justifyContent: "space-between"
    }
});

export default RentCar1;