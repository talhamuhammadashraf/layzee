import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Header from '../components/Header'
import Button from '../components/Buttons'
import Pen from '../../images/pen.png'
import Cross from '../../images/close.png'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Wedding1 extends Component {

    constructor(props) {
        super(props);
        this.state={
            hours:0
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Wedding" RightIcon={Cross} onRightPress={() => navigation.navigate('Starter')} goBack={navigation.goBack} />
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={{ height: height * 0.88 }}>

                <TouchableOpacity>
                    <View style={{ margin: 7 }} >
                        <View style={styles.innerContainer}>
                            <Text style={styles.itemText}>No. of hours</Text>
                            <View style={styles.hoursContainer}>
                            <TouchableOpacity 
                            onPress={()=>this.setState((prevState)=>({hours:prevState.hours - 1}))}
                            style={{
                                width:30,
                                height:29,
                                borderRightWidth:1,
                                borderRightColor:'#AE1371',
                                flexDirection:"row",
                                alignItems:"center",
                                justifyContent:"center",

                            }}><Text style={{color:'#AE1371',fontSize:16,fontWeight:"bold"}}>-</Text></TouchableOpacity>
                            <View style={{
                                width:30,
                                height:29,
                                flexDirection:"row",
                                alignItems:"center",
                                justifyContent:"center",
                                borderRightWidth:1,
                                borderRightColor:'#AE1371'
                            }}><Text style={{color:'black',fontSize:16,fontWeight:"bold"}}>{this.state.hours}</Text></View>
                            <TouchableOpacity 
                            onPress={()=>this.setState((prevState)=>({hours:prevState.hours + 1}))}
                            style={{
                                width:30,
                                flexDirection:"row",
                                alignItems:"center",
                                justifyContent:"center",
                                height:29,
                            }}><Text style={{color:'#AE1371',fontSize:16,fontWeight:"bold"}}>+</Text></TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity>
                    <View style={{ margin: 7 }} >
                        <View style={styles.innerContainer}>
                            <Text style={styles.itemText}>Select Date</Text>
                            <Image source={Pen} style={styles.icon}/>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity>
                    <View style={{ margin: 7 }} >
                        <View style={styles.innerContainer}>
                            <Text style={styles.itemText}>Select Time</Text>
                            <Image source={Pen} style={styles.icon}/>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity>
                    <View style={{ margin: 7 }} >
                        <View style={styles.innerContainer}>
                            <Text style={styles.itemText}>Add on</Text>
                            <Image source={Pen} style={styles.icon}/>
                        </View>
                    </View>
                </TouchableOpacity>

                    <View style={{ margin: 7 }} >
                    <Text style={styles.itemTitle}>Additional Information</Text>
                        <View style={[styles.innerContainer, { minHeight: 120,height:'auto' ,alignItems:'flex-start'}]}>
                        <TextInput
                        underlineColorAndroid="transparent"
                        multiline={true}
                        placeholder="Type here"
                        style={{width:"90%",marginRight:"auto",marginLeft:"auto"}}
                        />
                        </View>
                    </View>

                    <View style={styles.priceContanier}>
                        <Text style={styles.text1}>Price:  0.00 AED</Text>
                        <Text style={styles.text2}>including VAT - 10 AED</Text>
                    </View>

                    <View style={styles.bottom}>
                        <Button title="Book Service" colored width={"90%"} onPress={()=>this.props.navigation.navigate('Inspection')}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    hoursContainer:{
        width:90,
        borderColor:'#AE1371',
        borderWidth:1,
        height:30,
        borderRadius:10,
        marginRight:12,
        flexDirection:'row',

    },
    container: {
        backgroundColor:'#F8F8F8'
        // justifyContent:'center',
        // alignItems:'center',
        // flex: 1
    },
    innerContainer: {
        width: "84%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,
        justifyContent:"space-between",
        alignItems:"center"

    },
    itemTitle:{
        fontSize:14,
        color:"black",
        width:"80%",
        marginLeft:"auto",
        marginRight:"auto",
        marginBottom:10,
    },
    itemText:{
        color:"black",
        marginLeft:15,
        fontSize:16
    },
    icon:{
        width:20,
        height:20,
        marginRight:15
    },
    bottom:{
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },
    priceContanier:{
        borderTopWidth:1,
        width:"100%",
        // marginTop:15,
        position:"absolute",
        bottom:60,
        borderColor:"#DCDCDC",
        padding:20,
    },
    text1:{
        textAlign:"right",
        fontSize:18,
        fontWeight:"bold",
        color:"#AE1371"

    },
    text2:{
        justifyContent:"flex-end",
        textAlign:"right",
        fontSize:10

    }


});

export default Wedding1;