import React, { Component } from 'react';
import { View, Image, Keyboard, Text, Dimensions, ScrollView ,TouchableOpacity} from 'react-native';
import {Icon} from 'native-base'
import Button from '../components/Buttons'
import Input from '../components/Input'
import Header from '../components/Header'
import CodeInput from 'react-native-confirmation-code-input';
const { height, width } = Dimensions.get("window");

class FAQ extends Component {
    constructor(props) {
        super(props);
        this.state = {
            View1Expanded: false,
            View2Expanded: false,
            View3Expanded: true,
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="FAQ's" goBack={() => navigation.navigate('Starter')} />
    };

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this._keyboardDidShow());
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this._keyboardDidHide());
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
    }

    _keyboardDidHide() {
    }

    _verifyCode() {
    }

    _onChangeCode() {

    }


    render() {
        return (
            <ScrollView contentContainerStyle={{ backgroundColor: '#F8F8F8' }}>
                <View style={{ height: height - (height * 0.08) }} >
                    <TouchableOpacity>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Question 1 </Text>
                                <TouchableOpacity onPress={() => this.setState((prevState) => ({ View1Expanded: !prevState.View1Expanded }))}>
                                    <Icon name={this.state.View1Expanded ? 'ios-arrow-up' : 'ios-arrow-down'}
                                        style={{ fontSize: 18, marginRight: 15 }} />
                                </TouchableOpacity>
                            </View>
                                <View style={{ display: this.state.View1Expanded ? 'flex' : 'none', paddingBottom: 20 }}>
                                    <Text style={{ color: 'grey', fontSize: 12, width: '80%', alignSelf: 'center', }}>
                                        Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                                </Text>
                                </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Question 2</Text>
                                <TouchableOpacity onPress={() => this.setState((prevState) => ({ View2Expanded: !prevState.View2Expanded }))}>
                                    <Icon name={this.state.View2Expanded ? 'ios-arrow-up' : 'ios-arrow-down'}
                                        style={{ fontSize: 18, marginRight: 15 }} />
                                </TouchableOpacity>

                            </View>
                                <View style={{ display: this.state.View2Expanded ? 'flex' : 'none', paddingBottom: 20 }}>
                                    <Text style={{ color: 'grey', fontSize: 12, width: '80%', alignSelf: 'center', }}>
                                        Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                                </Text>
                                </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Question 3</Text>
                                <TouchableOpacity onPress={() => this.setState((prevState) => ({ View3Expanded: !prevState.View3Expanded }))}>
                                    <Icon name={this.state.View3Expanded ? 'ios-arrow-up' : 'ios-arrow-down'}
                                        style={{ fontSize: 18, marginRight: 15 }} />
                                </TouchableOpacity>

                            </View>
                                <View style={{ display: this.state.View3Expanded ? 'flex' : 'none', paddingBottom: 20 }}>
                                    <Text style={{ color: 'grey', fontSize: 12, width: '80%', alignSelf: 'center', }}>
                                        Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                                </Text>
                                </View>
                        </View>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        )
    }

}

const styles = {
    innerContainer: {
        width: "100%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,
        justifyContent: "space-between",
        alignItems: "center"

    },
    outerContainer: {
        width: "84%",
        margin: 8,
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        // flexDirection: "row",
        height: 'auto',
        borderRadius: 14,
        // justifyContent:"space-between",
        // alignItems:"center"

    },
    itemText: {
        color: "black",
        marginLeft: 15,
        fontSize: 16
    },
}

export default FAQ;