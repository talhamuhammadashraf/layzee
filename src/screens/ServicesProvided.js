import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Header from '../components/Header'
import Cross from '../../images/close.png'
import {createTabNavigator} from 'react-navigation'
import Tinting from '../../images/tinting.png'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;



class ServicesProvided extends Component{
    constructor(){
        super();
        this.state={
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Services Provided" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };
    render(){
    return(
    <ScrollView style={{ backgroundColor: '#F8F8F8', minHeight: height * 0.88 }}>

        <View style={{
        width:width,
        marginTop:10,
        flexDirection:'row',
        flexWrap:'wrap',
        justifyContent:"space-between"
            // alignItems:""
            }}>{
            [1,2,3,4,5,6,7,8]
        .map((data,index)=>(<View key={index} style={{
            height:width*0.425,
            width:width*0.425,
            borderRadius:12,
            marginHorizontal:width*0.0375,
            marginVertical:10,
            backgroundColor:'white',
            justifyContent:'center'
        }}>
        <Image source={Tinting} style={{
            height:50,
            width:110,
            alignSelf:"center",
            margin:15

            }}/>
        <Text style={{
            textAlign:"center",
            color:"black",
            fontSize:15,
            margin:10
            }}>Window Tinting</Text>
        </View>
        ))}
        </View>
    

    </ScrollView>
)}}


const styles = StyleSheet.create({

});

export default ServicesProvided;