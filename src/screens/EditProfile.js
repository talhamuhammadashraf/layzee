import React,{Component} from 'react';
import {View,Text,Image,Dimensions,ScrollView,TouchableOpacity,TextInput} from 'react-native';
import {connect} from 'react-redux'
import {UPDATE_PROFILE} from '../store/Middleware/profile'
import Avatar from '../../images/avatar.jpg'
import Camera from '../../images/camera.png'
import Home from '../../images/home3.png'
import Work from '../../images/work3.png'
import Car from '../../images/car3.png'
import Arrow from '../../images/arrow3.png'
import Header from '../components/Header'


const {width} = Dimensions.get('window').width;
const {height} = Dimensions.get('window')
const mapStateToProps = (state) => ({
    profile:state.profile
})
const mapDispatchToProps = (dispatch) => ({
    UPDATE_PROFILE:(abcd)=>dispatch(UPDATE_PROFILE(abcd))
})

let _this = null;
class EditProfile extends Component{
    constructor(){
        super();
        this.state={};
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Edit Profile" textRight="Save" onRightPress={() => 
            _this.props.UPDATE_PROFILE({
            id:_this.state.user.id,
            user:{..._this.state.user,
                name:_this.state.name || _this.state.user.name,
                phone:_this.state.phone  || _this.state.user.phone,
                email:_this.state.email  || _this.state.user.email,
            },
            navigation:_this.props.navigation
        })
    } goBack={navigation.pop} />
        }; 
        componentDidMount(){
    _this = this;

            this.props.profile && 
            this.setState({
                user:this.props.profile.user
            })    
        }
    render(){console.log(this.state,"hi")
        return(
            // <View><Text>hi</Text></View>
            <ScrollView 
            // contentContainerStyle={{minHeight:height*1.12}}
            >
            <View style={{ flex: 1 ,backgroundColor:'#F8F8F8',minHeight:height}}>
                <View style={styles.topContainer}>
                    <View style={styles.profileContainer}>
                        <Image source={Avatar} style={styles.avatar} />
                        <View
                            style={styles.cameraContainer}>
                            <Image source={Camera}
                                style={styles.camera}
                            />
                        </View>
                    </View>
                </View>
                {
                    [
                        {name:"name",value:this.state.user ? this.state.user.name : 'Username'},
                        {name:"email",value:this.state.user ? this.state.user.email : 'Email'},
                        {name:"phone",value:this.state.user ? this.state.user.phone : 'Phone'},
                        // {name:"Password",value:"Efw535353"},
                        // {name:"New Password"},
                        // {name:"Confirm Password"},
                ].map((value,index)=>(
                    <View style={styles.itemContainer} key={index}>
                    <Text style={[styles.itemHeader, { width: "70%", marginLeft: "auto", marginRight: "auto" }]}>{value.name}</Text>
                    <TextInput style={styles.itemText} defaultValue={value.name !== "email" ? value.value && value.value : null} value={value.name === "email" ? value.value : null } underlineColorAndroid="transparent" 
                    // secureTextEntry={value.name === 'Password' || value.name === 'New Password' || value.name === 'Confirm Password' ? true : false}
                    onChangeText={(text)=>this.setState({[value.name]:text})}
                    />
                    </View>
                ))
                }

                    {
                        this.state.user && this.state.user.UserAddresses && this.state.user.UserAddresses.length ?
                    this.state.user.UserAddresses.map((data,index)=>(

                        <View style={styles.itemIconContainer} key={index}>
                        <View style={{ width: "15%" }}>
                            <Image source={Home} style={styles.icon} /></View>
                        <Text style={[styles.itemHeader,{width:"70%"}]}>{data.house_no},{data.street_no},{data.state}</Text>
                        <View style={{ width: "15%" }}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate("AddOrEditAddress",{id:data.id})}>
                        <Image source={Arrow} style={[styles.icon,{height:15,width:15}]} /></TouchableOpacity>
                        </View>
                    </View>
                        ))
                    :
                    <View style={styles.itemIconContainer}>
                    <View style={{ width: "15%" }}>
                        <Image source={Home} style={styles.icon} /></View>
                    <Text style={[styles.itemHeader,{width:"70%"}]}>Add Adddress</Text>
                    <View style={{ width: "15%" }}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("AddOrEditAddress")}>
                    <Image source={Arrow} style={[styles.icon,{height:15,width:15}]} /></TouchableOpacity>
                    </View>
                </View>
                }

                    {/* <View style={styles.itemIconContainer}>
                        <View style={{ width: "15%" }}>
                            <Image source={Work} style={styles.icon} /></View>
                        <Text style={[styles.itemHeader,{width:"70%"}]}>Office D53,Block A, Area no 25</Text>
                        <View style={{ width: "15%" }}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate("AddOrEditCar")}>
                        <Image source={Arrow} style={[styles.icon,{height:15,width:15}]} /></TouchableOpacity>
                        </View>
                    </View> */}

                    <View style={{
                        // paddingTop:15,paddingBottom:15,borderTopWidth:1,
                        borderBottomWidth:1,borderColor:"#DCDCDC"}}>
                    {
                        this.state.user && this.state.user.UserAddresses &&
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("AddOrEditAddress")}
                    // style={{width:'70%',marginLeft:"auto",marginRight:"auto",flexDirection:"row",alignItems:"center",height:50}}
                    >
                    <Text style={{
                        color:"#0953f2",
                        paddingTop:5,
                        marginBottom:11.5,
                        width:"70%",
                        marginRight:"auto",
                        marginLeft:"auto"
                        }}>Add another location</Text>
                        </TouchableOpacity>
                        }
                    {/* <View style={{flexDirection:"row",alignItems:"center"}}>
                        <View style={{ width: "15%" }}>
                            <Image source={Car} style={[styles.icon,{width:30}]} /></View>
                        <Text style={[styles.itemHeader,{width:"70%"}]}>2015,BMW M4 Convertible, TC-6521</Text>
                        <View style={{ width: "15%" }}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate("AddOrEditCar")}>
                        <Image source={Arrow} style={[styles.icon,{height:15,width:15}]} />
                        </TouchableOpacity>
                        </View>
                    </View> */}
                    </View>

                        <View style={{width:'70%',marginLeft:"auto",marginRight:"auto",flexDirection:"row",alignItems:"center",height:50}}>
                            <Text style={{color:"#0953f2"}}>
                                Add another Car
                            </Text>
                        </View>

            </View>
            </ScrollView>
        )
    }
}

const styles = {
    avatar:{
        height:100,
        width:100,
        borderRadius:50,
        borderWidth:2,
        borderColor:"white",
        marginLeft:"auto",
        marginRight:"auto",
        marginTop:height*0.075,
        marginBottom : -10
    },
    ratingContainer:{
        width:"50%",
        marginLeft:"auto",
        marginRight:"auto",
        flexDirection:"row",
        justifyContent:"center"
    },
    userName:{
        color:"black",
        textAlign:"center",
        fontSize:18,
        fontWeight:"bold",
        marginTop:7.5
    },
    ratingStar:{
        height:10,
        width:10
    },
    topContainer:{
        height: height * 0.35,
        padding:40,
    },
    profileContainer:{
        height: 100, 
        width: 100, 
        marginRight: "auto", 
        marginLeft: "auto"
    },
    cameraContainer:{
        height: 30, 
        width: 30, 
        borderRadius: 15, 
        backgroundColor: 'white', 
        marginTop: -15, 
        marginLeft: 60
    },
    camera:{
        height: 20, 
        width: 20, 
        borderRadius: 10, 
        margin: 5
    },
    itemContainer:{
        borderColor:"#DCDCDC",
        borderTopWidth:1,
        height:70,
        paddingTop:15,
        paddingBottom:16
    },
    itemIconContainer:{
        borderColor:"#DCDCDC",
        borderTopWidth:1,
        height:70,
        alignItems:"center",
        flexDirection:"row"
    },
    itemHeader:{
        color:"black",
        fontSize:16
    },
    icon:{
        height:20,
        width:20,
        marginRight:"auto",
        marginLeft:"auto"
    },
    itemText:{
        color:"#bcb8b8",
        fontSize:12,
        width:"70%",
        marginLeft:"auto",
        marginRight:"auto"
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(EditProfile);