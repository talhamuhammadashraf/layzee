import React,{Component} from 'react';
import {View,StyleSheet,Text,Modal,TouchableOpacity,AsyncStorage,TouchableWithoutFeedback,PermissionsAndroid,TouchableHighlight,ScrollView,Dimensions,Image,Alert,TouchableNativeFeedback} from 'react-native';
import MapView, {  } from 'react-native-maps';
import Carousel from 'react-native-banner-carousel';
import { TabView,TabBar ,SceneMap,PagerPan } from 'react-native-tab-view';
import ModalReact from "react-native-modal";
import {NavigationActions} from 'react-navigation';
import Location from '../../images/mapsPurple.png' 
import Down from '../../images/Down.png'
import Header from '../components/Header'
import Mylocation from '../../images/mylocation.png'
import { Container, Content, Accordion } from "native-base";
import Dropdown from '../components/Dropdown';

var startPoint;
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const content = [
    "Earn rewards NOW!",
    "15% off on first service",
    "Bank on our biggest deal",
    "Get all your feedback"
];

const dataArray = [
    { title: "First Element", content: "Lorem ipsum dolor sit amet" }
  ];

class DashBoard extends Component {

constructor(props){
super(props);
this.state = {
    carSelect:'Current Location',
    region: {
        latitude: 24.9418733,
        longitude: 67.1121096,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      index: 0,
        routes: [
        { key: 'first', title: 'Vehicle',icon: require('../../images/car.png') },
        { key: 'second', title: 'Home', icon: require('../../images/home.png') }, 
        { key: 'third', title: 'Personal', icon: require('../../images/personal.png') },
        { key: 'fourth', title: 'Others', icon: require('../../images/other.png') },
        { key: 'fifth', title: 'Featured', icon: require('../../images/seat.png')},
        { key: 'sixth', title: 'Annoucements', icon: require('../../images/battery.png')}
        ],
      markers: [
        {
            coordinate: {
                latitude: 24.9418733,
                longitude: 67.1121096
            },
            title: "Best Place",
            description: "Description1",
            id: 1
        },
        // {
        //     coordinate: {
        //         latitude: 24.9438733,
        //         longitude: 67.1131096
        //     },
        //     title: "Best Place2",
        //     description: "Description 2",
        //     id: 2
        // }
    ],
    modalVisible:false,
    dropDownCurrentLocation:false,
            currentLocationHeader:{
                title:'Current location',
                id:0
            },
}
}
static navigationOptions = {
    header:({navigation}) => <Header menu openDrawer={navigation.openDrawer}/>
    };    

async componentWillMount(){
    await navigator.geolocation.getCurrentPosition(
      (position) => {
         startPoint = {
          longitude: position.coords.longitude,
          latitude: position.coords.latitude
        }
      },
      (error) => this.setState({ error: error.message}),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
    // this.scroller.scrollTo({x:380,y:0})
  }

onRegionChange(region) {
    this.setState({ region });
  }

  renderPage(banner, index) {
    return (
        <View style={[styles.banner,{display:this.state.modalVisible ? "none" : 'flex'}]} key={index}>
                <Text style={{color:'#AE1371',fontSize:26,fontWeight:'normal',textAlign:'center'}}>{banner}</Text>
        </View>
    );
}

openBottomSheet(){
this.setState({
    modalVisible:true
});
}

setModalVisible(){
    this.setState({
        modalVisible:false,
        index:2
    })
}
componentWillUnmount(){
    this.setModalVisible()
}
_renderHeader(title, expanded) {
    return (
      <View
        style={{ flexDirection: "row", padding: 10, justifyContent: "space-between", alignItems: "center", backgroundColor: "#A9DAD6" }}
      >
        <Text style={{ fontWeight: "600" }}>
          {" "}{title}
        </Text>
        
      </View>
    );
  }

  _renderHeader() {
    return (
      <View style={styles.headerAccordian}>
          <Text style={{fontSize:18,color:'grey',marginLeft:20}}>Current Location</Text>
          <Image source={Down} style={{height:15,width:15}}/>
          <View style={{width:18,borderWidth:1,borderColor:'black',height:18,marginRight:20,borderRadius:14,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
              <Text>+</Text>
          </View>
      </View>
    );
  }

  _renderContent() {
    return (
      <View style={styles.contentAccordian}>
            <TouchableOpacity>
                <Text style={styles.contentText}>Address No. 1</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={styles.contentText}>Address No. 2</Text>
            </TouchableOpacity>
            <View style={{height:1,width:"90%",backgroundColor:'grey',marginTop:10,alignSelf:'center'}}/>
            <TouchableOpacity>    
                <Text style={styles.addAnother}>Add another Address</Text>
            </TouchableOpacity>
      </View>
    );
  }

  navigateToScreen = ( route ) =>(
    () => {
    const navigateAction = NavigationActions.navigate({
        routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
})

_renderIcon = ({ route }) => {
    <Image source={""} resizeMode='contain' style={{width:40,height:30}} />
}

_renderContentSelectCar(o,i,is) {
    let imageView;
    if(o === 'Add new Car'){
        imageView = require('../../images/plus.png')
    }else{
        imageView = require('../../images/car.png')
    }
    return(
        <View style={styles.contentSelectCar}>
            <View style={styles.onePart} onPress={() => this.setState({carSelect:'Mercedes E250'})}>
                <Image resizeMode='contain' source={imageView} style={styles.carSize}/>
                <Text style={styles.contentWithImage}>{o}</Text>
            </View>
        </View>
    )
}

_renderHeaderSelectCar = () => {
    return(
        <View style={styles.headerStyleAccordian}>
            <Text style={styles.headerText}>{this.state.carSelect}</Text>
            <Image resizeMode='contain' source={require('../../images/Down.png')} style={styles.Arrow}/>
        </View>
    )
}

_renderTabBar = props => {
    return (
        <TabBar
        {...props}
        indicatorStyle={{ backgroundColor: 'black' }}
        style={{marginTop:10,backgroundColor:'white'}}
        scrollEnabled={true}
        bounces={true}
        labelStyle={{color:'black'}}
        tabStyle={{width:150,paddingTop:7}}
        renderIcon={
            ({route,index}) =>
             <Image key={index} source={route.icon} resizeMode='contain' style={{width:50,height:25}}/>
            }
      />
    );
  };

  _renderPager = props => {
      return (
        <PagerPan
        {...props}
        animationEnabled={true}
        swipeEnabled={true}
        />
      );
  }

  renderDropdownCurrentLocation = () => {
    let {title,id} = this.state.currentLocationHeader;
    let add = this.state.dropDownCurrentLocation ? require('../../images/up.png') : require('../../images/Down.png');
    let {downImage} = styles;
    return(
        <TouchableOpacity style={styles.dropDownContainer} onPress={() => this.setState(prevState => ({
            dropDownCurrentLocation: !prevState.dropDownCurrentLocation,
          }))}>
            
            <Text style={styles.headerText}>{title}</Text>
            <Image resizeMode='contain' source={add} style={downImage}/>
        </TouchableOpacity>
    )
  }

  valueSelected(dataArr){
    this.setState({
        currentLocationHeader:dataArr,
        dropDownCurrentLocation:false
    });
}

renderDropdownOptionsCurrentLocation = () => {
    let {dropDownOptionImage} = styles;
    if(this.state.dropDownCurrentLocation === true){
        return(
            <View style={styles.dropDownOptionContainer}>
                {/* <Text style={styles.optionsGuide}>Select Car</Text> */}
                <TouchableOpacity style={styles.row} onPress={()=>this.valueSelected({title:'Address 1',id:1})}>
                    <Image resizeMode='contain' source={require('../../images/address.png')} style={dropDownOptionImage}/>
                    <Text style={styles.headerText}>Address 1</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.row} onPress={()=>this.valueSelected({title:'Address 2',id:2})}>
                    <Image resizeMode='contain' source={require('../../images/address.png')} style={dropDownOptionImage}/>
                    <Text style={styles.headerText}>Address 2</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.row}>
                    <Image resizeMode='contain' source={require('../../images/plus.png')} style={dropDownOptionImage}/>
                    <Text style={styles.headerText}>Add new Address</Text>
                </TouchableOpacity>
            </View>
            
        )
    }else{
        return <View/>
    }
    
}
  

render(){console.log(this.props.navigation)
    const FirstRoute = () => (
        <View style={styles.container2}>
            <View style={{width:width,height:height*0.4,marginTop:10,justifyContent:'space-around'}}>
                <View style={styles.contentContainer}>

                    <ScrollView contentContainerStyle={styles.linearGradient}>
                        <TouchableOpacity activeOpacity={.7} style={styles.scrollablePart}>
                            

                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <View>
                                <Image resizeMode='contain' source={require('../../images/tinting.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Window{"\n"}Tinting</Text>
                            </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <View>
                            <Image resizeMode='contain' source={require('../../images/seat.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Interior{"\n"}Cleaning</Text>
                            </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <View>
                            <Image resizeMode='contain' source={require('../../images/battery.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Car{"\n"}Battery</Text>
                            </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('CarWashDetails')} style={styles.mainSubCategory}>
                            <View>
                            <Image resizeMode='contain' source={require('../../images/carWash.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Car{"\n"}Wash</Text>
                            </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <View>
                            <Image resizeMode='contain' source={require('../../images/carRepair.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Car{"\n"}Repair</Text>
                            </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('RentCar1')} style={styles.mainSubCategory}>
                            <View>
                            <Image resizeMode='contain' source={require('../../images/carRent.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Rent{"\n"}A Car</Text>
                            </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <View>
                            <Image resizeMode='contain' source={require('../../images/carCare.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Car{"\n"}Care</Text>
                            </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <View>
                            <Image resizeMode='contain' source={require('../../images/carService.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Car{"\n"}Service</Text>
                            </View>
                            </TouchableHighlight>
                            <TouchableHighlight underlayColor="#AE1371" onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <View>
                            <Image resizeMode='contain' source={require('../../images/tyreChange.png')} style={styles.subCategoryIcon}/>
                                <Text style={styles.subServiceName}>Tyre{"\n"}Change</Text>
                            </View>
                            </TouchableHighlight>
{/* // --------------------------------- */}
                            <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.navigateToScreen('RentCar1')} style={styles.mainSubCategory}>
                            </TouchableOpacity>


                            <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            </TouchableOpacity>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>
        </View>
      );
      const SecondRoute = () => (
        <View style={styles.container2}>
        <TouchableOpacity onPress={() => this.setModalVisible()} style={{width:width,height:height*0.55}}/>
        <View style={{width:width,height:height*0.4,marginTop:10,backgroundColor:'#FFFFFF'}}>
            <ScrollView contentContainerStyle={styles.contentContainer}>

                <View style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/businessServices.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Business{"\n"}Services</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('PrintReceipt')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/cleaning.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Cleaning</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('Laundry')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/laundry.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Laundary</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/homeDesign.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Design</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/gardening.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Garden</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/pesticide.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Pest{"\n"}Control</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/renovation.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Refurbishing</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/homeMaintenance.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Maintenance</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/pool.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Pool</Text>
                        </TouchableOpacity>
                    </View>
            </ScrollView>
        </View>
    </View>
      );
      const ThirdRoute = () => (
        <View style={styles.container2}>
        <TouchableOpacity onPress={() => this.setModalVisible()} style={{width:width,height:height*0.55}}/>
        
        <View style={{width:width,height:height*0.4,marginTop:10,backgroundColor:'#FFFFFF'}}>
            <ScrollView contentContainerStyle={styles.contentContainer}>

                <View style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/beauty.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Beauty</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/fitness.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Fitness</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/homePersonal.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Home{"\n"}Personal</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/personalDevelopment.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Personal{"\n"}Development</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tailoring.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Tailoring</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/dentistry.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Dentistry</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/exercise.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Exercise</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tutoring.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Tutoring</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/cooking.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Cooking</Text>
                        </TouchableOpacity>
                    </View>
            </ScrollView>
        </View>
    </View>
      );
      const FourthRoute = () => (
        <View style={styles.container2}>
        <TouchableOpacity onPress={() => this.setModalVisible()} style={{width:width,height:height*0.55}}/>
        
        <View style={{width:width,height:height*0.4,marginTop:10,backgroundColor:'#FFFFFF'}}>
            <ScrollView contentContainerStyle={styles.contentContainer}>

                <View style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/adminServices.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Admin{"\n"}Services</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/events.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Events</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/insurance.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Insurance</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/repair.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Repair</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/electronicRepair.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Electronic{"\n"}Repair</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/vet.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Vet</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tailoring2.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Tailoring</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/paint.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Paint</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/grocery.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Grocery</Text>
                        </TouchableOpacity>
                    </View>
            </ScrollView>
        </View>
    </View>
      );


      const FIfthRoute = () => (
        <View style={styles.container2}>
        <TouchableOpacity onPress={() => this.setModalVisible()} style={{width:width,height:height*0.55}}/>
        
        <View style={{width:width,height:height*0.4,marginTop:10,backgroundColor:'#FFFFFF'}}>
            <ScrollView contentContainerStyle={styles.contentContainer}>

                <View style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tinting.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Window{"\n"}Tinting</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/seat.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Interior{"\n"}Cleaning</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/battery.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Car{"\n"}Battery</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tinting.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Window{"\n"}Tinting</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/seat.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Interior{"\n"}Cleaning</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/battery.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Car{"\n"}Battery</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tinting.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Window{"\n"}Tinting</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/seat.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Interior{"\n"}Cleaning</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/battery.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Car{"\n"}Battery</Text>
                        </TouchableOpacity>
                    </View>
            </ScrollView>
        </View>
    </View>
      );

      const SixthRoute = () => (
        <View style={styles.container2}>
        <TouchableOpacity onPress={() => this.setModalVisible()} style={{width:width,height:height*0.55}}/>
        
        <View style={{width:width,height:height*0.4,marginTop:10,backgroundColor:'#FFFFFF'}}>
            <ScrollView contentContainerStyle={styles.contentContainer}>

                <View style={styles.linearGradient}>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tinting.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Window{"\n"}Tinting</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/seat.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Interior{"\n"}Cleaning</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/battery.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Car{"\n"}Battery</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tinting.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Window{"\n"}Tinting</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/seat.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Interior{"\n"}Cleaning</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/battery.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Car{"\n"}Battery</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/tinting.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Window{"\n"}Tinting</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/seat.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Interior{"\n"}Cleaning</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('WindowTinting')} style={styles.mainSubCategory}>
                            <Image resizeMode='contain' source={require('../../images/battery.png')} style={styles.subCategoryIcon}/>
                            <Text style={styles.subServiceName}>Car{"\n"}Battery</Text>
                        </TouchableOpacity>
                    </View>
            </ScrollView>
        </View>
    </View>
      );

      const FirstIcon = () => {
          <Image source={require('../../images/car.png')} resizeMode="contain" style={{height:30,width:40}}/>
      }

    return(
        <View style={styles.container}>
        <View 
        // onPress={()=>this.setState({modalVisible:false})}
        >
        {/* <ModalReact
        animationIn="slideInUp"
        animationOut="slideOutDown"
        avoidKeyboard={true}
        isVisible={this.state.modalVisible}
        onBackButtonPress={() => {
            this.setModalVisible();
        }}
        onBackdropPress={() => this.setModalVisible() }
        swipeDirection="down"
        hideModalContentWhileAnimating={true}
        onSwipe={() => this.setModalVisible()}
        backdropColor = {null}
        style={{margin:null}}
        > */}
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={this.setModalVisible
            }
        >
        <TouchableOpacity style={{width:width,flexDirection:'row',justifyContent:'center',marginTop:height*0.2,height:height*0.25}}
        onPress={()=>this.setModalVisible()}><Image source={require('../../images/close.png')}/></TouchableOpacity>
        <TabView
        navigationState={this.state}
        onIndexChange={index => this.setState({ index })}
        renderScene={SceneMap({
            first: FirstRoute,
            second: SecondRoute,
            third: ThirdRoute,
            fourth: FourthRoute,
            fifth: FIfthRoute,
            sixth: SixthRoute
        })}
        tabBarPosition="top"
        renderTabBar={this._renderTabBar}  
             
        initialLayout={ {width:width,height:height*0.5,backgroundColor:'#0C4A9E' }}
        />
    </Modal>
    </View>
            <MapView
            ref={map => this.map = map}
            showsMyLocationButton={true}
            showsCompass={true}
            showsUserLocation={true}
            loadingEnabled={true}
            initialRegion={this.state.region}
            style={[styles.containerMap,{paddingTop: this.state.paddingTop}]}
                onMapReady={() => PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                    .then(granted => {
                        this.setState({ paddingTop: 0 });
                    })}
            >
            {this.state.markers.map((marker)  =>
            (  
                <MapView.Marker
                    key={marker.id}
                    coordinate={marker.coordinate}
                    image={Location}
                    draggable
                    title={marker.title}
                    description={marker.description}
                />))
            }
            </MapView>
            
            <View style={{alignSelf:'center',flexDirection:'row',width:width*3,position:'absolute',top:height*0.11,alignItems:'center',height:'auto',justifyContent:'center'}}>
                <Carousel
                        autoplay
                        autoplayTimeout={10000}
                        loop
                        index={1}
                        pageSize={width*0.9}
                        pageIndicatorContainerStyle	={{height:'auto',width:'auto'}}
                        pageIndicatorStyle={{backgroundColor:'white',height:10,width:10,borderRadius:5,borderColor:'#AE1371',borderWidth:1}}
                        activePageIndicatorStyle={{backgroundColor:'#AE1371',}}
                        pageIndicatorOffset={20}
                    >
                        {content.map((banner, index) => this.renderPage(banner, index))}
                </Carousel>
            </View>

            {this.renderDropdownCurrentLocation()}
            {this.renderDropdownOptionsCurrentLocation()}
            <TouchableOpacity
            onPress={(position)=>{
                navigator.geolocation.getCurrentPosition((position)=>{
                    const {longitude,latitude} = position.coords
                    this.setState({
                        region:{
                            latitude:latitude,
                            longitude:longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                    
                        }
                    })
                })
            }}
            style={{
                bottom:70,
                right:width*0.05,
                position:'absolute'

            }}
            >
            <Image source={Mylocation} style={{
                height:100,
                width:100,
                }}/>
            </TouchableOpacity>
                <TouchableOpacity style={styles.bottomSheetButton} onPress={()=> this.openBottomSheet()}>
                    <Text style={{color:'white',fontSize:18,alignSelf:'center',marginLeft:width*0.17}}>Request a Service</Text>
                </TouchableOpacity>
            
        </View>
    );
}
}


const styles = StyleSheet.create({
    containerMap:{
        position:'absolute',
        width:'100%',
        height:'100%',
        margin:10,
        // alignSelf:"center",
        // justifyContent:'center',
        // alignItems:"center"
        top:0,
        bottom:0,
        right:0,
        left:0,
    },
    currentLocation:{
        width:"90%",
        height:'auto',
        flexDirection:'row',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        zIndex:200
    },
    carSize:{
        height:15,
        width:23
    },
    contentWithImage:{
        marginLeft:10,
        fontSize:14
    },
    onePart:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        width:"100%",
        padding:10,
        paddingLeft:7,
        paddingRight:7
    },
    contentSelectCar:{
        width:width*0.9,
        height:'auto',
        marginTop:5,
        padding:1,
        backgroundColor:'white',
        borderRadius:15,
        alignItems:'center',
        justifyContent:'center'
    },
    headerText:{
        fontSize:16,
        color:'black',
        marginLeft:10,
    },
    dropdownHead:{
        alignSelf:'center',
        marginTop:10,
        width:width*0.9,
        height:52,
        backgroundColor:'white',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        borderRadius:15
    },
    container:{
        flex:1
    },  
    contentContainer:{
        width:width,
        height:'auto'
    },
    linearGradient:{
        flexDirection:'row',
        flexWrap:'wrap',
        width:"100%",
        backgroundColor:'#f5f5f5',
        paddingTop:10,
        justifyContent:'center',
        alignSelf:'center',
    },
    map:{
        position:'absolute',
        top:0,
        left:0,
        right:0,
        bottom:0,
    },
    headerAccordian:{
        width:"90%",
        height:'auto',
        flexDirection:'row',
        alignSelf:'center',
        paddingTop:15,
        paddingBottom:15,
        alignItems:'center',
        borderRadius:15,
        justifyContent:'space-between',
        marginTop:10,
        backgroundColor: 'white',
        zIndex:200
    },
    subServiceName:{
        fontSize:12,
        textAlign:'center',
        marginTop:2,
        color:'black'
    },
    firstOfThree:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    secondOfThree:{
        flex:2,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    thirdOfThree:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    container2:{
        flex:1,
        width:width,
        alignItems:'flex-end',
        justifyContent:'flex-end'
      },
    members:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft:10,
        marginRight:10,
        paddingRight:10,
        backgroundColor: 'rgba(52, 52, 52, 0.4)',
        height: height*0.06,
        width:width*0.9,
        marginTop: 10,
        alignSelf:'center',
        borderRadius:5
    },
    bottomSheetButton:{
        position:'absolute',
        bottom:20,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignSelf:'center',
        padding:14,
        width:width*0.8,
        borderRadius:15,
        backgroundColor: '#AE1371'
    },
    banner:{
        flexDirection: 'row',
        // display:1==1 ? "none" : "block",
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:10,
        marginTop:20,
        zIndex:-10,
        paddingRight:10,
        backgroundColor: 'white',
        height: height*0.19,
        width:width*0.9,
        marginTop: 10,
        alignSelf:'center',
        borderRadius:25
    },
    dropStyle:{
        width:width*0.9,
        height:'auto',
        top:50,
        padding:3,
        backgroundColor:'white',
        flexDirection:'row',
        justifyContent:'flex-start',
        borderRadius:15
    },
    bannerMain:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:10,
        alignSelf:'center',
        paddingRight:10,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        height: height*0.1,
        width:width,
        borderRadius:5,
        paddingRight:10,
        paddingLeft:18
    },
    contentText:{
        fontSize:16,
        color:'grey',
        marginTop:10
    },
    bulletsSubService:{
        height:40,
        width:40,
        borderRadius:20,
        backgroundColor: 'rgba(52, 52, 52,0.7)',
        marginTop:20,
        marginRight:35,
        marginLeft:35,
        marginBottom:2
    },
    drawerContent:{
        fontSize:16,
        alignSelf:'flex-start',
        marginLeft:20
        
    },
    smallNavigate:{
        fontSize:16
    },
    ratingStar:{
        height:25,
        width:15
    },
    ratingNumber:{
        fontSize:16,
        marginRight:5
    },
    drawerOneElement:{
        flexDirection:'row',
        marginTop:25,
        alignItems:'center',
        marginLeft:10
    },
    bottomSheetUpper:{
        width:width,
        height:height*0.09,
        backgroundColor:'white',
        flexDirection:'row'
    },
    subCategoryIcon:{
        height:30,
        width:40
    },
    mainSubCategory:{
        justifyContent:'center',
        alignItems:'center',
        margin:17,
        marginTop:2,
        width:width*0.20,
        borderRadius:10,
        height:width*0.20,
        backgroundColor:'white',//change
        padding:10
    },
    icon:{
        height:10,
        width:9
    },
    contentAccordian:{
        width:"90%",
        height:'auto',
        zIndex:100,
        justifyContent:'center',
        backgroundColor:'#DCDFE1',
        alignSelf:'center',
        padding:20
    },
    addAnother:{
        color:'#3D81C9',
        fontSize:16,
        marginTop:10
    },
    dropDownOptionImage:{
        height:15,
        width:20,
        marginLeft:10
    },
    downImage:{
        height:15,
        width:20,
        position:'absolute',
        right:20
    },
    dropDownContainer:{
        flexDirection:'row',
        width:width*0.9,
        alignSelf:'center',
        height:height*0.06,
        backgroundColor:'white',
        borderRadius:15,
        padding:7,
        alignItems:'center',
        marginTop:20
    },
    dropDownOptionContainer:{
        width:width*0.9,
        height:'auto',
        alignSelf:'center',
        backgroundColor:'white',
        borderRadius:15,
        padding:12,
        alignItems:'center',
        marginTop:10
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        width:'100%',
        marginTop:10,
        paddingBottom:5,
        alignSelf:'center',
        borderBottomWidth:0.5,
        borderBottomColor:'#E3E0DD'
    },
    scrollablePart:{
        backgroundColor:'white',
        width:width,
        backgroundColor:'#f5f5f5',
        flexDirection:'row',
        flexWrap:'wrap',
        justifyContent:'center'
    }
});



export default DashBoard;
// import {
//     TouchableOpacity,
//     Dimensions,
//     AppRegistry,
//     StyleSheet,
//     Text,
//     View
//    } from 'react-native';
//    import React, { Component } from 'react';
//    import MapView from 'react-native-maps'
   
//    export default class TheMap extends Component {
//     constructor(props){
//       super(props);
//       this.state={
//         position: {
//             latitude :24.9418733,
//             longitude:67.1121096,
//           },
//           region: {
//             latitude:24.9418733,
//             longitude:67.11210962,
//             latitudeDelta: 0.0922,
//             longitudeDelta: 0.0421,
//           }
//         }
//     }
   
//     _findMe(){
//       navigator.geolocation.getCurrentPosition(
//         ({coords}) => {
//           const {latitude, longitude} = coords
//           this.setState({
//             position: {
//               latitude,
//               longitude,
//             },
//             region: {
//               latitude,
//               longitude,
//               latitudeDelta: 0.0922,
//               longitudeDelta: 0.0421,
//             }
//           })
//         },
//         (error) => alert(JSON.stringify(error)),
//         {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
//       )
//     }
//     componentDidMount() {
//     //     navigator.geolocation.getCurrentPosition(
//     //       ({coords}) => {
//     //         const {latitude, longitude} = coords
//     //         this.setState({
//     //           position: {
//     //             latitude,
//     //             longitude,
//     //           },
//     //           region: {
//     //             latitude,
//     //             longitude,
//     //             latitudeDelta: 0.0922,
//     //             longitudeDelta: 0.0421,
//     //           }
//     //         })
//     //       },
//     //       (error) => alert('Error: Are location services on?'),
//     //       {enableHighAccuracy: true}
//     //     );
//     //     this.watchID = navigator.geolocation.watchPosition(
//     //       ({coords}) => {
//     //         const {lat, long} = coords
//     //         this.setState({
//     //           position: {
//     //             lat,
//     //             long
//     //           }
//     //         })
//     //     });
//       }
//       componentWillUnmount() {
//         // navigator.geolocation.clearWatch(this.watchID);
//       }
//     render() {
//       const { height: windowHeight } = Dimensions.get('window');
//       const varTop = windowHeight - 125;
//       const hitSlop = {
//         top: 15,
//         bottom: 15,
//         left: 15,
//         right: 15,
//       }
//       bbStyle = function(vheight) {
//         return {
//           position: 'absolute',
//           top: vheight,
//           left: 10,
//           right: 10,
//           backgroundColor: 'transparent',
//           alignItems: 'center',
//         }
//       }
//       return(
//         <View style={styles.container}>
//           <View style={bbStyle(varTop)}>
//             <TouchableOpacity
//               hitSlop = {hitSlop}
//               activeOpacity={0.7}
//               style={styles.mapButton}
//               onPress={ () => this._findMe() }
//             >
//                 <Text style={{fontWeight: 'bold', color: 'black',}}>
//                   Find Me
//                 </Text>
//             </TouchableOpacity>
//           </View>
//           <MapView
//             style={styles.map}
//             region={this.state.region}
//             showsUserLocation={true}
//           >
//           </MapView>
//         </View>
//       );
//     }
//    }
   
//    const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//       backgroundColor: '#EEEEEE',
//     },
//     text: {
//       color: 'white',
//     },
//       map: {
//       flex: 1,
//       zIndex: -1, 
//     },
//     mapButton: {
//       width: 75,
//       height: 75,
//       borderRadius: 85/2,
//       backgroundColor: 'rgba(252, 253, 253, 0.9)',
//       justifyContent: 'center',
//       alignItems: 'center',
//       shadowColor: 'black',
//       shadowRadius: 8,
//       shadowOpacity: 0.12,
//       opacity: .6,
//       zIndex: 10,
//    }})