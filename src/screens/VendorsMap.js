import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon } from "native-base";
import StarRating from 'react-native-star-rating';
import Header from '../components/Header'
import MapView, { Callout } from 'react-native-maps';
import MyLocation from '../../images/mapsPurple.png' 
import LocationSelected from '../../images/Path.png' 
import Location from '../../images/mapgrey.png' 
import Button from '../components/Buttons'
import Cross from '../../images/close.png'
import Star from '../../images/star.png'
import Round from '../../images/infogrey.png'
import Avatar from '../../images/avatar.jpg'


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class VendorMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            region: {
                latitude: 24.9418733,
                longitude: 67.1121096,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              },
              markers: [
                {
                    coordinate: {
                        latitude: 24.9418733,
                        longitude: 67.1121096
                    },
                    title: "Best Place",
                    description: "Description1",
                    id: 1
                },
                {
                    coordinate: {
                        latitude: 24.9438733,
                        longitude: 67.1241096
                    },
                    title: "Best Place2",
                    description: "Description 2",
                    id: 2
                },
                {
                    coordinate: {
                        latitude: 24.9416577,
                        longitude: 67.1120923
                    },
                    title: "Best Place3",
                    description: "Description3",
                    id: 3
                },
                {
                    coordinate: {
                        latitude: 24.9439003,
                        longitude: 67.1135437
                    },
                    title: "Best Place4",
                    description: "Description 4",
                    id: 4
                },
            ],
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Select Vendor" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };
    render() {
        const mypointer = {
            latitude: 24.9418733,
            longitude: 67.1121096
        }
        const OtherIcon = (marker) => {
            if(marker && this.state.selected && marker.id === this.state.selected.id){
                return LocationSelected
            }
            else {
                return Location
            }
        }
        console.log(this.state,'this is the state')
        return (
            <ScrollView contentContainerStyle={styles.container}>
                                {/* <Image source={Star} style={{height:20,width:20,zIndex:2}}/> */}

                <MapView
                    ref={map => this.map = map}
                    showsMyLocationButton={true}
                    showsCompass={true}
                    showsUserLocation={true}
                    loadingEnabled={true}
                    initialRegion={this.state.region}
                    style={styles.containerMap}
                >
                    {this.state.markers.map((marker) =>
                        (
                            <MapView.Marker
                                key={marker.id}
                                coordinate={marker.coordinate}
                                image={marker.coordinate.latitude === mypointer.latitude && marker.coordinate.longitude === mypointer.longitude ? MyLocation :OtherIcon(marker)}
                                draggable
                                title={marker.title}
                                description={marker.description}
                                onPress={(event)=>this.setState({selected : marker})}
                            ><Callout tooltip>
                                <View style={{paddingHorizontal:8,borderRadius:10,backgroundColor:'white',flexDirection:'row'}}>
                                <Text
                                style={{color:marker && this.state.selected && marker.id === this.state.selected.id ? '#AE1371' : 'black'}}
                                >5.00</Text>
                                <Image source={Star} style={{height:18,width:18}}/>
                                </View>
                            </Callout>
                            </MapView.Marker>
                            ))
                    }
                </MapView>
                <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={[styles.circleColor, { backgroundColor: "#AE1371" }]} />
                    <View style={[styles.bar, { backgroundColor: "#AE1371" }]} />
                    <View style={styles.circleColor} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                </View>

                <View style={styles.addressBarContainer}>
                    <Text style={styles.addressBarText}>Current Location</Text>
                    <Icon name={'ios-arrow-down'} style={{ fontSize: 20, marginRight: 15 }} />
                </View>

                {this.state.selected && <View style={styles.cardContainer} >
                            <View style={styles.cardContainerLeft}>
                                <Image source={Avatar} style={styles.avatar} />
                            </View>
                            <View style={styles.cardContainerRight}>
                                <View style={styles.upperRow}>
                                    <Text style={{ fontSize: 16, color:'black' }}>John Doe</Text>
                                    <View style={{ width: '40%', flexDirection: 'row' }}>
                                        <Text style={{ color:'grey' }}>4KM</Text>
                                        <View style={{}}>
                                            <StarRating
                                                disabled={false}
                                                starSize={15}
                                                starStyle={{}}
                                                disabled={true}
                                                // style={{flexDirection:'row',justifyContent:'flex-start'}}
                                                emptyStar={'ios-star-outline'}
                                                fullStar={'ios-star'}
                                                halfStar={'ios-star-half'}
                                                iconSet={'Ionicons'}
                                                maxStars={5}
                                                rating={5}
                                                selectedStar={(rating) => this.setState({
                                                    starCount: rating
                                                })}
                                                fullStarColor={'#e8c814'}
                                            />
                                        </View>
                                    </View>
                                </View>

                                <View style={{ width: '90%', flexDirection: 'row', justifyContent: 'space-between', alignSelf: "center", alignItems: 'center' }}>
                                    <Text style={{ width: '65%', fontSize: 12, color:'grey' }}>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit</Text>
                                    <Image source={Round} style={{ width: 30, height: 30, marginRight: 15 }} />
                                </View>

                            </View>
                        </View>}
                <View style={styles.bottom}>
                    <Button 
                        disabled={!this.state.selected ? true : false}
                        style={{
                            backgroundColor: !this.state.selected ? '#EFEFEF' : '#AE1371'
                        }}
                        textStyle={{
                            color: !this.state.selected ? '#888888' : 'white'
                        }}
                        onPress={() => this.props.navigation.navigate('VendorList')}
                        title="Confirm Vendor" colored width={"90%"}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    upperRow: {
        width: '90%',
        flexDirection: 'row',
        marginTop: 12,
        justifyContent: 'space-between',
        alignSelf: "center"
    },
    cardContainerRight: {
        width: '78%',
        // backgroundColor:"red",
        height: '100%'

    },
    avatar: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 1,
        borderColor: "#C5C8CC"
    },
    cardContainerLeft: {
        width: '22%',
        // backgroundColor:"green",
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'

    },
    cardContainer: {
        bottom:80,
        position:'absolute',
        backgroundColor: 'white',//"#AE1371",,
        marginTop: 12,
        width: '90%',
        alignSelf:"center",
        height: 100,
        borderRadius: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    containerMap:{
        position:'absolute',
        top:0,
        bottom:0,
        right:0,
        left:0
    },
    addressBarText: {
        color: 'black',
        fontSize: 14,
        marginLeft: 15
    },
    addressBarContainer: {
        width: width * 0.9,
        height: 40,
        borderRadius: 15,
        alignSelf: 'center',
        backgroundColor: "white",
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: "center"
    },
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    container: {
        // justifyContent:'center',
        // alignItems:'center',
        backgroundColor: "#F8F8F8",
        flex: 1
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleColor: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    bottom: {
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },



});

export default VendorMap;