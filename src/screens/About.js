import React ,{Component} from 'react'
import {View,Text,Dimensions,Image,TouchableOpacity,StyleSheet} from 'react-native';
import Header from '../components/Header'
const {height,width} =Dimensions.get('window')
import { Container, Content, Accordion ,Icon, Thumbnail} from "native-base";
const dataArray = [
  { title: "Contact details of Layzee", 
  content: "Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet                                                                                    "
 },
];
export default class About extends Component {
    constructor(){
        super()
    }
    static navigationOptions = {
      header:({navigation}) => <Header title="About" goBack={()=>navigation.navigate('Starter')}/>
      };    
  _renderContent({content}){
        console.log(content,"bc")
        return(
            <View
            style={{
            width:width*0.85,marginRight:"auto",marginLeft:"auto",backgroundColor:"green",borderRadius:10,marginVertical:10,height:"auto"
            }}
            >
                <Text>{content}</Text>
            </View>
        )
    }
  render() {
    return (
      <Container
        style={{ backgroundColor: "#F8F8F8" }}
      >
        <Content >
          <TouchableOpacity onPress={()=> this.props.navigation.navigate("Policies")}>
          <View
            style={Styles.itemContainer}
          >
            <Text
              style={Styles.itemText}
            >Instructioin and Policies</Text>
            <Icon name='ios-arrow-forward' style={{ fontSize: 18, margin: 10 }} />
          </View>
          </TouchableOpacity>
          <View style={Styles.itemContainer}>
            <Text
              style={Styles.itemText}
            >Terms and Condition</Text>
            <Icon name='ios-arrow-forward' style={{ fontSize: 18, margin: 10 }} />
          </View>

          <Accordion dataArray={dataArray}
            // icon="ios-arrow-forward"
            style={{ borderBottomWidth: 0 }}
            headerStyle={Styles.headerStyle}
            contentStyle={Styles.contentStyle}
          />
        </Content>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  itemContainer:{
    flexDirection: "row", 
    justifyContent: "space-between",
    width: width * 0.85, 
    marginRight: "auto", 
    marginLeft: "auto", 
    borderRadius: 10, 
    marginTop: 10,
    backgroundColor:"white"
  },
  itemText:{
    height: "auto",
    fontSize: 16, 
    fontWeight: "400", 
    margin: 10, 
    color: "black"
  },
  headerStyle:{
    width:width*0.85,
    marginRight:"auto",
    marginLeft:"auto",
    backgroundColor:"green",
    borderRadius:10,
    marginTop:10,
    borderWidth:0,
    backgroundColor:"white"

  },
  contentStyle:{
    width:width*0.85,
    marginRight:"auto",
    fontSize:14,
    marginLeft:"auto",
    backgroundColor:"green",
    borderBottomLeftRadius:10,
    borderBottomRightRadius:10,
    marginTop:-7,
    height:"auto",
    backgroundColor:"white"

  }
})