import React,{Component} from 'react';
import {View,Image,Keyboard,Text,Dimensions,TouchableOpacity,ScrollView,Platform,Animated,Modal} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Input from './../components/Input';
import Buttons from './../components/Buttons';
const { height, width } = Dimensions.get("window");

class LoginScreen extends Component{
    constructor(props){
        super(props);
        this.state = {
            height:height,
            width:width,
            logoHeight:height*0.12,
            modalVisible:false,
            index: 0,
            routes: [
            { key: 'first', title: 'First' },
            { key: 'second', title: 'Second' },
            { key: 'third', title: 'Third' },
            { key: 'fourth', title: 'Fourth' }
            ]
        }
    }

    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this._keyboardDidShow());
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',() => this._keyboardDidHide());
      }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
    componentWillMount(){
        Animated.spring();
    }

    _keyboardDidShow () {
      }
    
      _keyboardDidHide () {
      }

    _onChangeEmail(email){
        console.log(email);
    }

    _onChangePassword(password){
        console.log(password)
    }

    _SignIn(){
        console.log('called');
    }

    _Guest(){
        console.log('guest login');
    }

    _onFocus(){
        console.log('focus received');
    }

    _onBlur(){
        console.log('focus lost');
        this.setState({
            logoHeight:height*0.12
        });
    }

    _forgetPassword(){
        //Actions.verifying();
        this.setState({
            modalVisible:true
        })
    }

    setModalVisible(){
        this.setState({
            modalVisible:false
        })
    }

    render(){
        const FirstRoute = () => (
            <View style={[styles.container, { backgroundColor: '#FFFFFF' }]}>
                <TouchableOpacity onPress={()=>this.setModalVisible()}>
                    <Text style={{fontSize:18}}>Click here to close this</Text>
                </TouchableOpacity>
                
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#ff4081',marginTop:10}}/>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#ff4081',marginTop:10}}/>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#ff4081',marginTop:10}}/>
                    <TouchableOpacity
                        onPress={() => {
                        this.setModalVisible();
                        }}>
                        <Text>Hide Modal</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
          );
          const SecondRoute = () => (
            <View style={[styles.container, { backgroundColor: '#FFFFFF' }]}>
                <TouchableOpacity onPress={()=>this.setModalVisible()}>
                    <Text style={{fontSize:18}}>Click here to close this</Text>
                </TouchableOpacity>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#673ab7',marginTop:10}}/>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#673ab7',marginTop:10}}/>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#673ab7',marginTop:10}}/>
                    <TouchableOpacity
                        onPress={() => {
                        this.setModalVisible();
                        }}>
                        <Text>Hide Modal</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
          );
          const ThirdRoute = () => (
            <View style={[styles.container, { backgroundColor: '#FFFFFF' }]}>
                <TouchableOpacity onPress={()=>this.setModalVisible()}>
                    <Text style={{fontSize:18}}>Click here to close this</Text>
                </TouchableOpacity>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#761590',marginTop:10}}/>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#761590',marginTop:10}}/>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#761590',marginTop:10}}/>
                    <TouchableOpacity
                        onPress={() => {
                        this.setModalVisible();
                        }}>
                        <Text>Hide Modal</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
          );
          const FourthRoute = () => (
            <View style={[styles.container, { backgroundColor: '#FFFFFF' }]}>

                <TouchableOpacity onPress={()=>this.setModalVisible()}>
                    <Text style={{fontSize:18}}>Click here to close this</Text>
                </TouchableOpacity>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#C44E07',marginTop:10}}/>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#C44E07',marginTop:10}}/>
                    <View style={{height:height*0.3,width:width*0.9,backgroundColor:'#C44E07',marginTop:10}}/>
                    <TouchableOpacity
                        onPress={() => {
                        this.setModalVisible();
                        }}>
                        <Text>Hide Modal</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
          );
        return(
            <LinearGradient 
                colors={['#848AD8', '#950F56', '#BD4686']} 
                style={styles.linearGradient}
                start={{x: 1, y: 0}} 
                end={{x: 0.5, y: 1.0}}
                >
                <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                    <TabView
                    navigationState={this.state}
                    onIndexChange={index => this.setState({ index })}
                    renderScene={SceneMap({
                        first: FirstRoute,
                        second: SecondRoute,
                        third: ThirdRoute,
                        fourth: FourthRoute
                    })}
                    tabBarPosition=""
                    initialLayout={ {width:width,height:height*0.5,backgroundColor:'#0C4A9E' }}
                    />
                </Modal>
                <View style={{flex:1,alignItems:'center'}}>
                    <TouchableOpacity style={{position:'absolute',top:Platform.OS=='ios'?height*0.05:height*0.03,left:15}} onPress={()=>this._navigateBack()}>
                        <Image
                        source={require('../../images/back.png')}
                        style={{height:20,width:20}}
                        />
                    </TouchableOpacity>
                    <View style={{flex:1,justifyContent:'center'}}>
                        <Image
                        source={require('../../images/logo.png')}
                        style={{height:50,width:width*0.3}}
                        resizeMode='contain'
                        />
                    </View>
                    <View style={{justifyContent:'flex-start',alignItems:'center',flex:1.75}}>
                        <Input type="email-address" hint="Email or phone" changeText={(text)=> this._onChangeEmail(text)} image={require('../../images/email.png')}/>
                        <Input hint="Password" changeText={(text)=> this._onChangePassword(text)} image={require('../../images/password.png')} password={true}/>
                        <TouchableOpacity style={{alignSelf:'flex-end',width:'auto',alignItems:'flex-end',marginTop:8}} onPress={() => this.props.navigation.navigate('ForgetPassword')}>
                            <Text style={styles.forgetPassword}>Forgot Password?</Text>
                        </TouchableOpacity>
                        <Buttons color={true} heading="SIGN IN" onPress={()=>this._SignIn}/>
                    </View>
                    <View style={{alignItems:'center',flex:1}}>
                            <Text style={styles.extraText}>or Sign in With</Text>
                            <View style={styles.horizonSocial}>
                                <TouchableOpacity>
                                    <Image
                                    source={require('../../images/google.png')}
                                    style={styles.google}
                                    />
                                </TouchableOpacity>
                                
                                <TouchableOpacity>
                                    <Image
                                    source={require('../../images/facebook.png')}
                                    style={styles.google}
                                    />
                                </TouchableOpacity>
                            </View>
                    </View>
                    <View style={{justifyContent:'center',alignItems:'center',flex:0.5}}>
                        <View style={styles.horizon}/>
                        <TouchableOpacity style={styles.guest} onPress={()=>this._Guest()}>
                            <Text style={styles.guestText}>CONTINUE AS GUEST</Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
            </LinearGradient>
        )
    }

}

const styles = {
    linearGradient: {
        flex: 1,
      },
      container:{
        height:height*0.4,
        width:width,
        position:'absolute',
        bottom:0,
        alignItems:'center',
        justifyContent:'center'
      },
      forgetPassword:{
          color:'#848AD8'
      },
      extraText:{
          color:'#FFFFFF',
          marginTop:50
      },
      horizon:{
          height:1,
          width:width*0.7,
          backgroundColor:'#FFFFFF'
      },
      guest:{
          marginTop:10
      },
      guestText:{
        color:'#FFFFFF',
      },
      google:{
          height:50,
          width:50
      },
      horizonSocial:{
          flexDirection:'row',
          justifyContent:'space-between',
          width:width*0.4,
          height:'auto',
          marginTop:10
      },
      contentContainer:{
        width:width*0.9,
        height:'auto'  
    }
}

export default LoginScreen;