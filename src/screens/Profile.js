import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity,ScrollView } from 'react-native';
import ImageResizer from 'react-native-image-resizer';
import {UPDATE_PROFILE} from '../store/Middleware/profile'
import {connect} from 'react-redux'
import ImagePicker from 'react-native-image-picker';
import Avatar from '../../images/avatar.jpg'
import Star from '../../images/star.png'
import Camera from '../../images/camera.png'
import Home from '../../images/home.png'
import Work from '../../images/work3.png'
import Car from '../../images/car3.png'
import Header from '../components/Header'
import Edit from '../../images/edit3.png'
const { width, height } = Dimensions.get('window').width;
let _this = null;

const mapStateToProps = (state) => ({
    profile: state.profile
})
const mapDispatchToProps = (dispatch) => ({
    UPDATE_PROFILE:(abcd)=>dispatch(UPDATE_PROFILE(abcd))
})

class Profile extends Component {
    constructor(){
        super();
        this.state={

        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Profile" RightIcon={Edit} onRightPress={() => navigation.navigate('EditProfile')} goBack={()=>navigation.navigate('Starter')} />
    };
    selectPhotoTapped() {console.log("clicked")
    const options = {
      quality: 1,
    //   maxWidth: 100,
    //   maxHeight: 100,
      takePhotoButtonTitle:"Take Photo",
      chooseFromLibraryButtonTitle:"Choose Existing",
      permissionDenied:{
          title:"Layzee Would Like to Access The Camera",
          text:"Layzee camera use",
          okTitle:"OK"
        },
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        console.log(source,",############")
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
            avatarSource: source.uri
          });

        ImageResizer.createResizedImage(source.uri, 50, 50, 'JPEG', 1).then((response) => {
            console.log(response.uri,'THIS ONE RESIZED URI')
            source = {uri : response.uri}
            // response.uri is the URI of the new image that can now be displayed, uploaded...
            // response.path is the path of the new image
            // response.name is the name of the new image with the extension
            // response.size is the size of the new image
              this.props.UPDATE_PROFILE({
                  id:this.state.user.id,
                  user:{...this.state.user,
                      user_role_id:4,
                      profile_picture:source.uri
                  },
                  navigation:this.props.navigation,
              })
          }).catch((err) => {
              console.log(err,"hello resized failed")
            // Oops, something went wrong. Check that the filename is correct and
            // inspect err to get more details.
          });


        this.setState({
          avatarSource: source.uri
        });
        this.props.UPDATE_PROFILE({
            id:this.state.user.id,
            user:{...this.state.user,
                user_role_id:4,
                // profile_picture:source
            },
            navigation:this.props.navigation,
        })
      }
    });
  }
  componentDidMount() {
    _this = this;
    var profile = this.props.navigation.getParam('user');
    console.log(profile ? (profile,'this is param') : 'abhi nahi aya param')
    // this.props.profile && 
    this.setState({user:profile})
}
getSnapshotBeforeUpdate(prevProps){
 if(prevProps.navigation.getParam('user')){
     return prevProps.navigation.getParam('user')
 }
 else{
     return null
 }
}
componentDidUpdate(prevProps,state,snap){
    console.log(snap,"check")
}
    render() {console.log('this is state in profile',this.state.user)
    const name = this.state.user && this.state.user.name  || 'Username'
    const email = this.state.user && this.state.user.email  || 'Email'
    const phone = this.state.user && this.state.user.phone  || 'Phone'
        return (
            <ScrollView style={{ flex: 1 ,backgroundColor:"#F8F8F8"}}>
                <View style={styles.topContainer}>
                    <View style={styles.profileContainer}>
                        <Image source={this.state.avatarSource ? this.state.avatarSource : Avatar} style={styles.avatar} />
                        <TouchableOpacity onPress={()=>this.selectPhotoTapped()}>
                        <View
                            style={styles.cameraContainer}>
                            <Image source={Camera}
                                style={styles.camera}
                            />
                        </View></TouchableOpacity>
                    </View>
                    <Text
                        style={styles.userName}
                    >{name}</Text>
                    <View style={styles.ratingContainer}>
                        {[1, 2, 3, 4, 5].map((value, index) => (<Image key={index} source={Star} style={styles.ratingStar} />))}
                    </View>
                </View>
                <View
                    style={styles.itemContainer}
                >
                    <Text style={[styles.itemHeader, { width: "70%", marginLeft: "auto", marginRight: "auto" }]}>Email</Text>
                    <Text style={styles.itemText}>{email}</Text>
                </View>

                <View
                    style={styles.itemContainer}
                >
                    <Text style={[styles.itemHeader, { width: "70%", marginLeft: "auto", marginRight: "auto" }]}>Phone</Text>
                    <Text style={styles.itemText}>{phone}</Text>
                </View>
                    {console.log(this.state.user ? this.state.user.UserAddresses : 'abhi wait',"ho there")}
                {this.state.user && this.state.user.UserAddresses && this.state.user.UserAddresses.length ? this.state.user.UserAddresses.map((data,index)=>
                (<TouchableOpacity style={styles.itemIconContainer} key={index} 
                // onPress={()=>this.props.navigation.navigate('AddOrEditAddress',{id:data.id})}
                >
                <View style={{ width: "15%" }}>
                    <Image source={Home} style={styles.icon} /></View>{console.log(this.state.user.UserAddresses,"daikho")}
                <Text style={[styles.itemHeader]}>{data.house_no},{data.street_no},{data.state}</Text>
            </TouchableOpacity>)
                ) :
                <TouchableOpacity style={styles.itemIconContainer} onPress={()=>this.props.navigation.navigate('AddOrEditAddress')}>
                    <View style={{ width: "15%" }}>
                        <Image source={Home} style={styles.icon} /></View>
                    <Text style={[styles.itemHeader]}>Add Address</Text>
                </TouchableOpacity>
             }

                {/* <TouchableOpacity style={styles.itemIconContainer} onPress={()=>this.props.navigation.navigate("AddOrEditAddress")}>
                    <View style={{ width: "15%" }}>
                        <Image source={Work} style={styles.icon} /></View>
                    <Text style={[styles.itemHeader]}>Add office Address</Text>
                </TouchableOpacity> */}

                <TouchableOpacity style={styles.itemIconContainer} onPress={()=>this.props.navigation.navigate("AddOrEditCar")}>
                    <View style={{ width: "15%" }}>
                        <Image source={Car} style={styles.icon} /></View>
                    <Text style={[styles.itemHeader]}>Add Car</Text>
                </TouchableOpacity>

            </ScrollView>
        )
    }
}

const styles = {
    avatar: {
        height: 100,
        width: 100,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: "white",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: height * 0.075,
        marginBottom: -10
    },
    ratingContainer: {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",
        flexDirection: "row",
        justifyContent: "center"
    },
    userName: {
        color: "black",
        textAlign: "center",
        fontSize: 18,
        fontWeight: "bold",
        marginTop: 7.5
    },
    ratingStar: {
        height: 10,
        width: 10
    },
    topContainer: {
        height: height * 0.35,
        padding: 40,
    },
    profileContainer: {
        height: 100,
        width: 100,
        marginRight: "auto",
        marginLeft: "auto"
    },
    cameraContainer: {
        height: 30,
        width: 30,
        borderRadius: 15,
        backgroundColor: 'white',
        marginTop: -15,
        marginLeft: 60
    },
    camera: {
        height: 20,
        width: 20,
        borderRadius: 10,
        margin: 5
    },
    itemContainer: {
        borderColor: "#DCDCDC",
        borderTopWidth: 1,
        height: 70,
        paddingTop: 15,
        paddingBottom: 16
    },
    itemIconContainer: {
        borderColor: "#DCDCDC",
        borderTopWidth: 1,
        height: 70,
        alignItems: "center",
        flexDirection: "row"
    },
    itemHeader: {
        color: "black",
        fontSize: 16
    },
    icon: {
        height: 20,
        width: 20,
        marginRight: "auto",
        marginLeft: "auto"
    },
    itemText: {
        color: "#bcb8b8",
        fontSize: 12,
        marginTop: 5,
        width: "70%",
        marginLeft: "auto",
        marginRight: "auto"
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Profile);