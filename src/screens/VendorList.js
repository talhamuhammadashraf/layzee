import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon } from "native-base";
import StarRating from 'react-native-star-rating';
import Header from '../components/Header'
import Button from '../components/Buttons'
import Avatar from '../../images/avatar.jpg'
import Round from '../../images/infogrey.png'
import RoundWhite from '../../images/infowhite.png'
import VendorCard from '../components/VendorCard'

import Cross from '../../images/close.png'
import Group from '../../images/Group2.png'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class VendorList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sortVisible: false,
            vendor: null
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Select Vendor" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };
    render() {console.log(this.state.vendor,"this is in vendor list")
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={[styles.circleColor, { backgroundColor: "#AE1371" }]} />
                    <View style={[styles.bar, { backgroundColor: "#AE1371" }]} />
                    <View style={styles.circleColor} />
                    <View style={styles.bar} />
                    <View style={styles.circle} />
                </View>

                <View style={styles.addressBarContainer}>
                    <Text style={styles.addressBarText}>House B55,Block 4,Area 23</Text>
                    <Icon name={'ios-arrow-down'} style={{ fontSize: 20, marginRight: 15 }} />
                </View>
                <View style={styles.sortContainer}>
                    <TouchableOpacity style={styles.sortButton} onPress={() => this.setState((prevState) => ({ sortVisible: !prevState.sortVisible }))}>
                        <Image source={Group} style={{ width: 37, height: 18, margin: 7, marginLeft: 12, }} />
                        <Text style={{ fontSize: 15 }}>Sort by</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView contentContainerStyle={{
                    width: width * 0.9,
                    alignSelf: 'center'
                }}>

                    {[1, 2, 3, 4, 5, 6, 7]
                        .map((data, index) => (
                        <VendorCard key={index} onPress={()=>this.setState({vendor:index})} distance={4} info image={Avatar} selected={this.state.vendor === index ? true : false} navigation={this.props.navigation}/>
                        ))
                        }
                </ScrollView>
                <View style={styles.bottom}>
                    <Button disabled={this.state.vendor === null ? true : false}
                        style={{
                            backgroundColor: this.state.vendor === null ? '#EFEFEF' : '#AE1371'
                        }}
                        textStyle={{
                            color: this.state.vendor === null ? '#888888' : 'white'
                        }}
                        onPress={() => this.props.navigation.navigate('VendorProfile')}
                        title="Confirm Vendor" colored width={"90%"}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    upperRow: {
        width: '90%',
        flexDirection: 'row',
        marginTop: 12,
        justifyContent: 'space-between',
        alignSelf: "center"
    },
    cardContainerRight: {
        width: '78%',
        // backgroundColor:"red",
        height: '100%'

    },
    avatar: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 1,
        borderColor: "#C5C8CC"
    },
    cardContainerLeft: {
        width: '22%',
        // backgroundColor:"green",
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'

    },
    cardContainer: {
        // backgroundColor: 'white',//"#AE1371",,
        marginTop: 12,
        width: '100%',
        height: 100,
        borderRadius: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    sortButton: {
        backgroundColor: "white",
        width: width * 0.30,
        borderRadius: 14,
        height: "80%",
        flexDirection: 'row',
        alignItems: "center"

    },
    addressBarText: {
        color: 'black',
        fontSize: 14,
        marginLeft: 15
    },
    addressBarContainer: {
        width: width * 0.9,
        height: 40,
        borderRadius: 15,
        alignSelf: 'center',
        backgroundColor: "white",
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: "center"
    },
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    container: {
        // justifyContent:'center',
        // alignItems:'center',
        backgroundColor: "#F8F8F8",
        flex: 1
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleColor: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    bottom: {
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },
    sortContainer: {
        width: '90%',
        marginRight: 'auto',
        marginLeft: 'auto',
        marginTop: 14,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: 50

    },



});

export default VendorList;