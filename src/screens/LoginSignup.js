
import React, {Component} from 'react';
import {StyleSheet, Image,Dimensions, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Buttons from '../components/Buttons';
import {Actions} from 'react-native-router-flux';
const { height, width } = Dimensions.get("window");

export default class LoginSignUp extends Component{

    constructor(props){
        super(props);
        this.state = {
            height:height,
            width:width
        }
    }

    _SignIn(){
        Actions.login();
    }

    _SignUp(){
        Actions.signUp();
    }

    _Guest(){
        console.log('Guest');
    }

    render(){
        return(
                <LinearGradient 
                colors={['#848AD8', '#950F56', '#BD4686']} 
                style={styles.linearGradient}
                start={{x: 1, y: 0}} 
                end={{x: 0.5, y: 1.0}}
                >
                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                    <Image
                    source={require('../../images/logo.png')}
                    style={{height:50,width:width*0.3,position:'absolute',top:height*0.2}}
                    resizeMode='contain'
                    />
                    <Buttons color={true} heading="SIGN UP" onPress={this._SignUp}/>
                    <Buttons color={true} heading="SIGN IN" onPress={this._SignIn}/>
                    <Buttons heading="CONTINUE AS GUEST" onPress={this._Guest}/>
                </View>
                    
                </LinearGradient>
            
        );
      }
}

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
  buttonContainer:{
    height:height*0.06,
    width:width*0.8,
    backgroundColor:'#ffffff',
    marginTop:25,
    borderRadius:20
  },
  buttonContainerEmpty:{
    height:height*0.06,
    width:width*0.8,
    borderColor:'#ffffff',
    marginTop:25,
    borderWidth:2,
    borderRadius:20
  }
});