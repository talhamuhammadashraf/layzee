import React,{Component} from 'react';
import {View,Text,Image,Dimensions,ScrollView,TouchableOpacity,TextInput} from 'react-native';
import Avatar from '../../images/avatar.jpg'
import Camera from '../../images/camera.png'
import Down from '../../images/downarrowWhite.png'
import Header from '../components/Header'
import ModalDropdown from 'react-native-modal-dropdown';

const {width} = Dimensions.get('window').width;
const {height}= Dimensions.get('window')

class AddOrEditCar extends Component{
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Edit rofile" textRight="Save" onRightPress={() => navigation.navigate('AddOrEditAddress')} goBack={navigation.goBack} />
        };    
    render(){
        return(
            <ScrollView>
                <View style={{ flex: 1 ,backgroundColor:'#F8F8F8',minHeight:height*0.95}}>
                    <View style={styles.topContainer}>
                        <View style={styles.profileContainer}>
                            <Image source={Avatar} style={styles.avatar} />
                            <View
                                style={styles.cameraContainer}>
                                <Image source={Camera}
                                    style={styles.camera}
                                />
                            </View>
                        </View>
                    </View>

                    {["Car Model", "Car Type", "Car Make"].map((data, index) => (
                        <View key={index} style={{ margin: 8 }}>
                            <Text style={styles.itemTitle}>{data}</Text>
                            <View style={styles.innerContainer}>
                                <View style={{ width: "80%" }}>
                                    <TextInput underlineColorAndroid="transparent" placeholder="Type here" style={{ paddingLeft: 20 }} />
                                </View>
                                <ModalDropdown options={['option 1', 'option 2']} style={styles.rightContainer}
                                    dropdownStyle={{
                                        backgroundColor: "green",
                                        width: "80%",
                                        marginRight: "auto",
                                        marginLeft: "auto"
                                    }}
                                >
                                    <View style={{ width: "100%" }}>
                                        <Image source={Down} style={styles.downArrow} />
                                    </View>
                                </ModalDropdown>
                            </View>
                        </View>
                    ))
                    }

                    <View style={{ margin: 8 }}>
                        <Text style={styles.itemTitle}>Plate Number</Text>
                        <View style={styles.innerContainer}>
                            <View style={{ width: "80%" }}>
                                <TextInput underlineColorAndroid="transparent" placeholder="Type here" style={{ paddingLeft: 20 }} />
                            </View>
                            {/* <View style={styles.rightContainer}>
                                <Image source={Down} style={styles.downArrow} />
                            </View> */}
                        </View>
                    </View>


                </View>
            </ScrollView>
        )
    }
}

const styles = {
    avatar:{
        height:100,
        width:100,
        borderRadius:50,
        borderWidth:2,
        borderColor:"white",
        marginLeft:"auto",
        marginRight:"auto",
        marginTop:height*0.075,
        marginBottom : -10
    },
    topContainer:{
        height: height * 0.35,
        padding:40,
    },
    profileContainer:{
        height: 100, 
        width: 100, 
        marginRight: "auto", 
        marginLeft: "auto"
    },
    cameraContainer:{
        height: 30, 
        width: 30, 
        borderRadius: 15, 
        backgroundColor: 'white', 
        marginTop: -15, 
        marginLeft: 60
    },
    camera:{
        height: 20, 
        width: 20, 
        borderRadius: 10, 
        margin: 5
    },
    itemTitle:{
        fontSize:14,
        color:"black",
        width:"80%",
        marginLeft:"auto",
        marginRight:"auto",
        marginBottom:10,
        fontWeight:"bold"
    },
    innerContainer:{
        width: "84%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,

    },
    rightContainer:{
        width: "20%",
        backgroundColor: "#AE1371",
        borderTopRightRadius: 14,
        borderBottomRightRadius: 14,
        height:"100%",
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center"

    },
    downArrow:{
        height:15,width:15,marginRight:"auto",marginLeft:"auto"
    }
}

export default AddOrEditCar;