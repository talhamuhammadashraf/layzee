import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon } from "native-base";
import StarRating from 'react-native-star-rating';
import Star from '../../images/star.png'
import Header from '../components/Header'
import Button from '../components/Buttons'
import CardImage from '../../images/card.jpg'
import Avatar from '../../images/avatar.jpg'
import PictureCard from '../components/PictureCard'
import Cross from '../../images/close.png'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Portfolio extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Portfolio" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={{height: height * 0.35}}>
                    <View style={{zIndex:2,height:height*0.075}}>
                        <Image source={Avatar} style={styles.avatar} />
                    </View>
                        <View style={styles.topCard}>
                        <View style={{margin:10,width:'auto',alignSelf:'flex-end'}}>
                        <Text style={{fontSize:12,color:'grey'}}>4 KM away</Text>                  
                        </View>
                        <View style={{marginTop: height*0.035}}>
                                <Text style={styles.userName}>John Doe</Text>

                                    <View style={styles.ratingContainer}>
                                        {[1, 2, 3, 4, 5].map((value, index) => (<Image key={index} source={Star} style={styles.ratingStar} />))}
                                    </View>

                                <Text style={styles.userDetails}>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                </Text>

                        </View>
                        </View>
                    </View>
                    <View>

                        <View style={{
                            width: width,
                            marginTop: 10,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            justifyContent: "space-between"
                            // alignItems:""
                        }}>{
                                [1, 2, 3, 4, 5, 6, 7, 8]
                                    .map((data, index) => (
                                        <PictureCard key={index} title="wedding" number={10} src={CardImage}/>
                                    //     <View key={index}>
                                    // <View  style={styles.cardContainer}>
                                    // <Image source={CardImage} style={styles.cardImage}/>
                                    // </View>
                                    // <View style={{width:width * 0.425,marginHorizontal: width * 0.0375,flexDirection:'row',justifyContent:'space-between'}}>
                                    //     <Text style={{color:'grey',marginHorizontal:7}}>Wedding</Text>
                                    //     <Text style={{color:'grey',marginHorizontal:7}}>10 photos</Text>
                                    // </View>
                                    // </View>
                                    ))}
                        </View>

                    </View>
                </ScrollView>
                <View style={styles.bottom}>
                    <Button disabled={this.state.vendor === null ? true : false}
                        style={{
                            backgroundColor: this.state.vendor === null ? '#EFEFEF' : '#AE1371'
                        }}
                        textStyle={{
                            color: this.state.vendor === null ? '#888888' : 'white'
                        }}
                        onPress={() => this.props.navigation.navigate('Wedding')}
                        title="Confirm Vendor" colored width={"90%"}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#F8F8F8'
    },
    cardImage:{
        width:width* 0.375,
        height: width* 0.375,
        marginRight:'auto',
        marginLeft:"auto"
        },
    userDetails:{
        fontSize: 13,
        textAlign: 'center',
        color: 'grey',
        width:'80%',
        alignSelf:"center",
        marginVertical:3
    },
    userName:{
        fontSize: 18,
        textAlign: 'center',
        color: 'black',
        marginVertical:3
    },
    topCard:{
        width:'90%',
        height:height*0.225,
        borderRadius:12,
        marginTop:height*0.0375,
        backgroundColor:'white',
        alignSelf:'center'
    },
    avatar:{
        width: 100,
        height: 100,
        borderRadius: 50,
        borderColor: 'white',
        borderWidth: 2,
        alignSelf: 'center',
        margin: 18,
    },
    ratingStar:{
        height:10,
        width:10
    },
    ratingContainer:{
        width:"50%",
        marginLeft:"auto",
        marginRight:"auto",
        flexDirection:"row",
        justifyContent:"center",
        
    },
    cardContainer:{
        height: width * 0.425,
        width: width * 0.425,
        borderRadius: 12,
        marginHorizontal: width * 0.0375,
        marginVertical: 10,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems:'center',
        borderRadius:12
    }

});

export default Portfolio;