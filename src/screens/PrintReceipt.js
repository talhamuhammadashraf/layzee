import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body } from "native-base";
import Header from '../components/Header'
import Button from '../components/Buttons'
import StarRating from 'react-native-star-rating';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class PrintReceipt extends Component {

    constructor(props) {
        super(props);
        this.state = {
            starCount: 0,
            fav: false
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Home Cleaning" goBack={() => navigation.navigate('Starter')} />
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>

                <Text style={styles.container1Title}>Job Details</Text>
                <View style={[styles.container1Outer,{borderTopWidth: 0,borderBottomWidth:1}]}>
                    <View style={[styles.container1Inner,{padding:3}]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Booking ID</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>126127</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer,{borderTopWidth: 0,borderBottomWidth:1}]}>
                    <View style={[styles.container1Inner,{padding:3}]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Booking Date</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>23rd Oct,2018</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer,{borderTopWidth: 0,borderBottomWidth:1}]}>
                    <View style={[styles.container1Inner,{padding:3}]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Booking Time</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>10:45 AM</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer,{borderTopWidth: 0,borderBottomWidth:1}]}>
                    <View style={[styles.container1Inner,{padding:3}]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Location</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>Street xyz,State</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer,{borderTopWidth: 0,borderBottomWidth:1}]}>
                    <View style={[styles.container1Inner,{padding:3}]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Vendor</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>John Doe</Text>
                    </View>
                </View>

                <View style={[styles.container1Outer,{borderTopWidth: 0,borderBottomWidth:1}]}>
                    <View style={[styles.container1Inner,{padding:3}]}>
                        <Text style={{ color: "black", fontSize: 14 }}>Feedback Given</Text>
                        <View style={{ width: '17.5%', flexDirection: 'row' }}>
                            <View style={{}}>
                                <StarRating
                                    disabled={false}
                                    starSize={15}
                                    starStyle={{}}
                                    disabled={true}
                                    // style={{flexDirection:'row',justifyContent:'flex-start'}}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={5}
                                    selectedStar={(rating) => this.setState({
                                        starCount: rating
                                    })}
                                    fullStarColor={'#e8c814'}
                                />
                            </View>
                                    </View>
                    </View>
                </View>


                <Text style={styles.container1Title}>Job Details</Text>
                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>No. of Cleaners</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>2 x 20AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>No. of Hours</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>3 X 50AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Materials</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>10AED</Text>
                    </View>
                </View>

                <Text style={styles.container1Title}>Add ons</Text>
                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 1</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>20AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 2</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>30AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 14 }}>Add on 3</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>40AED</Text>
                    </View>
                </View>


                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Wait time</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>5AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Service Time</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>435.00AED</Text>
                    </View>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Discount</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>-100AED</Text>
                    </View>
                </View>


                <View style={styles.container1Outer}>
                    <View style={styles.container1Inner}>
                        <Text style={{ color: "black", fontSize: 16 }}>Final Service Cost</Text>
                        <Text style={{ color: "grey", fontSize: 13 }}>355.00 AED</Text>
                    </View>
                </View>

                <View style={styles.container1Outer}>
                    <View style={{ width: '90%', marginRight: 'auto', marginLeft: 'auto' }}>
                        <Text style={{ color: 'black', fontSize: 17, height: 40, textAlignVertical: "center", }}>Additional Info</Text>
                        <View style={{ backgroundColor: 'white', borderRadius: 15, minHeight: 80,padding:10 }}>
                            <TextInput multiline={true}
                                defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
                            />
                        </View>
                    </View>
                </View>


                <View style={styles.container1Outer}>
                    <View style={{ width: '90%', marginRight: 'auto', marginLeft: 'auto' }}>
                        <Text style={{ color: 'black', fontSize: 17, height: 40, textAlignVertical: "center", }}>Review</Text>
                        <View style={{ backgroundColor: 'white', borderRadius: 15, minHeight: 80,padding:10 }}>
                            <TextInput multiline={true}
                                defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
                            />
                        </View>
                    </View>
                </View>

                <View style={styles.bottomDiv}>
                    <Button
                        title="Print Receipt" colored width={"90%"}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    bottomSheetButton: {
        flexDirection: 'row',
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        width: width * 0.6,
        borderRadius: 15,
        backgroundColor: '#AE1371'
    },
    bottomSheetButtonMinor: {
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        width: width * 0.17,
        borderRadius: 15,
        backgroundColor: 'white'
    },

    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F8F8F8'
    },
    waitingImage: {
        height: 25,
        width: 25
    },
    bottomDiv: {
        marginVertical: 12,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: width * 0.9,
        alignSelf: 'center',
    },
    container1Title: {
        color: 'black',
        fontSize: 17,
        width: '90%',
        marginRight: 'auto',
        marginLeft: 'auto',
        height: 40,
        textAlignVertical: "center",

    },
    container1Outer: {
        borderTopWidth: 1,
        borderColor: '#DCDCDC',
        width: width,
        paddingVertical: 7

    },
    container1Inner: {
        width: width * 0.9,
        marginLeft: 'auto',
        marginRight: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5,

    }
});

export default PrintReceipt;