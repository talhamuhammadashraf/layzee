import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon } from "native-base";
import StarRating from 'react-native-star-rating';
import Star from '../../images/star.png'
import Header from '../components/Header'
import Button from '../components/Buttons'
import CardImage from '../../images/card.jpg'
import Avatar from '../../images/avatar.jpg'
import PictureCard from '../components/PictureCard'
import Cross from '../../images/close.png'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Wedding extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Wedding" onRightPress={() => navigation.navigate('Starter')} RightIcon={Cross} goBack={navigation.goBack} />
    };
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={{
                        height: height * 0.22,
                        backgroundColor: 'white',
                        marginTop: 14,
                    }}>
                        <View style={{
                            margin:30,
                            // backgroundColor:'green',
                            // height:'100%'
                        }}>
                            <Text style={{
                                color:'#AE1371',
                                fontSize:20,
                                alignSelf:'center',
                                margin:10
                            }}>Sherry's Wedding</Text>
                            <Text style={{
                                color:'grey',
                                fontSize:12,
                                width:'80%',
                                marginRight:'auto',
                                marginLeft:'auto',
                                }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
                                </Text>
                        </View>
                    </View>

                    <View>
                        <View style={{
                            width: width,
                            marginTop: 10,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            justifyContent: "space-between"
                            // alignItems:""
                        }}>{
                                [1, 2, 3, 4, 5, 6, 7, 8]
                                    .map((data, index) => (
                                        <PictureCard key={index} src={CardImage} />
                                    // <View  key={index} style={styles.cardContainer}>
                                    // <Image source={CardImage} style={styles.cardImage}/>
                                    // </View>
                                    ))}
                        </View>

                    </View>
                </ScrollView>
                <View style={styles.bottom}>
                    <Button disabled={this.state.vendor === null ? true : false}
                        style={{
                            backgroundColor: this.state.vendor === null ? '#EFEFEF' : '#AE1371'
                        }}
                        textStyle={{
                            color: this.state.vendor === null ? '#888888' : 'white'
                        }}
                        onPress={() => this.props.navigation.navigate('Wedding1')}
                        title="Confirm Vendor" colored width={"90%"}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#F8F8F8'
    },
    cardImage:{
        width:width* 0.375,
        height: width* 0.375,
        marginRight:'auto',
        marginLeft:"auto"
        },
    cardContainer:{
        height: width * 0.425,
        width: width * 0.425,
        borderRadius: 12,
        marginHorizontal: width * 0.0375,
        marginVertical: 10,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems:'center',
        borderRadius:12
    }

});

export default Wedding;