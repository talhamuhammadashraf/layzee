import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableOpacity, TextInput, AsyncStorage } from 'react-native';
import { PROFILE } from '../store/Middleware/profile'
import Modal from 'react-native-modal'
import {BallIndicator} from 'react-native-indicators'
import { connect } from 'react-redux'
import Avatar from '../../images/avatar.jpg'
import Camera from '../../images/camera.png'
import WhiteHome from '../../images/whiteHome.png'
import WhiteWork from '../../images/whiteWork.png'
import Other from '../../images/location.png'
import Header from '../components/Header'
import Button from '../components/Buttons'
const { width } = Dimensions.get('window');
const { height } = Dimensions.get('window')


const mapStateToProps = (state) => ({
    profile: state.profile
})
const mapDispatchToProps = (dispatch) => ({
    PROFILE: (abcd) => dispatch(PROFILE(abcd))
})


class AddOrEditAddress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: "",
            icon: [
                WhiteHome, WhiteWork, Other
            ]
        };
        this.submit = this.submit.bind(this)
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Edit rofile" goBack={navigation.goBack} />
    };
    submit() {
        var id = this.props.navigation.getParam('id')
        var address = {
            house_no: this.state.apartment,
            street_no: this.state.area,
            state: this.state.state,
            address_type_id: this.state.type,
            CityId: "1",
            id: id && id
        }
        // let user={
        //     ...this.props.profile,
        //     addresses:addressArr
        // }
        fetch(`http://18.223.28.90:3000/user/addresses/${this.props.profile.user.id}`, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(
                { addresses: [address] }
            ), // body data type must match "Content-Type" header
        }).then((response) => {
            console.log(response, "this is response in edit ")
            if (response.status === 200) {
                console.log(response,'this is reponse in updation hi hi hi')
                // fetch(`http://18.223.28.90:3000/user/${this.props.profile.user.id}/4`, {
                //     method: "GET", // *GET, POST, PUT, DELETE, etc.
                //     mode: "cors", // no-cors, cors, *same-origin
                //     headers: {
                //         "Content-Type": "application/json",
                //     },
                //     // body: JSON.stringify(
                //     //     { addresses: [address] }
                //     // ), // body data type must match "Content-Type" header
                // }).then((res)=>{
                    // if(res.status === 200){
                        let user = JSON.parse(response._bodyText).data.data
                        console.log(user,'grom get req')
                        AsyncStorage.setItem('user', JSON.stringify(user))
                        console.log(user)
                        // this.setState({indicator:true})
                        // setTimeout(() => {
                            // this.setState({indicator:false})
                            this.props.PROFILE({
                                // navigation:this.props.navigation,route:"Profile"
                            });
                        // }, 100);
                        // this.props.profile.user &&
                            this.props.navigation.navigate('Profile',{user:user})
                //     }
                // }).catch((error)=>alert(error))

            //     AsyncStorage.getItem('user', (err, res) => {
            //         console.log(res, "from async bc")
            //         if (!err && res !== null) {
            //             var user = JSON.parse(res);
            //             user.UserAddresses.push(JSON.parse(response._bodyText).data.data[0])
            //             AsyncStorage.setItem('user', JSON.stringify(user))
            //             console.log(user)
            //             this.props.PROFILE({navigation:this.props.navigation,route:"Profile"});
            //             // this.props.profile.user &&
            //             //     this.props.navigation.navigate('Profile',{user:this.props.profile.user})
            //         }
            //         else (alert(err))
            //     })
            }
            //     console.log(response,"this is the response");
            // if(response.status === 200){
            //     var uid = JSON.parse(response._bodyText).data.data.id
            //     payload.navigation.navigate('verify',{uid:uid,route:'Starter'});
            //     AsyncStorage.setItem('user',JSON.stringify(JSON.parse(response._bodyText).data.data));
            //     dispatch(PROFILE())
            //             // AsyncStorage.getItem('user',(err,res)=>{console.log(err,'abc',res)})
            //     return dispatch(Auth.registerSuccess(JSON.parse(response._bodyText).data.data));
            // }else{
            //     return dispatch(Auth.registerFail(JSON.parse(response._bodyText).data.message));
            // }
            // payload.navigation.navigate('verify');
            //  ('user',response._bodyText.user)
            // return dispatch(Auth.registerSuccess(JSON.parse(response._bodyText.data.data)));
        }).catch((error) => {
            alert(error)
            // console.log('thid id error in apiu',error);
            // return dispatch(Auth.registerFail(error));
        })
    }
    render() {
        console.log(this.state, "this is the state", this.props)
        return (
            <ScrollView>
                <View style={{ flex: 1, backgroundColor: '#F8F8F8', minHeight: this.state.type ? height * 1.25 : height * 1.1 }}>
                    <Modal isVisible={this.state.indicator}>
                        <View style={{ width: width * 0.3, height: width * 0.3, backgroundColor: 'white', alignSelf: 'center', borderRadius: 14 }}>
                            <BallIndicator size={60} color="#AE1371" />
                        </View>
                    </Modal>
                    <View style={styles.topContainer}>
                        <View style={styles.profileContainer}>
                            <Image source={Avatar} style={styles.avatar} />
                            <View
                                style={styles.cameraContainer}>
                                <Image source={Camera}
                                    style={styles.camera}
                                />
                            </View>
                        </View>
                    </View>

                    {this.state.type !== "" &&
                        <View style={styles.barContainer}>
                            <View style={styles.barInnerContainer}>
                                <View style={styles.imageContainer}>
                                    <Image source={this.state.icon[this.state.type]} style={styles.image} />
                                </View>
                                <View style={styles.textContainer}>
                                    <Text style={styles.text}>
                                        {`${this.state.apartment}, ${this.state.area}, ${this.state.state}`}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    }

                    {[
                        { id: "apartment", name: 'Apartment number/Villa' },
                        { id: "area", name: 'Area/City' },
                        { id: "state", name: 'State' },
                    ].map((data, index) => (
                        <View style={{ margin: 12 }} key={index}>
                            <Text style={styles.itemTitle}>{data.name}</Text>
                            <View style={styles.innerContainer}>
                                <View style={{ width: "80%" }}>
                                    <TextInput underlineColorAndroid="transparent" placeholder="Type here" style={{ paddingLeft: 20 }}
                                        onChange={
                                            (ev) => this.setState({
                                                [data.id]: ev.nativeEvent.text
                                            })
                                        }
                                    />
                                </View>
                            </View>
                        </View>
                    ))
                    }

                    <View style={styles.selectContainer}>
                        {
                            ['Home', 'Work', "Other"].map((data, index) => (
                                <TouchableOpacity onPress={() => { this.setState({ type: index }) }} key={index}
                                    style={[styles.selectBox, { backgroundColor: this.state.type === index ? "#AE1371" : "white" }]}>
                                    <View>
                                        <Text style={{ fontSize: 13, color: this.state.type === index ? "white" : "black" }}>{data}</Text>
                                    </View>
                                </TouchableOpacity>
                            ))
                        }
                    </View>
                    {/* <View style={{width:width,position:'absolute',bottom:0,flexDirection:'row',justifyContent:"center"}}> */}
                    <Button
                        title="Add Address"
                        colored
                        width={width * 0.9}
                        style={{
                            left: width * 0.05,
                            right: width * 0.05,
                            position: 'absolute',
                            bottom: 10
                        }}
                        onPress={this.submit}
                    />
                    {/* </View> */}
                </View>
            </ScrollView>
        )
    }
}

const styles = {
    avatar: {
        height: 100,
        width: 100,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: "white",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: height * 0.075,
        marginBottom: -10
    },
    topContainer: {
        height: height * 0.35,
        padding: 40,
    },
    profileContainer: {
        height: 100,
        width: 100,
        marginRight: "auto",
        marginLeft: "auto"
    },
    cameraContainer: {
        height: 30,
        width: 30,
        borderRadius: 15,
        backgroundColor: 'white',
        marginTop: -15,
        marginLeft: 60
    },
    camera: {
        height: 20,
        width: 20,
        borderRadius: 10,
        margin: 5
    },
    itemTitle: {
        fontSize: 14,
        color: "black",
        width: "80%",
        marginLeft: "auto",
        marginRight: "auto",
        marginBottom: 10,
        fontWeight: "bold"
    },
    innerContainer: {
        width: "84%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,

    },
    barContainer: {
        backgroundColor: "#AE1371",
        width: width,
        height: 50,

    },
    barInnerContainer: {
        width: "80%", marginRight: "auto", marginLeft: "auto", flexDirection: 'row', height: "100%"
    },
    imageContainer: {
        width: "15%",
        height: "100%",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",

    },
    textContainer: {
        width: "85%",
        height: "100%",
        alignItems: "center",
        justifyContent: "flex-start",
        flexDirection: "row",

    },
    image: {
        height: 20, width: 20, margin: 10
    },
    text: {
        color: "white"
    },
    selectContainer: {
        flexDirection: "row",
        width: "75%",
        marginRight: "auto",
        marginLeft: "auto",
        justifyContent: "space-between",
        marginTop: 20
    },
    selectBox: {
        width: "27.5%",
        height: 30,

        borderRadius: 12.5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'center'
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddOrEditAddress);