import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon } from 'native-base'
import Header from '../components/Header'
import StarRating from 'react-native-star-rating';
import Button from '../components/Buttons'
import Cross from '../../images/close.png'
import Plus from '../../images/plus.png'
import ToggleOff from '../../images/toggleOff.png'
import ToggleOn from '../../images/switch.png'


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class ConfirmBooking extends Component {

    constructor(props) {
        super(props);
        this.state = {
            View1Expanded: false,
            View2Expanded: false,
            View3Expanded: false,
            paymentExpanded: false,
            SwitchOn : false
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Confirming Booking" RightIcon={Cross} onRightPress={() => navigation.navigate('Starter')} goBack={navigation.goBack} />
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View
                    style={{ minHeight: height,height:'auto' }}
                >
                    <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={[styles.circleColor, { backgroundColor: "#AE1371" }]} />
                        <View style={[styles.bar, { backgroundColor: "#AE1371" }]} />
                        <View style={[styles.circleColor, { backgroundColor: "#AE1371" }]} />
                        <View style={[styles.bar, { backgroundColor: "#AE1371" }]} />
                        <View style={styles.circleColor} />
                    </View>

                    <View style={styles.addressBarContainer}>
                        <Text style={styles.addressBarText}>House B55,Block 4,Area 23</Text>
                        <Icon name={'ios-arrow-down'} style={{ fontSize: 20, marginRight: 15 }} />
                    </View>
                    <TouchableOpacity>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Job Details</Text>
                                <TouchableOpacity onPress={() => this.setState((prevState) => ({ View1Expanded: !prevState.View1Expanded }))}>
                                    <Icon name={this.state.View1Expanded ? 'ios-arrow-up' : 'ios-arrow-down'}
                                        style={{ fontSize: 15, marginRight: 15 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ display: this.state.View1Expanded ? 'flex' : 'none', paddingBottom: 20 }}>
                            <View style={{width:'90%',alignSelf:'center',borderBottomColor:"#9F9F9F",marginVertical:7,borderBottomWidth:0.4,flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'black',fontSize:16}}>Car</Text>
                            <Text style={{color:"#9F9F9F",fontSize:16}}>BMW X1</Text>
                            </View>

                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'#9F9F9F',fontSize:12}}>Car Make</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>BMW</Text>
                            </View>

                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'#9F9F9F',fontSize:12}}>Car Type</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>SUV</Text>
                            </View>

                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'#9F9F9F',fontSize:12}}>Car Modal</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>X1</Text>
                            </View>


                            <View style={{width:'90%',alignSelf:'center',borderBottomColor:"#9F9F9F",marginVertical:7,borderBottomWidth:0.4,flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'black',fontSize:16}}>Window Tinting</Text>
                            <Text style={{color:"#9F9F9F",fontSize:16}}>AED 195.00</Text>
                            </View>

                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'#9F9F9F',fontSize:12}}>Wind Sheild</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>No</Text>
                            </View>

                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'#9F9F9F',fontSize:12}}>Frint Windows</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>No</Text>
                            </View>

                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'#9F9F9F',fontSize:12}}>Back Windows </Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>Yes</Text>
                            </View>

                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'#9F9F9F',fontSize:12}}>Tint Percentage</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>55%</Text>
                            </View>

                            <View style={{width:'90%',alignSelf:'center',borderColor:"#9F9F9F",paddingVertical:8,marginVertical:7,borderBottomWidth:0.4,borderTopWidth:0.4,flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'black',fontSize:16}}>Add ons</Text>
                            </View>

                            <View>
                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'black',fontSize:12}}>Add on1</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>AED 15.00</Text>
                            </View>
                            <Text style={{color:"#9F9F9F",width:'90%',alignSelf:"center",fontSize:12,marginVertical:3}}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            </Text>
                            </View>

                            <View>
                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'black',fontSize:12}}>Add on2</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>AED 15.00</Text>
                            </View>
                            <Text style={{color:"#9F9F9F",width:'90%',alignSelf:"center",fontSize:12,marginVertical:3}}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            </Text>
                            </View>

                            <View>
                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'black',fontSize:12}}>Add on3</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>AED 15.00</Text>
                            </View>
                            <Text style={{color:"#9F9F9F",width:'90%',alignSelf:"center",fontSize:12,marginVertical:3}}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            </Text>
                            </View>

                            <View>
                            <View style={{width:'90%',alignSelf:'center',flexDirection:'row',justifyContent:"space-between"}}>
                            <Text style={{color:'black',fontSize:12}}>Add on4</Text>
                            <Text style={{color:"#9F9F9F",fontSize:12}}>AED 15.00</Text>
                            </View>
                            <Text style={{color:"#9F9F9F",width:'90%',alignSelf:"center",fontSize:12,marginVertical:3}}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            </Text>
                            </View>

                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Vendor Details</Text>
                                <TouchableOpacity onPress={() => this.setState((prevState) => ({ View2Expanded: !prevState.View2Expanded }))}>
                                    <Icon name={this.state.View2Expanded ? 'ios-arrow-up' : 'ios-arrow-down'}
                                        style={{ fontSize: 15, marginRight: 15 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ display: this.state.View2Expanded ? 'flex' : 'none', paddingBottom: 20, borderTopWidth: 1, borderColor: '#C5C8CC' }}>
                                <View style={{ width: '90%', alignSelf: 'center', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row" }}>
                                    <Text style={{ color: '#9F9F9F' }}>John Doe</Text>
                                    <View style={{ width: '15%', flexDirection: 'row' }}>
                                        <View>
                                            <StarRating
                                                disabled={false}
                                                starSize={13}
                                                starStyle={{}}
                                                disabled={true}
                                                // style={{flexDirection:'row',justifyContent:'flex-start'}}
                                                emptyStar={'ios-star-outline'}
                                                fullStar={'ios-star'}
                                                halfStar={'ios-star-half'}
                                                iconSet={'Ionicons'}
                                                maxStars={5}
                                                rating={5}
                                                selectedStar={(rating) => this.setState({
                                                    starCount: rating
                                                })}
                                                fullStarColor={'#e8c814'}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={{ width: '90%', alignSelf: 'center' }}>
                                    <Text style={{ color: "#9F9F9F", fontSize: 12, marginTop: 4 }}>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                </Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Payment Method</Text>
                                <TouchableOpacity onPress={() => this.setState((prevState) => ({ View3Expanded: !prevState.View3Expanded }))}>
                                    <Icon name={this.state.View3Expanded ? 'ios-arrow-up' : 'ios-arrow-down'}
                                        style={{ fontSize: 15, marginRight: 15 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ display: this.state.View3Expanded ? 'flex' : 'none', paddingBottom: 20 }}>
                                <View style={{ width: '90%', alignSelf: "center" }}>
                                    <Text style={{ color: "black" }}>Pay Via</Text>
                                </View>
                                <View style={{ width: '90%', alignSelf: "center", alignItems: 'center', justifyContent: "space-between", flexDirection: 'row' }}>
                                    <Text style={{ color: "black" }}>
                                        {this.state.paymentType || "Cash"}
                                    </Text>
                                    <TouchableOpacity onPress={() => this.setState((prevState) => ({ paymentExpanded: !prevState.paymentExpanded }))}>
                                        <Icon name={this.state.paymentExpanded ? 'ios-arrow-up' : 'ios-arrow-down'}
                                            style={{ fontSize: 15, marginRight: 15 }} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>

                    {this.state.paymentExpanded && <View style={{
                        // position:'absolute',
                        alignSelf: 'center',
                        // top:width*0.15,
                        // right:12,
                        marginTop: -15,
                        zIndex: 2,
                        backgroundColor: 'white',
                        borderRadius: 14,
                        height: 100,
                        width: width * 0.85,
                        alignItems: 'center',
                        borderColor: "#e5e3e3",
                        borderWidth: 0.40
                    }}>
                        <TouchableOpacity onPress={() => this.setState({ paymentType: '1234 xxxx xxxx xxxx', paymentExpanded: false })} style={{ width: '100%', height: "33.33%" }}><Text style={{ width: '80%', marginLeft: 15 }}>1234 xxxx xxxx xxxx </Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ paymentType: 'Cash', paymentExpanded: false })} style={{ width: '100%', height: "33.33%", borderColor: "#DCDCDC", borderTopWidth: 1 }}><Text style={{ width: '80%', marginLeft: 15 }}>Cash</Text></TouchableOpacity>
                        <View style={[styles.attach, { height: "33.33%", borderColor: "#DCDCDC", borderTopWidth: 1, marginTop: 0 }]}>
                            <Image source={Plus} style={styles.attachIcon} />
                            <Text style={{ color: "#9F9F9F" }}>
                                Add new Card
                                </Text>
                        </View>
                    </View>}

                    <View style={{ margin: 10 }}>
                        <View style={styles.insideContainer}>
                            <View style={{ width: "80%" }}>
                                <TextInput placeholder="Promo Code" underlineColorAndroid='#DCDCDC' style={{ paddingLeft: 20, width: '80%', alignSelf: "center" }} />
                            </View>
                            <View style={styles.rightContainer}>
                                <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'center', alignItems: "center" }}>
                                    <Text style={{ color: 'white', fontSize: 13 }}>Verify</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <Text style={{
                        width: '84%',
                        alignSelf: 'center',
                        fontSize: 17.75,
                        color: 'black',
                        marginTop: 7
                    }}>Wallet Balance</Text>
                    <View style={{
                        width: '85%',
                        alignSelf: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: "center"
                    }}>
                        <Text style={{ color: "grey" }}>User balance First (100.00AED)</Text>
                        <TouchableOpacity onPress={()=>this.setState((prevState)=>({SwitchOn:!prevState.SwitchOn}))}>
                        <Image source={this.state.SwitchOn ? ToggleOn : ToggleOff} style={{ width: 50, height: 35, margin: 5 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.priceContanier}>
                        <Text style={styles.text1}>Price:  155.00 AED</Text>
                        <Text style={styles.text2}>including VAT - 10 AED</Text>
                    </View>


                    <View style={styles.bottom}>
                        <Button title="Place Order" colored width={"90%"} onPress={() => this.props.navigation.navigate('Inspection')} />
                    </View>

                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    itemTitle: {
        fontSize: 14,
        color: "black",
        width: "80%",
        marginLeft: "auto",
        marginRight: "auto",
        marginBottom: 10,
        fontWeight: "bold"
    },
    insideContainer: {
        width: "90%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 12.5,

    },
    rightContainer: {
        width: "20%",
        backgroundColor: "#AE1371",
        borderTopRightRadius: 12.5,
        borderBottomRightRadius: 12.5,
        height: "100%",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"

    },
    addressBarText: {
        color: 'black',
        fontSize: 14,
        marginLeft: 15
    },
    addressBarContainer: {
        width: width * 0.9,
        height: 50,
        borderRadius: 15,
        alignSelf: 'center',
        backgroundColor: "white",
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: "center",
        margin: 10
    },
    priceContanier: {
        borderTopWidth: 1,
        width: "100%",
        // marginTop:15,
        position: "absolute",
        bottom: 60,
        borderColor: "#DCDCDC",
        padding: 20,
    },
    text1: {
        textAlign: "right",
        fontSize: 18,
        fontWeight: "bold",
        color: "#AE1371"

    },
    text2: {
        justifyContent: "flex-end",
        textAlign: "right",
        fontSize: 10

    },
    bar: {
        height: 3,
        width: width * 0.3,
        backgroundColor: '#C5C8CC'
    },
    container: {
        backgroundColor: '#F8F8F8'
        // justifyContent:'center',
        // alignItems:'center',
        // flex: 1
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: '#C5C8CC'
    },
    circleColor: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 3,
        borderColor: '#AE1371',
        backgroundColor: 'white'
    },
    innerContainer: {
        width: "100%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,
        justifyContent: "space-between",
        alignItems: "center"

    },
    outerContainer: {
        width: "90%",
        margin: 10,
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        // flexDirection: "row",
        height: 'auto',
        borderRadius: 14,
        // justifyContent:"space-between",
        // alignItems:"center"

    },
    itemText: {
        color: "black",
        marginLeft: 15,
        fontSize: 16
    },
    icon: {
        width: 20,
        height: 20,
        marginRight: 15
    },
    bottom: {
        // position: 'absolute',
        bottom: 10,
        width: "100%"
    },
    attach: {
        width: "100%", flexDirection: 'row', alignItems: "center", marginTop: 10
    },
    attachIcon: {
        height: 12, width: 12, marginHorizontal: 10
    },

    priceContanier: {
        borderTopWidth: 1,
        width: "100%",
        // marginTop:15,
        // position: "absolute",
        marginBottom: 20,
        borderColor: "#DCDCDC",
        padding: 20,
    },
    text1: {
        textAlign: "right",
        fontSize: 18,
        fontWeight: "bold",
        color: "#AE1371"

    },
    text2: {
        justifyContent: "flex-end",
        textAlign: "right",
        fontSize: 10

    }


});

export default ConfirmBooking;