import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Header from '../components/Header'
import Button from '../components/Buttons'
import Pen from '../../images/pen.png'
import Cross from '../../images/close.png'
import Modal from 'react-native-modal'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Inspection extends Component {

    constructor(props) {
        super(props);
        this.state={
            hours:0,
            modalVisible:false
        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Home" RightIcon={Cross} onRightPress={() => navigation.navigate('Starter')} goBack={navigation.goBack} />
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={{ height: height * 0.88 }}>
                <Modal isVisible={this.state.modalVisible}>
                    <View style={[styles.modalContainer, { height: height * 0.4 }]}>
                        <View style={styles.modalTop}>
                            <Text style={styles.modalTitle}>Confirm Booking</Text>
                            <TouchableOpacity onPress={() => this.setState({ modalVisible: false})}>
                                <Image source={Cross} style={{ height: 20, width: 20, marginRight: 10, marginVertical: 15 }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            height:50,
                            borderBottomColor:'#DCDCDC',
                            borderBottomWidth:0.3,
                            flexDirection:"row",
                            alignItems:'center'
                        }}>
                            <Text style={{width:'85%',alignSelf:'center',fontSize:14,color:'grey',marginRight:'auto',marginLeft:'auto'}}>IT Services - Invoice</Text>
                        </View>
                        <View style={{width:'85%',alignSelf:'center',justifyContent:'space-between',alignItems:"center",flexDirection:'row'}}>
                            <Text style={{color:'grey',marginVertical:4}}>Service Charges</Text>
                            <Text style={{color:'grey',marginVertical:4}}>200 AED</Text>
                        </View>
                        <View  style={{width:'85%',alignSelf:'center',justifyContent:'space-between',alignItems:"center",flexDirection:'row'}}>
                            <Text style={{color:'grey',marginVertical:4}}>Spare Parts Charges</Text>
                            <Text style={{color:'grey',marginVertical:4}}>10 AED</Text>
                        </View>

                        <View style={{borderTopColor:'#DCDCDC',borderTopWidth:1,padding:10}}>
                        <Text style={{fontSize:20,flexDirection:'row',alignSelf:"flex-end",fontWeight:'500',color:'#AE1371'}}>Price : 100.00 AEd</Text>
                        <Text style={{fontSize:12,alignSelf:'flex-end',color:'grey'}}>Including VAT - 10 AED</Text>
                        </View>
                        <TouchableOpacity onPress={() => this.setState({ modalVisible: false })} style={styles.modalButton}>
                            <Text style={styles.modalButtonText}>Confirm</Text>
                        </TouchableOpacity>

                    </View>
                </Modal>

                <View style={{
                    width:'90%',
                    alignSelf:'center',
                    marginTop:18
                }}>
                    <Text style={{color:'grey',fontSize:12}}>
                        An inspection is required to determine the type of work ,Selected vendor will come to your location and .....
                    </Text>
                </View>

                    <View style={styles.priceContanier}>
                        <Text style={styles.text1}>Price:  0.00 AED</Text>
                        <Text style={styles.text2}>including VAT - 10 AED</Text>
                    </View>

                    <View style={styles.bottom} >
                        <Button title="Book inspection" colored width={"90%"} onPress={() => this.setState({ modalVisible: true })}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor:'#F8F8F8'
        // justifyContent:'center',
        // alignItems:'center',
        // flex: 1
    },
    bottom:{
        position: 'absolute',
        bottom: 10,
        width: "100%"
    },
    priceContanier:{
        borderTopWidth:1,
        width:"100%",
        // marginTop:15,
        position:"absolute",
        bottom:60,
        borderColor:"#DCDCDC",
        padding:20,
    },
    text1:{
        textAlign:"right",
        fontSize:18,
        fontWeight:"bold",
        color:"#AE1371"

    },
    text2:{
        justifyContent:"flex-end",
        textAlign:"right",
        fontSize:10

    },
    modalButton: {
        backgroundColor: '#AE1371',
        width: '100%',
        height: 50,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        position: 'absolute',
        bottom: 0

    },
    modalTitle: {
        color: 'black',
        fontSize: 18,
        width: '80%',
        marginRight: "auto",
        marginLeft: "auto",
        height: 50,
        fontWeight: "bold",
        textAlignVertical: "center"

    },
    modalContainer: {
        width: width * 0.9,
        height: height * 0.7,
        borderRadius: 15,
        backgroundColor: '#F8F8F8',

    },
    modalTop: {
        backgroundColor: "white",
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        width: '100%',
        height: 50,
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    modalButtonText: {
        color: "white",
        textAlign: 'center',
        height: 50,
        textAlignVertical: 'center',
        fontSize: 16

    },


});

export default Inspection;