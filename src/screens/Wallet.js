import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Container, Content, ListItem, CheckBox, Body, Right } from "native-base";
import Header from '../components/Header'
import {Icon} from 'native-base'
import Plus from '../../images/plus.png'
import Tick from '../../images/tick.png'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Wallet extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ViewExpanded:true,

        }
    }
    static navigationOptions = {
        header: ({ navigation }) => <Header title="Wallet" goBack={()=>navigation.navigate('Starter')} />
    };
    render() {
        return (
            <ScrollView style={{backgroundColor:'#F8F8F8'}}>
                <Text style={styles.title}>
                    Current Payment Method
                </Text>
                <View
                    style={styles.card}
                >
                    <View style={{ width: '45%', height: '100%' }}>
                        <Text style={styles.cardText}>Card Type</Text>
                        <Text style={styles.cardText}>Card Number</Text>
                        <Text style={styles.cardText}>Month Year</Text>
                        <Text style={{ fontSize: 11, color: 'white', marginLeft: 14, marginTop: 10 }}>1220</Text>
                        <Text style={styles.cardText}>Card Holder</Text>
                    </View>
                    <View style={{ width: '55%', height: '100%' }}>
                        <Text style={{ color: 'white', marginTop: 12 }}>VISA</Text>
                        <Text style={{ color: 'white', marginTop: 12 }}>4950 xxxx xxxx xxxx</Text>
                        <Text></Text>
                        <Text></Text>
                        <Text></Text>
                    </View>
                </View>

                <View
                style={{borderTopWidth:1,marginTop:18,borderColor:'#DCDCDC'}}
                >
                <View style={{flexDirection:'row',marginRight:'auto',marginLeft:"auto",width:'90%',alignItems:"center"}}>
                    <Image source={Plus} style={{height:16,width:16,marginRight:5}}/>
                    <Text style={[styles.title,{paddingBottom:'auto'}]}>Add Credit Card</Text>
                </View>
                </View>

                {/* <TouchableOpacity> */}
                        <View style={styles.outerContainer} >
                            <View style={styles.innerContainer}>
                                <Text style={styles.itemText}>Choose Payment Mode</Text>
                                <TouchableOpacity onPress={()=>this.setState((prevState)=>({ViewExpanded:!prevState.ViewExpanded}))}>
                                <Icon name={this.state.ViewExpanded ? 'ios-arrow-up' : 'ios-arrow-down'} 
                                style={{ fontSize: 18, marginRight: 15 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{display:this.state.ViewExpanded ? 'flex' : 'none',paddingBottom:20}}>
                            {/* <View style> */}
                            <View style={styles.attach}>
                            <Image source={Tick} style={{height:13,width:20}}/>
                                <Text style={{color:"#AE1371"}}> 4950 xxxx xxxx xxxx</Text>
                            </View>
                            {/* </View> */}
                            <View style={styles.attach}>
                            <Image source={null} style={{height:10,width:20}}/>
                                <Text style={{color:"#8e8b8b"}}>1230 xxxx xxxx xxxx</Text>
                            </View>
                            <View style={styles.attach}>
                            <Image source={null} style={{height:10,width:20}}/>
                                <Text style={{color:"#8e8b8b"}}>Cash</Text>
                            </View>
                            </View>
                        </View>
                    {/* </TouchableOpacity> */}

                <TouchableOpacity onPress={()=>alert('hi')}>
                <View style={{
                    height:50,
                    width:'100%',
                    borderColor:'#DCDCDC',
                    borderTopWidth:1,
                }}>
                <View style={{width:'84%',marginRight:'auto',marginLeft:'auto',height:'100%',flexDirection:'row',justifyContent:"space-between",alignItems:'center'}}>
                    <Text style={{color:"#827f7f",fontSize:16}}>Topup Credit Card</Text>
                    <Icon name='ios-arrow-forward' style={{fontSize:18,color:'#827f7f'}} />

                </View>
                </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>alert('hi')}>
                <View style={{
                    height:50,
                    width:'100%',
                    borderColor:'#DCDCDC',
                    borderTopWidth:1,
                }}>
                <View style={{width:'84%',marginRight:'auto',marginLeft:'auto',height:'100%',flexDirection:'row',justifyContent:"space-between",alignItems:'center'}}>
                    <Text style={{color:"#827f7f",fontSize:16}}>Redeem Voucher</Text>
                    <Icon name='ios-arrow-forward' style={{fontSize:18,color:'#827f7f'}} />

                </View>
                </View></TouchableOpacity>

                <View style={{
                    height:120,
                    width:'100%',
                    borderColor:'#DCDCDC',
                    borderTopWidth:1,
                }}>
                <View style={{width:'84%',marginRight:'auto',marginLeft:'auto',height:'100%'}}>
                    <Text style={{color:"#AE1371",fontSize:16,marginTop:10}}>Wallet Balance</Text>
                    <Text style={{color:"#827f7f",fontSize:16,marginTop:10}}>User Balance (100.00 AED)</Text>
                </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cardText: { marginTop: 12, marginLeft: 14, color: 'white' },
    title: {
        width: '90%',
        marginRight: 'auto',
        marginLeft: 'auto',
        fontSize: 17,
        color: 'black',
        marginVertical: 12,
        paddingBottom: 12

    },
    card: {
        backgroundColor: "#AE1371",
        borderRadius: 18,
        width: '90%',
        marginRight: 'auto',
        marginLeft: 'auto',
        height: height * 0.23,
        flexDirection: 'row',

    },
    innerContainer: {
        width: "100%",
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        flexDirection: "row",
        height: 50,
        borderRadius: 14,
        justifyContent:"space-between",
        alignItems:"center"

    },
    outerContainer: {
        width: "84%",
        margin:10,
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "white",
        // flexDirection: "row",
        height: 'auto',
        borderRadius: 14,
        // justifyContent:"space-between",
        // alignItems:"center"

    },
    itemText:{
        color:"#8e8b8b",
        marginLeft:15,
        fontSize:16
    },
    attach:{
        width:"80%",
        flexDirection:'row',
        marginRight:'auto',
        marginLeft:'auto',
        alignItems:"center",
        marginTop:5,
        // justifyContent:'flex-end'
    },
    attachIcon:{
        height:12,width:12,marginHorizontal:10
    },

});

export default Wallet;