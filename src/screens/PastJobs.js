import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';

import Group from '../../images/Group2.png'
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class Past extends Component{
    constructor(){
        super();
        this.state={
            sortVisible:false
        }
    }
    render(){console.log(this.state,"this is state")
    return(
        <View style={{backgroundColor: '#F8F8F8'}}>
        <View style={{
            width:width,
            padding:8,
            height:50,
            justifyContent:"center"
        }}>
        <Text style={{color:'black',fontSize:16}}>Monthly Summary</Text>
        </View>
        <ScrollView 
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
            height:80,
            backgroundColor:"white"
        }}>
        {[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,].map((data,index)=>(<TouchableOpacity 
        onPress={()=>this.setState({month:index})}
        key={index}
        style={{
        height:'auto',
        backgroundColor:this.state.month === index ? '#AE1371'  : "white",
        width:'auto',
        padding: this.state.month === index ? 8 : 3 ,
        borderRadius : this.state.month === index ? 9 : 0,
        marginHorizontal:8

    }}
        >
        <Text style={{color:this.state.month === index ? 'white' : '#AE1371',textAlign:"center",fontSize:14}}>1k AED</Text>
        <Text style={{color:this.state.month === index ? 'white' : 'grey',fontSize:20,textAlign:'center'}}>May</Text>
        <Text style={{color:this.state.month === index ? 'white' : 'grey',textAlign:"center",fontSize:8,margin:4}}>2018</Text>

        </TouchableOpacity>))}
        </ScrollView>

    <ScrollView style={{ backgroundColor: '#F8F8F8' ,minHeight:height*0.88}}>
    <View style={styles.barOuterContainer}>
    <View style={styles.barInnerContainer}>
        <Text style={{color:"black",fontSize:16}}>Individual Summary</Text>
        <TouchableOpacity style={styles.sortButton} onPress={()=>this.setState((prevState)=>({sortVisible:!prevState.sortVisible}))}>
        <Image source={Group} style={{width:37,height:18,margin:7,marginLeft:12,}}/>
        <Text style={{fontSize:15}}>Sort by</Text>
        </TouchableOpacity>
    </View>
    </View>
    {this.state.sortVisible && <View style={{
        position:'absolute',
        top:width*0.15,
        right:12,
        zIndex:2,
        backgroundColor:'white',
        borderRadius:14,
        height:80,
        width:150,
        alignItems:'center',
        borderColor:"#e5e3e3",
        borderWidth:0.40
    }}>
    <View style={{width:'100%',height:"33.33%"}}><Text style={{width:'80%',marginLeft:15}}>Vendor Name</Text></View>
    <View style={{width:'100%',height:"33.33%",borderColor:"#DCDCDC",borderTopWidth:1}}><Text style={{width:'80%',marginLeft:15}}>Date</Text></View>
    <View style={{width:'100%',borderColor:"#DCDCDC",height:"33.33%",borderTopWidth:1}}><Text style={{width:'80%',marginLeft:15}}>Service Name</Text></View>
    </View>}
    <View>
        {[
            {
                titleRight: 'Car Wash',
                titleLeft: '100AED',
                vendor: 'xyzzy',
                location: 'street123,xyz,area,state',
                date: '23rd,oct,2018'
            }, {
                titleRight: 'Plumbing',
                titleLeft: '100AED',
                vendor: 'xyzzy',
                location: 'street123,xyz,area,state',
                date: '23rd,oct,2018'
            }, {
                titleRight: 'Home Cleaning',
                titleLeft: '100AED',
                vendor: 'xyzzy',
                location: 'street123,xyz,area,state',
                date: '23rd,oct,2018'
            }, {
                titleRight: 'Laundry',
                titleLeft: '100AED',
                vendor: 'xyzzy',
                location: 'street123,xyz,area,state',
                date: '23rd,oct,2018'
            },
        ]
            .map((data, index) => (<View key={index} style={styles.container} >
                <View style={styles.left} >
                    <Text style={styles.titleLeft} >
                        {data.titleRight}
                    </Text>
                    <Text style={styles.textLeft} >
                        Vendor:{data.vendor}
                    </Text>
                    <Text style={styles.textLeft} >
                        Location:{data.location}
                    </Text>
                </View>
                <View style={styles.right} >
                    <Text style={styles.titleRight} >
                        {data.titleLeft}
                    </Text>
                    <Text style={styles.textRight} >
                        {data.date}
                    </Text>
                </View>

            </View>
            )
            )
        }</View>
    </ScrollView>
    </View>
)}}

const styles = StyleSheet.create({
    container:{
        backgroundColor:"white",
        borderRadius:15,
        marginRight:'auto',
        marginLeft:'auto',
        width:"90%",
        height:height*0.135,
        marginTop:20,
        flexDirection:'row',
        alignItems:'center'

    },
    titleLeft:{
        color: 'black',
        fontSize:16,
        marginLeft:17,
        // fontWeight:'450'
    },
    titleRight:{
        color: 'black',
        fontSize: 16,
        textAlign:"center",
        // fontWeight:'450'

    },
    barInnerContainer:{
        width:'90%',
        marginRight:'auto',
        marginLeft:'auto',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        height:'100%'

    },
    textRight:{
        fontSize: 10,
        textAlign:'center',
        marginTop:3
    },
    textLeft:{
        fontSize: 12,
        marginLeft: 17,
        marginTop:5
    },
    right:{
        width:'32%',
        height:'80%',
    },
    left:{
        width:'68%',
        height:'80%'
    },
    barOuterContainer:{
        height:50,
        borderBottomWidth:1,
        borderColor:'#DCDCDC'

    },
    sortButton:{
        backgroundColor:"white",
        width:width*0.30,
        borderRadius:14,
        height:"80%",
        flexDirection:'row',
        alignItems:"center"

    }
});