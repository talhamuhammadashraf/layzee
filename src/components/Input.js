import React from 'react';
import {View,Text,Dimensions,TextInput} from 'react-native';
import PhoneInput from 'react-native-phone-input'
const {height,width} = Dimensions.get("window")
const Input = (props) =>(
    <View
    style={{marginTop:15}}
    >
    {props.error && 
    <Text style={{
        color:"red",
        fontSize:12,
        width:width*0.9,
        marginRight:"auto",
        marginLeft:"auto"
        }}
    >{props.error}</Text>}
    <View 
    style={{
        backgroundColor:"white",
        borderRadius:10,
        width:width*0.9,
        marginRight:"auto",
        marginLeft:"auto",
        height:height*0.08,
        marginTop:5,
        alignItems:'center',
        borderColor:"red",
        borderWidth: props.error ? 1 : 0
    
    }}
    >
        {props.phone ? 
        <PhoneInput 
        getISOCode={(code)=>console.log(code,'this is the code')}
        onChangePhoneNumber={props.onChangePhoneNumber}
        getValue={props.getValue}
        isValidNumber={props.isValidNumber}
        textProps={{
            placeholder:"Mobile Number"
        }}
        textStyle	={{fontSize:18}}
        initialCountry="ae"
        // onSelectCountry={(a)=>console.log(a)}
        style={{
            width:"85%",
            marginVertical:15,
        }}
        {...props}
        />:
        <TextInput
        // editable={props.editable}
        secureTextEntry={props.secureTextEntry}
        onChangeText={props.onChangeText}
        underlineColorAndroid="transparent"
        placeholder={props.placeholder}
        placeholderTextColor="#d6d4d4"
        onFocus={props.onFocus}
        style={{
            width:"85%",
            marginRight:"auto",
            marginLeft:"auto",
            fontSize:18

        }}
        />}
    </View>
    </View>
)

export default Input