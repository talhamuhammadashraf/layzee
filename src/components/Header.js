import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import Logo from '../../images/logoSplash.jpg'
import Menu from '../../images/menu.png'
import Back from '../../images/backward.png'
import Cross from '../../images/close.png'

const { height, width } = Dimensions.get('window')

const Header = (props) => {
    const onLeftPress = props.menu ? () => props.openDrawer() : () => props.goBack(null);
    const onRightPress = props.onRightPress ? () => props.onRightPress() : () => alert("hi")
    return (
        <View style={Styles.container}>{console.log(props)}
            <TouchableOpacity onPress={() => onLeftPress()} style={Styles.iconContainer}>
                <Image source={props.menu ? Menu : Back} style={Styles.imageLeft} />
            </TouchableOpacity>
            {props.title ?
                <View style={{ width: "77%", flexDirection: "row", justifyContent: 'flex-start'  ,alignItems:"center"}}>
                    <Text style={Styles.headerText}>{props.title}</Text>
                </View> :
                <Image source={Logo} style={Styles.logo} />} 
            {/* {props.title && <View style={Styles.empty}></View>} */}
            {props.RightIcon ?
                <TouchableOpacity onPress={() => onRightPress()} style={Styles.iconContainer}>
                    <Image source={props.RightIcon} style={Styles.imageRight} />
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={() => onRightPress()} style={{flexDirection: "row", justifyContent: 'flex-start'  ,alignItems:"center"}}>
                    <Text style={Styles.textRight}>{props.textRight}</Text>
                </TouchableOpacity>
            }
        </View>
    )
}
export default Header;

const Styles = StyleSheet.create({
    iconContainer:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:"center",
    },
    container: {
        width: width,
        height: height * 0.08,
        flexDirection: "row",
        justifyContent: "space-between",
        alignContent:"center",
        backgroundColor: "white"
    },
    imageLeft: {
        height: 20,
        width: 20,
        marginLeft: 16,
        // marginVertical: 20
    },
    imageRight: {
        height: 20,//17,
        width: 20,
        // marginTop: 15,
        marginRight: 20,

    },
    textRight: {
        // marginTop: 14,
        marginRight: 20,
        fontSize: 15,
        color: 'black'
    },
    empty: {
        width: "40%",
        height: height * 0.08
    },
    logo: {
        height: height * 0.0275,
        marginVertical: 15,
        width: width * 0.25,
        alignSelf:'center',
    },
    headerText: {
        fontSize: 15,
        color: "black",
        margin: 10,
        // marginTop: 14
    }
})