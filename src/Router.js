
import React, { Component } from 'react';
import {Provider} from 'react-redux'
import store from './store'
import { StackNavigator, addNavigationHelpers, createSwitchNavigator, HeaderBackButton, NavigationActions, createDrawerNavigator } from 'react-navigation';
import Splash from './screens/Splash';
import { View, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import Profile from './screens/Profile';
import DashBoard from './screens/DashBoard';
import SideDrawer from './SideDrawer';
import WindowTinting from './screens/WindowTinting';
import LoginScreen from './screens/LoginScreen';
import Calender from '../images/smallcalendar.png'
import Header from './components/Header'
import SignUp from './screens/SignUp';
import VerificationCode from './screens/VerificationCode';
import About from './screens/About';
import SignIn from './screens/SignIn';
import Policies from './screens/Policies';
import EditProfile from './screens/EditProfile';
import AddOrEditCar from './screens/AddOrEditCar';
import AddOrEditAddress from './screens/AddOrEditAddress';
import RentCar1 from './screens/RentCar1';
import RentCar2 from './screens/RentCar2';
import RentCar3 from './screens/RentCar3';
import RentCar4 from './screens/RentCar4';
import FavServices from './screens/FavoriteServices';
import Wallet from './screens/Wallet';
import Jobs from './screens/Jobs';
import Receipt from './screens/Receipt';
import VendorList from './screens/VendorList';
import RatingReviews from './screens/RatingReviews';
import VendorProfile from './screens/VendorProfile';
import ServicesProvided from './screens/ServicesProvided';
import Portfolio from './screens/Portfolio';
import Wedding from './screens/Wedding';
import Wedding1 from './screens/Wedding1';
import Inspection from './screens/Inspection';
import JobCalendar from './screens/JobCalendar';
import Agenda from './screens/Agenda';
import PrintReceipt from './screens/PrintReceipt';
import CarWashDetails from './screens/CarWashDetails';
import CarWashDetails1 from './screens/CarWashDetails1';
import ConfirmBooking from './screens/ConfirmBooking';
import FAQ from './screens/FAQ';
import Laundry from './screens/Laundry';
import VendorMap from './screens/VendorsMap';
import ForgetPassword from './screens/ForgetPassword'
import NewPassword from './screens/NewPassword'
import Indicator from './screens/ActivityIndicator';
const navigateOnce = (getStateForAction) => (action, state) => {
  const { type, routeName } = action;
  return (
    state &&
    type === NavigationActions.NAVIGATE &&
    routeName === state.routes[state.routes.length - 1].routeName
  ) ? null : getStateForAction(action, state);
  // you might want to replace 'null' with 'state' if you're using redux (see comments below)
};

const AppStarter = StackNavigator(
  {
    Home: { screen: Splash },
    // SignUp:{screen : SignUp}
  },
  {
    navigationOptions: { header: null },
    initialRouteName: "Home"
  },
)
  ;

export const InBetween = StackNavigator(
  {
    // home: { screen: DashBoard },
    Profile: { screen: Profile },
    Jobs: {
      screen: Jobs,
      navigationOptions: ({ navigation }) => ({
        header: ({ navigation }) => <Header title="Jobs" goBack={() => navigation.navigate('Starter')} RightIcon={Calender} onRightPress={() => navigation.navigate('JobCalendar')} />
      })
    },
    Indicator:{screen:Indicator},
    newPassword : {screen : NewPassword},
    ForgetPassword : {screen :ForgetPassword},
    Wallet: { screen: Wallet },
    FavServices: { screen: FavServices },
    About: { screen: About },
    Receipt: { screen: Receipt },
    WindowTinting: { screen: WindowTinting },
    SignUp: { screen: SignUp },
    SignIn: { screen: SignIn },
    verify: { screen: VerificationCode },
    Policies: { screen: Policies },
    EditProfile: { screen: EditProfile },
    AddOrEditCar: { screen: AddOrEditCar },
    AddOrEditAddress: { screen: AddOrEditAddress },
    RentCar1: { screen: RentCar1 },
    RentCar2: { screen: RentCar2 },
    RentCar3: { screen: RentCar3 },
    RentCar4: { screen: RentCar4 },
    VendorList: { screen: VendorList },
    RatingReviews: { screen: RatingReviews },
    VendorProfile: { screen: VendorProfile },
    Portfolio: { screen: Portfolio },
    Wedding: { screen: Wedding },
    Wedding1: { screen: Wedding1 },
    Inspection :{screen: Inspection},
    Agenda :{screen: Agenda},
    JobCalendar :{screen: JobCalendar},
    ServicesProvided: { screen: ServicesProvided },
    PrintReceipt : {screen : PrintReceipt},
    CarWashDetails : {screen :CarWashDetails},
    CarWashDetails1 : {screen :CarWashDetails1},
    ConfirmBooking:{screen:ConfirmBooking},
    FAQ:{screen:FAQ},
    Laundry :{ screen :Laundry},
    VendorMap :{ screen :VendorMap},
    // DashBoard:{ screen: DrawerContainer},
  }, {
    navigationOptions: {

    }
  }
);
// console.log(InBetween.router,"maderchod")
// InBetween.router.getStateForAction = navigateOnce(InBetween.router.getStateForAction);
// console.log(InBetween.router.getStateForAction,"this is state")
const DrawerButton = (props) => {
  return (
    <View>
      <TouchableOpacity onPress={() => { props.navigation.toggleDrawer() }}>
        <Image
          source={require('../images/menu.png')}
          style={{ height: 17, width: 25, marginLeft: 10 }}
        />
      </TouchableOpacity>
    </View>
  );
};

const Starter = StackNavigator({
  home: {
    screen: DashBoard,
    // navigationOptions: ({navigation}) => ({
    //   title: "Layzee",
    //   headerLeft: <DrawerButton navigation={navigation}  />
    // }),
  }
});
const Drawer = createDrawerNavigator({
  Starter: { screen: Starter },
},
  {
    // define customComponent here
    contentComponent: SideDrawer,
  }
);

 const Stack = createSwitchNavigator(
  {
    Auth: AppStarter,
    App: InBetween,
    DashBoard: Drawer
    // InBetween: { screen: InBetween }

  },
  {
    initialRouteName: "Auth",
  }
);
// export default Stack
// const mapStateToProps = state => ({
//   navigation: state.nav,
// })


// export default class Main extends Component{
//   render(){
//     return(
//       <Stack
//       navigation={{
//         dispatch: this.props.dispatch,
//         state: this.props.nav,
//         addListener
//       }}
//       />
//       )
//     }
//   }
// export const MainContainer =connect(mapStateToProps)(Main)
export default class App extends Component {
  render() {
    return (
      <Provider store={store}><Stack/></Provider>
    );
  }
}